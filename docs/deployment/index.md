# Installation Guide

There are multiple ways to install the Melete:

* with [Helm](https://helm.sh/), using project repository chart;
* with kubectl apply, using YAML manifests.

## Quick start

If you have Helm, you can deploy the melete with the following command:

```shell
HELM_EXPERIMENTAL_OCI=1 \
helm upgrade \
  --install \
  --namespace melete-system \
  --create-namespace \
  melete \
  oci://registry.gitlab.com/redcreepster/melete/melete \
  --version {{ version }} \
  --atomic \
  --wait-for-jobs
```

It will install the melete in then melete-system namespace, creating that namespace if it doesn't already exist.

!!! info "Info"
    This command is idempotent:

    * if the melete is not installed, it will install it, <!-- markdownlint-disable-line no-space-in-emphasis code-block-style -->
    * if the melete is already installed, it will upgrade it.

If tou don't have Helm or if you prefer to use a YAML manifest, you can run then following command instead:

[//]: # (TODO: Implement it)
!!! warning "Not implemented"
    This way will be implemented in future releases.

```shell
kubectl apply -f https://gitlab.com/redcreepster/melete/-/raw/{{ version }}/config/deploy.yaml
```

!!! info "Info"
    The YAML manifest in the command above was generated with `helm template`,
    so you will end up with almost the same resources as if you had used Helm to install the melete.
