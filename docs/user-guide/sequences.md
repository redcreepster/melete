# Sequence

## Webhooks

```mermaid
sequenceDiagram
    participant User
    participant Kubernetes
    participant Melete
    autonumber

    User ->> Kubernetes: Create Odyssey
    Kubernetes ->> Melete: Webhooks
    opt odysseyInstanceReferences not present
        Note over Melete: Add master and replica references
    end
    opt postgresEndpointReference not present
        Melete ->> Kubernetes: Get default PostgresEndpoint
        critical
            Kubernetes ->> Melete: Not found
            Melete ->> Kubernetes: Error
            Kubernetes ->> User: Error
        end
        Kubernetes ->> Melete: PostgresEndpoint
        Note over Melete: Add default postgres endpoint reference
    end
    Melete ->> Kubernetes: Ok
    Kubernetes ->> User: Ok
```

## Odyssey Controller

```mermaid
sequenceDiagram
    participant Kubernetes
    participant Melete
    participant Postgres
    autonumber

    Kubernetes ->> Melete: Odyssey
    opt password not present
        Note over Melete: Generate password
        Melete ->> Kubernetes: Update Odyssey
        critical
            Kubernetes ->> Melete: Error
            Melete ->> Kubernetes: Retry after 30 seconds
        end
    end
    Melete ->> Kubernetes: Get PostgresEndpoint
    critical
        Kubernetes ->> Melete: Error
        Melete ->> Kubernetes: Retry after 30 seconds
    end
    Melete ->> Postgres: Create or update user and database
    critical
        Postgres ->> Melete: Error
        Melete ->> Kubernetes: Retry after 30 seconds
    end
    loop Each odyssey instance reference
        alt Replica found in PostgresEndpoint
            Note over Melete: Add database to new or existing OdysseyInstance
            Melete ->> Kubernetes: Create or update OdysseyInstance
            critical
                Kubernetes ->> Melete: Error
                Melete ->> Kubernetes: Retry after 30 seconds
            end
            Note over Melete, Kubernetes: Create or update<br/>Deployment and Secret<br/>in OdysseyInstance controller
        else Replica not found in PostgresEndpoint
            Note over Melete: Skip reference
        end
    end
    Melete ->> Kubernetes: Create or update Secret
    critical
        Kubernetes ->> Melete: Error
        Melete ->> Kubernetes: Retry after 30 seconds
    end
```
