# Basic usage

For create database, user and secret create [Odyssey](crds/odyssey.md) resource.

Basic manifest may contain only [`maxConnections`](crds/odyssey.md#maxconnections) field.

The password can be specified in the [`password`](crds/odyssey.md#password) field.
Otherwise, it will be generated automatically.

```yaml title="odyssey.yaml" linenums="1"
apiVersion: database.monitorsoft.ru/v1alpha5
kind: Odyssey
metadata:
  name: my-service
  namespace: default
spec:
  maxConnections: 30
```

Melete will create a new database and user named `default.my-service` in the postgres cluster.

Melete will then create a secret called `my-service-odyssey` with the following fields:

* `DB_HOST`
* `DB_PORT`
* `DB_NAME`
* `DB_USERNAME`
* `DB_PASSWORD`

For information about additional fields see [Odyssey](crds/odyssey.md#fields).
