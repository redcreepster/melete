# PostgresEndpoint

This CRD contains information about postgres cluster.

## Example

```yaml title="postgres-endpoint.yaml" linenums="1"
apiVersion: database.monitorsoft.ru/v1alpha1
kind: PostgresEndpoint
metadata:
  name: postgres-endpoint
  namespace: default
spec:
  host: postgres.postgres.svc # Address of master postgres node
  port: 5432 # Port of master postgres node
  user: # Credentials
    name: melete
    password: 5MnPXu84
  defaultDatabase: melete # Database name for connection. .user.name will be used when is empty.
  replicas: # Additional replicas
  - name: replica
    host: postgres-replica.postgres.svc
    port: 5432
  - name: sync-replica
    host: postgres-sync-replica.postgres.svc
    port: 5432
```

## Required fields

* `.spec.host`
* `.spec.port`
* `.spec.user.name`
* `.spec.user.password`

For more information about additional replicas see
[replicas](../replicas.md),
[Odyssey Instance References](odyssey.md#odysseyinstancereferences),
[Postgres Endpoint Reference](odyssey.md#postgresEndpointReference),
[Odyssey Secret](odyssey.md#secret),
and [OdysseyInstance `.metadata.name`](odyssey-instance.md).

## Fields

### `host`

Host to connect.

### `port`

Port to connect.

### `user`

Credentials to connect from Melete.

* `name` - Username
* `password` - Password

### `defaultDatabase`

Name of the database to connect from Melete.
By default, the value from `user.name` will be taken.

### `replicas`

Array of additional connections to postgres cluster - replica, synchronous replica, replica with lag, etc...

Additional information can be found in [Replicas](../replicas.md).

* `name` - Name of replica. Will be used for create/update [OdysseyInstance](odyssey-instance.md).
* `host` - Host to connect.
* `port` - Port to connect.
