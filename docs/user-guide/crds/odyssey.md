# Odyssey

This CRD contains information of database, username and yandex odyssey route.

## Example

```yaml title="odyssey.yaml" linenums="1"
apiVersion: database.monitorsoft.ru/v1alpha5
kind: Odyssey
metadata:
  name: my-service
  namespace: default
spec:
  password: BefUf2xv
  maxConnections: 30
  extensions:
  - pgcrypto
  deletable: false
  templateDatabase: my-old-service
  existingDatabase:
    name: my-old-service
    user: my-old-service-username
    password: WpsKam3m
  enablePoolReservePreparedStatement: false
  pool: transaction
odysseyInstanceReferences:
- group: database.monitorsoft.ru
  kind: OdysseyInstance
  name: master
- group: database.monitorsoft.ru
  kind: OdysseyInstance
  name: replica
- group: database.monitorsoft.ru
  kind: OdysseyInstance
  name: sync-replica
postgresEndpointReference:
  group: database.monitorsoft.ru
  kind: PostgresEndpoint
  name: postgres-endpoint-default
  namespace: default
```

## Required fields

* `.spec.maxConnections`

## Fields

### `password`

Set password of user for connect to database.
If this field is empty, will be generated random password and stored in `.status.generatedPassword`.

### `maxConnections`

Set max connections for database and user.
[Details](https://github.com/yandex/odyssey/blob/master/documentation/configuration.md#pool_size-integer)

### `extensions`

List of extensions that will be created in database.
If extension does not exist or the list contains duplicate extensions, an error occurs.

### `deletable`

If set in `true`, the database and user will be deleted after the resource is deleted.

### `templateDatabase`

Name of database set as [database template](https://www.postgresql.org/docs/13/manage-ag-templatedbs.html).

After the database is created, a script will be executed to change the ownership of
tables, sequences, views, functions, types, schemas and database.

### `existingDatabase`

Credentials to connect to an existing database.
Needed to use the same database by multiple applications.

### `enablePoolReservePreparedStatement`

Enable yandex odyssey prepared statements feature.
See yandex odyssey [release notes](https://github.com/yandex/odyssey/releases/tag/1.3) for more information.

### `pool`

Poll type. `transaction` or `session`.
[Details](https://github.com/yandex/odyssey/blob/master/documentation/configuration.md#pool-string)

### `odysseyInstanceReferences`

This field contains references of odyssey instances.
All fields are required.

### `postgresEndpointReference`

This field contains references of postgres endpoint.
All fields are required.

If field is empty, Melete will try to find the default [PostgresEndpoint](postgres-endpoint.md)
in the current namespace or in the cluster(if not found in current namespace) and set it.

## Names generation

Odyssey:

* Name: `my-service`
* Namespace: `default`

Secret:

* Name: `my-service-odyssey`

Database:

* Name: `default.my-service`
* User: `default.my-service`
