# CDRs

* [Odyssey](odyssey.md) - Route configuration
* [OdysseyInstance](odyssey-instance.md) - Yandex Odyssey configuration
* [PostgresEndpoint](postgres-endpoint.md) - Information about postgres cluster
