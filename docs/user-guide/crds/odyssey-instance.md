# OdysseyInstance

This CRD contains yandex odyssey configuration.

This resource will be created automatic for all [PostgresEndpoints](postgres-endpoint.md)(master and replicas) in
[Odyssey `.spec.postgresEndpointReference`](odyssey.md#postgresEndpointReference).

## Example

```yaml title="odyssey-instance.yaml" linenums="1"
apiVersion: database.monitorsoft.ru/v1alpha2
kind: OdysseyInstance
metadata:
  name: master
replicas: 3 # Replica count of Deployment
config: ... # Basic config for yandex odyssey
databases: [ ] # List of yandex odyssey routes
```

`.database` will generate automatic based on list of odyssey in kubernetes cluster.

## Required fields

* `.replicas`
* `.config`

## Fields

### `replicas`

Replicas of deployment with yandex odyssey.

### `config`

Config of yandex odyssey.

More information can be found in
[yandex/odyssey repository](https://github.com/yandex/odyssey#configuration-reference).

## Basic config

```text title="odyssey.conf" linenums="1"
include "/etc/odyssey/storages.conf"
include "/etc/odyssey/databases.conf"

log_format "%n %l [%i %s] (%c) %m\n"
log_to_stdout yes
log_debug no
log_config yes
log_session yes
log_query no
log_stats yes
stats_interval 120

workers 1
resolvers 1
readahead 8192
cache_coroutine 0
coroutine_stack_size 8
nodelay yes
keepalive 7200
client_max 1000
listen {
  host "*"
  port 5432
  backlog 128
}
```
