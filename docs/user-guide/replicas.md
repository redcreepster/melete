# Replicas

Create resource [PostgresEndpoint](crds/postgres-endpoint.md) to manage replicas
with the [`replicas`](crds/postgres-endpoint.md#replicas) field.

Each replica must contain name, host, and port.
Example:

```yaml title="replicas.yaml" linenums="1"
replicas:
- name: replica
  host: postgres-replica.postgres.svc
  port: 5432
- name: sync-replica
  host: postgres-sync-replica.postgres.svc
  port: 5432
```

For each replica Melete will add 2 fields in secret with name patterns
`"DB_" + replica_name + "_HOST"` and `"DB_" + replica_name + "_PORT"`.
`replica_name` will be converted to uppercase and replaced by `-` with `_`.
Example:

* `DB_REPLICA_HOST`
* `DB_REPLICA_PORT`
* `DB_SYNC_REPLICA_HOST`
* `DB_SYNC_REPLICA_PORT`
