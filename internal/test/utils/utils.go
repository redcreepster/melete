//go:build test

/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package utils

import (
	"context"
	"strings"

	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/rand"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
)

func RandomName() string {
	chars := "qwertyuiopasdfghjklzxcvbnm"

	var buf strings.Builder

	for i := 0; i < 8; i++ {
		buf.WriteByte(chars[rand.IntnRange(0, len(chars)-1)])
	}

	return buf.String()
}

func GetOdyssey(ctx context.Context, client client.Client, name, namespace string, dst *v1alpha5.Odyssey) func() (v1alpha5.Odyssey, error) {
	return func() (v1alpha5.Odyssey, error) {
		if dst == nil {
			dst = &v1alpha5.Odyssey{}
		}
		lookupKey := types.NamespacedName{Name: name, Namespace: namespace}

		return *dst, client.Get(ctx, lookupKey, dst)
	}
}

func GetOdysseyInstance(ctx context.Context, client client.Client, name string, dst *v1alpha2.OdysseyInstance) func() (v1alpha2.OdysseyInstance, error) {
	return func() (v1alpha2.OdysseyInstance, error) {
		if dst == nil {
			dst = &v1alpha2.OdysseyInstance{}
		}
		lookupKey := types.NamespacedName{Name: name}

		return *dst, client.Get(ctx, lookupKey, dst)
	}
}

func PickError(f func() (v1alpha2.OdysseyInstance, error)) error {
	_, err := f()
	return err
}
