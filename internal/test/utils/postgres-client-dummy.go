//go:build test

/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package utils

import (
	"context"

	"git.monitorsoft.ru/k8s/melete/internal/postgres"
)

var _ postgres.Client = &PostgresClientDummy{}

type PostgresClientDummy struct {
	Credentials postgres.Credentials

	GetExtensionsResult               []string
	GetExtensionsResultError          error
	CreateOrUpdateUserResultError     error
	CreateOrUpdateDatabaseResultError error
	DropUserResultError               error
	DropDatabaseResultError           error
}

func (c *PostgresClientDummy) GetCredentials() postgres.Credentials {
	out := postgres.Credentials{}
	c.Credentials.DeepCopyInto(&out)

	return out
}
func (c *PostgresClientDummy) GetExtensions(_ context.Context) ([]string, error) {
	return c.GetExtensionsResult, c.GetExtensionsResultError
}
func (c *PostgresClientDummy) CreateOrUpdateUser(_ context.Context, _, _ string) error {
	return c.CreateOrUpdateUserResultError
}
func (c *PostgresClientDummy) CreateOrUpdateDatabase(_ context.Context, _, _ string, _ []string, _ *string) error {
	return c.CreateOrUpdateDatabaseResultError
}
func (c *PostgresClientDummy) DropUser(_ context.Context, _ string) error {
	return c.DropUserResultError
}
func (c *PostgresClientDummy) DropDatabase(_ context.Context, _ string) error {
	return c.DropDatabaseResultError
}
func (c *PostgresClientDummy) Close(_ context.Context) {
}

func (c *PostgresClientDummy) CleanValues() {
	c.GetExtensionsResult = nil
	c.GetExtensionsResultError = nil
	c.CreateOrUpdateUserResultError = nil
	c.CreateOrUpdateDatabaseResultError = nil
	c.DropUserResultError = nil
	c.DropDatabaseResultError = nil
}
