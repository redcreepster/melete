/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package webhooks

import (
	"net/http"
	"strconv"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	. "git.monitorsoft.ru/k8s/melete/internal/common"
	. "git.monitorsoft.ru/k8s/melete/internal/test/utils"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
)

var _ = Describe("PostgresEndpoint", func() {
	var name string
	var namespace = "default"
	BeforeEach(func() {
		name = RandomName()
	})

	Describe("Defaulter", func() {
		It("set defaults", func() {
			endpoint := v1alpha1.PostgresEndpoint{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha1.PostgresEndpointSpec{
					PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
						Host: "patroni.postgres.svc",
						Port: Pointer[uint32](5432),
					},
					User: v1alpha1.PostgresEndpointUser{
						Name:     "postgres",
						Password: "some-pass",
					},
					Replicas: []v1alpha1.PostgresEndpointReplica{
						{
							Name: "replica",
							PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
								Host: "patroni-replica.postgres.svc",
								Port: Pointer[uint32](5432),
							},
						},
						{
							Name: "sync-replica",
							PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
								Host: "patroni-sync-replica.postgres.svc",
								Port: Pointer[uint32](5432),
							},
						},
					},
				},
			}

			Expect(k8sClient.Create(ctx, &endpoint)).Should(Succeed())

			Eventually(func() (v1alpha1.PostgresEndpoint, error) {
				endpointLookupKey := types.NamespacedName{Name: name, Namespace: namespace}
				return endpoint, k8sClient.Get(ctx, endpointLookupKey, &endpoint)
			}, 10, 1).Should(HaveField("Spec.DefaultDatabase", HaveValue(Equal(endpoint.Spec.User.Name))))
		})
	})

	Describe("Validator", func() {
		It("Schema errors", func() {
			endpoint := v1alpha1.PostgresEndpoint{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha1.PostgresEndpointSpec{
					PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
						Host: "",
					},
					User: v1alpha1.PostgresEndpointUser{
						Name:     "",
						Password: "",
					},
					Replicas: []v1alpha1.PostgresEndpointReplica{
						{
							Name: "",
							PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
								Host: "",
								Port: nil,
							},
						},
					},
				},
			}

			var causesMatcher = And(
				ContainElement(metav1.StatusCause{
					Type:    `FieldValueInvalid`,
					Message: `Invalid value: "": spec.host in body should be at least 1 chars long`,
					Field:   `spec.host`,
				}),
				ContainElement(metav1.StatusCause{
					Type:    `FieldValueInvalid`,
					Message: `Invalid value: "": spec.user.name in body should be at least 1 chars long`,
					Field:   `spec.user.name`,
				}),
				ContainElement(metav1.StatusCause{
					Type:    `FieldValueInvalid`,
					Message: `Invalid value: "": spec.user.password in body should be at least 1 chars long`,
					Field:   `spec.user.password`,
				}),
			)
			if minorVersion, _ := strconv.Atoi(version.Minor); minorVersion < 24 {
				causesMatcher = And(
					HaveLen(7),
					causesMatcher,
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueInvalid`,
						Message: `Invalid value: "null": spec.port in body must be of type integer: "null"`,
						Field:   `spec.port`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueInvalid`,
						Message: `Invalid value: "": spec.replicas.name in body should be at least 1 chars long`,
						Field:   `spec.replicas.name`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueInvalid`,
						Message: `Invalid value: "": spec.replicas.host in body should be at least 1 chars long`,
						Field:   `spec.replicas.host`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueInvalid`,
						Message: `Invalid value: "null": spec.replicas.port in body must be of type integer: "null"`,
						Field:   `spec.replicas.port`,
					}),
				)
			} else {
				causesMatcher = And(
					HaveLen(7),
					causesMatcher,
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueTypeInvalid`,
						Message: `Invalid value: "null": spec.port in body must be of type integer: "null"`,
						Field:   `spec.port`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    "FieldValueInvalid",
						Message: `Invalid value: "": spec.replicas[0].name in body should be at least 1 chars long`,
						Field:   `spec.replicas[0].name`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    "FieldValueInvalid",
						Message: `Invalid value: "": spec.replicas[0].host in body should be at least 1 chars long`,
						Field:   `spec.replicas[0].host`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    "FieldValueTypeInvalid",
						Message: `Invalid value: "null": spec.replicas[0].port in body must be of type integer: "null"`,
						Field:   `spec.replicas[0].port`,
					}),
				)
			}

			Expect(k8sClient.Create(ctx, &endpoint)).Should(And(
				HaveOccurred(),
				HaveField("ErrStatus.Status", Equal(metav1.StatusFailure)),
				HaveField("ErrStatus.Code", BeEquivalentTo(http.StatusUnprocessableEntity)),
				HaveField("ErrStatus.Reason", Equal(metav1.StatusReasonInvalid)),
				HaveField("ErrStatus.Details", And(
					HaveField("Group", Equal("database.monitorsoft.ru")),
					HaveField("Kind", Equal("PostgresEndpoint")),
					HaveField("Name", Equal(name)),
					HaveField("Causes", causesMatcher),
				)),
			))
		})
	})
})
