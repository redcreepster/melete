/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package webhooks

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	. "git.monitorsoft.ru/k8s/melete/internal/common"
	. "git.monitorsoft.ru/k8s/melete/internal/test/utils"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
)

var _ = Describe("Odyssey", Ordered, func() {
	var name string
	var namespace = "default"
	BeforeAll(func() {
		postgresEndpoints := v1alpha1.PostgresEndpointList{}
		Expect(k8sClient.List(ctx, &postgresEndpoints)).ShouldNot(HaveOccurred())
		for _, postgresEndpoint := range postgresEndpoints.Items {
			if value, ok := postgresEndpoint.Annotations["melete.monitorsoft.ru/is-default-endpoint"]; ok && value == "true" {
				Expect(k8sClient.Delete(ctx, &postgresEndpoint)).ShouldNot(HaveOccurred())
			}
		}

		defaultPostgresEndpoint := v1alpha1.PostgresEndpoint{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "default",
				Namespace: namespace,
				Annotations: map[string]string{
					"melete.monitorsoft.ru/is-default-endpoint": "true",
				},
			},
			Spec: v1alpha1.PostgresEndpointSpec{
				PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
					Host: "patroni.postgres.svc",
					Port: Pointer[uint32](5432),
				},
				User: v1alpha1.PostgresEndpointUser{
					Name:     "postgres",
					Password: "some-pass",
				},
				DefaultDatabase: Pointer("postgres"),
				Replicas: []v1alpha1.PostgresEndpointReplica{
					{
						Name: "replica",
						PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
							Host: "patroni-replica.postgres.svc",
							Port: Pointer[uint32](5432),
						},
					},
				},
			},
		}

		Expect(k8sClient.Create(ctx, &defaultPostgresEndpoint)).Should(Succeed())
	})
	BeforeEach(func() {
		name = RandomName()
	})

	Describe("Defaulter", func() {
		It("set defaults", func() {
			odyssey := v1alpha5.Odyssey{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha5.OdysseySpec{
					MaxConnections: 10,
				},
			}
			Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

			Eventually(func() (v1alpha5.Odyssey, error) {
				lookupKey := types.NamespacedName{Name: name, Namespace: namespace}
				return odyssey, k8sClient.Get(ctx, lookupKey, &odyssey)
			}, 10, 1).Should(And(
				HaveField("ObjectMeta.Finalizers", ContainElement(FinalizerName)),
				HaveField("OdysseyInstanceReferences", And(
					HaveLen(2),
					ContainElement(v1alpha5.OdysseyInstanceReference{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "master",
					}),
					ContainElement(v1alpha5.OdysseyInstanceReference{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "replica",
					}),
				)),
				HaveField("PostgresEndpointReference", HaveValue(Equal(v1alpha5.PostgresEndpointReference{
					Group:     "database.monitorsoft.ru",
					Kind:      "PostgresEndpoint",
					Name:      "default",
					Namespace: namespace,
				}))),
			))
		})
		It("skip deleted", func() {
			// Skip for =>1.25
			if minorVersion, _ := strconv.Atoi(version.Minor); minorVersion < 25 {
				odyssey := v1alpha5.Odyssey{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: "default",
						DeletionTimestamp: &metav1.Time{
							Time: time.Now(),
						},
					},
					Spec: v1alpha5.OdysseySpec{
						MaxConnections: 10,
					},
				}
				Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

				Consistently(func() (v1alpha5.Odyssey, error) {
					lookupKey := types.NamespacedName{Name: name, Namespace: "default"}
					return odyssey, k8sClient.Get(ctx, lookupKey, &odyssey)
				}, 10, 1).Should(And(
					HaveField("ObjectMeta.Finalizers", HaveLen(0)),
				))
			}
		})
	})
	Describe("Validator", func() {
		AfterEach(func() {
			postgresClient.CleanValues()
		})

		Describe("Create", func() {
			It("Schema errors", func() {
				pool := v1alpha5.OdysseyPool("unknown-pool")
				odyssey := v1alpha5.Odyssey{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: "default",
					},
					Spec: v1alpha5.OdysseySpec{
						MaxConnections: 0,
						Extensions: []v1alpha5.Extension{
							"",
						},
						Pool: &pool,
					},
					OdysseyInstanceReferences: []v1alpha5.OdysseyInstanceReference{
						{
							Group: "",
							Kind:  "",
							Name:  "",
						},
						{
							Group: "some-grp",
							Kind:  "some-kind",
							Name:  "",
						},
					},
					PostgresEndpointReference: &v1alpha5.PostgresEndpointReference{
						Group:     "",
						Kind:      "",
						Name:      "",
						Namespace: "",
					},
				}

				var causesMatcher = And(
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueInvalid`,
						Message: `Invalid value: 0: spec.maxConnections in body should be greater than or equal to 1`,
						Field:   `spec.maxConnections`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueNotSupported`,
						Message: `Unsupported value: "unknown-pool": supported values: "session", "transaction"`,
						Field:   `spec.pool`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueInvalid`,
						Message: `Invalid value: "": postgresEndpointReference.name in body should be at least 1 chars long`,
						Field:   `postgresEndpointReference.name`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueInvalid`,
						Message: `Invalid value: "": postgresEndpointReference.namespace in body should be at least 1 chars long`,
						Field:   `postgresEndpointReference.namespace`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueNotSupported`,
						Message: `Unsupported value: "": supported values: "database.monitorsoft.ru"`,
						Field:   `postgresEndpointReference.group`,
					}),
					ContainElement(metav1.StatusCause{
						Type:    `FieldValueNotSupported`,
						Message: `Unsupported value: "": supported values: "PostgresEndpoint"`,
						Field:   `postgresEndpointReference.kind`,
					}),
				)
				if minorVersion, _ := strconv.Atoi(version.Minor); minorVersion < 24 {
					causesMatcher = And(
						HaveLen(10),
						causesMatcher,
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueInvalid`,
							Message: `Invalid value: "": spec.extensions in body should be at least 1 chars long`,
							Field:   `spec.extensions`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueNotSupported`,
							Message: `Unsupported value: "": supported values: "database.monitorsoft.ru"`,
							Field:   `odysseyInstanceReferences.group`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueNotSupported`,
							Message: `Unsupported value: "": supported values: "OdysseyInstance"`,
							Field:   `odysseyInstanceReferences.kind`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueInvalid`,
							Message: `Invalid value: "": odysseyInstanceReferences.name in body should be at least 1 chars long`,
							Field:   `odysseyInstanceReferences.name`,
						}),
					)
				} else {
					causesMatcher = And(
						HaveLen(13),
						causesMatcher,
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueInvalid`,
							Message: `Invalid value: "": spec.extensions[0] in body should be at least 1 chars long`,
							Field:   `spec.extensions[0]`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueNotSupported`,
							Message: `Unsupported value: "": supported values: "database.monitorsoft.ru"`,
							Field:   `odysseyInstanceReferences[0].group`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueNotSupported`,
							Message: `Unsupported value: "": supported values: "OdysseyInstance"`,
							Field:   `odysseyInstanceReferences[0].kind`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueInvalid`,
							Message: `Invalid value: "": odysseyInstanceReferences[0].name in body should be at least 1 chars long`,
							Field:   `odysseyInstanceReferences[0].name`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueNotSupported`,
							Message: `Unsupported value: "some-grp": supported values: "database.monitorsoft.ru"`,
							Field:   `odysseyInstanceReferences[1].group`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueNotSupported`,
							Message: `Unsupported value: "some-kind": supported values: "OdysseyInstance"`,
							Field:   `odysseyInstanceReferences[1].kind`,
						}),
						ContainElement(metav1.StatusCause{
							Type:    `FieldValueInvalid`,
							Message: `Invalid value: "": odysseyInstanceReferences[1].name in body should be at least 1 chars long`,
							Field:   `odysseyInstanceReferences[1].name`,
						}),
					)
				}

				Expect(k8sClient.Create(ctx, &odyssey)).Should(And(
					HaveOccurred(),
					HaveField("ErrStatus.Status", Equal(metav1.StatusFailure)),
					HaveField("ErrStatus.Code", BeEquivalentTo(http.StatusUnprocessableEntity)),
					HaveField("ErrStatus.Reason", Equal(metav1.StatusReasonInvalid)),
					HaveField("ErrStatus.Details", And(
						HaveField("Group", Equal("database.monitorsoft.ru")),
						HaveField("Kind", Equal("Odyssey")),
						HaveField("Name", Equal(name)),
						HaveField("Causes", causesMatcher),
					)),
				))
			})

			It("Unable get extensions", func() {
				odyssey := v1alpha5.Odyssey{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: namespace,
					},
					Spec: v1alpha5.OdysseySpec{
						MaxConnections: 10,
						Extensions: []v1alpha5.Extension{
							"extension1",
						},
					},
				}

				postgresClient.GetExtensionsResultError = errors.New("some error")

				Expect(k8sClient.Create(ctx, &odyssey)).Should(And(
					HaveOccurred(),
					HaveField("ErrStatus.Status", Equal(metav1.StatusFailure)),
					HaveField("ErrStatus.Code", BeEquivalentTo(http.StatusUnprocessableEntity)),
					HaveField("ErrStatus.Reason", Equal(metav1.StatusReasonInvalid)),
					HaveField("ErrStatus.Details", And(
						HaveField("Group", Equal("database.monitorsoft.ru")),
						HaveField("Kind", Equal("Odyssey")),
						HaveField("Name", Equal(name)),
						HaveField("Causes", And(
							HaveLen(1),
							ContainElement(And(
								HaveField("Type", Equal(metav1.CauseType("InternalError"))),
								HaveField("Message", Equal("Internal error: unable get extensions")),
								HaveField("Field", Equal("spec.extensions")),
							)),
						)),
					)),
				))
			})

			It("Extensions errors", func() {
				odyssey := v1alpha5.Odyssey{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: namespace,
					},
					Spec: v1alpha5.OdysseySpec{
						MaxConnections: 10,
						Extensions: []v1alpha5.Extension{
							"extension1",
							"extension1", // Duplicate extension error
							"extension2", // Unexist extension
						},
					},
				}

				postgresClient.GetExtensionsResult = []string{
					"extension1",
				}

				Expect(k8sClient.Create(ctx, &odyssey)).Should(And(
					HaveOccurred(),
					HaveField("ErrStatus.Status", Equal(metav1.StatusFailure)),
					HaveField("ErrStatus.Code", BeEquivalentTo(http.StatusUnprocessableEntity)),
					HaveField("ErrStatus.Reason", Equal(metav1.StatusReasonInvalid)),
					HaveField("ErrStatus.Details", And(
						HaveField("Group", Equal("database.monitorsoft.ru")),
						HaveField("Kind", Equal("Odyssey")),
						HaveField("Name", Equal(name)),
						HaveField("Causes", And(
							HaveLen(3),
							ContainElement(And(
								HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
								HaveField("Message", Equal("Invalid value: \"extension1\": Duplicated extension")),
								HaveField("Field", Equal("spec.extensions[0]")),
							)),
							ContainElement(And(
								HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
								HaveField("Message", Equal("Invalid value: \"extension1\": Duplicated extension")),
								HaveField("Field", Equal("spec.extensions[1]")),
							)),
							ContainElement(And(
								HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
								HaveField("Message", Equal("Invalid value: \"extension2\": Extension not found in database")),
								HaveField("Field", Equal("spec.extensions[2]")),
							)),
						)),
					)),
				))
			})
		})

		Describe("Update", func() {
			It("Schema errors", func() {
				odyssey := v1alpha5.Odyssey{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: "default",
					},
					Spec: v1alpha5.OdysseySpec{
						MaxConnections: 10,
					},
				}

				Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

				odyssey.Spec.MaxConnections = 0
				odyssey.Spec.Extensions = []v1alpha5.Extension{
					"",
				}
				pool := v1alpha5.OdysseyPool("unknown-pool")
				odyssey.Spec.Pool = &pool

				var causesMatcher = And(
					HaveLen(3),
					ContainElement(And(
						HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
						HaveField("Message", Equal("Invalid value: 0: spec.maxConnections in body should be greater than or equal to 1")),
						HaveField("Field", Equal("spec.maxConnections")),
					)),
					ContainElement(And(
						HaveField("Type", Equal(metav1.CauseType("FieldValueNotSupported"))),
						HaveField("Message", Equal("Unsupported value: \"unknown-pool\": supported values: \"session\", \"transaction\"")),
						HaveField("Field", Equal("spec.pool")),
					)),
				)
				if minorVersion, _ := strconv.Atoi(version.Minor); minorVersion < 24 {
					causesMatcher = And(
						causesMatcher,
						ContainElement(And(
							HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
							HaveField("Message", Equal(`Invalid value: "": spec.extensions in body should be at least 1 chars long`)),
							HaveField("Field", Equal(`spec.extensions`)),
						)),
					)
				} else {
					causesMatcher = And(
						causesMatcher,
						ContainElement(And(
							HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
							HaveField("Message", Equal(`Invalid value: "": spec.extensions[0] in body should be at least 1 chars long`)),
							HaveField("Field", Equal(`spec.extensions[0]`)),
						)),
					)
				}
				Expect(k8sClient.Update(ctx, &odyssey)).Should(And(
					HaveOccurred(),
					HaveField("ErrStatus.Status", Equal(metav1.StatusFailure)),
					HaveField("ErrStatus.Code", BeEquivalentTo(http.StatusUnprocessableEntity)),
					HaveField("ErrStatus.Reason", Equal(metav1.StatusReasonInvalid)),
					HaveField("ErrStatus.Details", And(
						HaveField("Group", Equal("database.monitorsoft.ru")),
						HaveField("Kind", Equal("Odyssey")),
						HaveField("Name", Equal(name)),
						HaveField("Causes", causesMatcher),
					)),
				))
			})

			It("Unable get extensions", func() {
				odyssey := v1alpha5.Odyssey{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: "default",
					},
					Spec: v1alpha5.OdysseySpec{
						MaxConnections: 10,
					},
				}

				Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

				odyssey.Spec.Extensions = []v1alpha5.Extension{
					"extension1",
				}

				postgresClient.GetExtensionsResultError = errors.New("some error")

				Expect(k8sClient.Update(ctx, &odyssey)).Should(And(
					HaveOccurred(),
					HaveField("ErrStatus.Status", Equal(metav1.StatusFailure)),
					HaveField("ErrStatus.Code", BeEquivalentTo(http.StatusUnprocessableEntity)),
					HaveField("ErrStatus.Reason", Equal(metav1.StatusReasonInvalid)),
					HaveField("ErrStatus.Details", And(
						HaveField("Group", Equal("database.monitorsoft.ru")),
						HaveField("Kind", Equal("Odyssey")),
						HaveField("Name", Equal(name)),
						HaveField("Causes", And(
							HaveLen(1),
							ContainElement(And(
								HaveField("Type", Equal(metav1.CauseType("InternalError"))),
								HaveField("Message", Equal("Internal error: unable get extensions")),
								HaveField("Field", Equal("spec.extensions")),
							)),
						)),
					)),
				))
			})

			It("Extensions errors", func() {
				odyssey := v1alpha5.Odyssey{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: "default",
					},
					Spec: v1alpha5.OdysseySpec{
						MaxConnections: 10,
					},
				}

				Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

				odyssey.Spec.Extensions = []v1alpha5.Extension{
					"extension1",
					"extension1", // Duplicate extension error
					"extension2", // Unexist extension
				}

				postgresClient.GetExtensionsResult = []string{
					"extension1",
				}

				Expect(k8sClient.Update(ctx, &odyssey)).Should(And(
					HaveOccurred(),
					HaveField("ErrStatus.Status", Equal(metav1.StatusFailure)),
					HaveField("ErrStatus.Code", BeEquivalentTo(http.StatusUnprocessableEntity)),
					HaveField("ErrStatus.Reason", Equal(metav1.StatusReasonInvalid)),
					HaveField("ErrStatus.Details", And(
						HaveField("Group", Equal("database.monitorsoft.ru")),
						HaveField("Kind", Equal("Odyssey")),
						HaveField("Name", Equal(name)),
						HaveField("Causes", And(
							HaveLen(3),
							ContainElement(And(
								HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
								HaveField("Message", Equal("Invalid value: \"extension1\": Duplicated extension")),
								HaveField("Field", Equal("spec.extensions[0]")),
							)),
							ContainElement(And(
								HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
								HaveField("Message", Equal("Invalid value: \"extension1\": Duplicated extension")),
								HaveField("Field", Equal("spec.extensions[1]")),
							)),
							ContainElement(And(
								HaveField("Type", Equal(metav1.CauseType("FieldValueInvalid"))),
								HaveField("Message", Equal("Invalid value: \"extension2\": Extension not found in database")),
								HaveField("Field", Equal("spec.extensions[2]")),
							)),
						)),
					)),
				))
			})
		})
	})
})
