/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package webhooks

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	. "git.monitorsoft.ru/k8s/melete/internal/test/utils"

	"go.uber.org/zap/zapcore"
	admissionv1beta1 "k8s.io/api/admission/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	apiVersion "k8s.io/apimachinery/pkg/version"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha3"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha4"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
	"git.monitorsoft.ru/k8s/melete/internal/postgres"
	"git.monitorsoft.ru/k8s/melete/internal/webhooks"
	//+kubebuilder:scaffold:imports
)

var (
	cfg                          *rest.Config
	k8sClient                    client.Client
	testEnv                      *envtest.Environment
	discoveryClient              *discovery.DiscoveryClient
	version                      *apiVersion.Info
	postgresClient               *PostgresClientDummy
	defaultOdysseyInstanceConfig []byte
	ctx                          context.Context
	cancel                       context.CancelFunc
)

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecs(t, "Webhook Suite")
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(
		zap.WriteTo(GinkgoWriter),
		zap.UseDevMode(true),
		zap.UseFlagOptions(&zap.Options{Level: zapcore.Level(-127)}),
	))

	ctx, cancel = context.WithCancel(context.TODO())

	dir, _ := os.Getwd()
	Expect(os.Chdir(filepath.Join(dir, ".."))).NotTo(HaveOccurred())
	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("..", "..", "config", "crd", "bases")},
		ErrorIfCRDPathMissing: true,
		WebhookInstallOptions: envtest.WebhookInstallOptions{
			Paths: []string{filepath.Join("..", "..", "config", "webhook")},
		},
	}

	var err error
	// cfg is defined in this file globally.
	cfg, err = testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	discoveryClient, err = discovery.NewDiscoveryClientForConfig(cfg)
	Expect(err).NotTo(HaveOccurred())

	version, err = discoveryClient.ServerVersion()
	Expect(err).NotTo(HaveOccurred())

	scheme := runtime.NewScheme()

	err = v1alpha1.AddToScheme(scheme)
	Expect(err).NotTo(HaveOccurred())

	err = v1alpha2.AddToScheme(scheme)
	Expect(err).NotTo(HaveOccurred())

	err = v1alpha3.AddToScheme(scheme)
	Expect(err).NotTo(HaveOccurred())

	err = v1alpha4.AddToScheme(scheme)
	Expect(err).NotTo(HaveOccurred())

	err = v1alpha5.AddToScheme(scheme)
	Expect(err).NotTo(HaveOccurred())

	err = admissionv1beta1.AddToScheme(scheme)
	Expect(err).NotTo(HaveOccurred())

	//+kubebuilder:scaffold:scheme

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	// start webhook server using Manager
	webhookInstallOptions := &testEnv.WebhookInstallOptions
	mgr, err := ctrl.NewManager(cfg, ctrl.Options{
		Scheme:             scheme,
		Host:               webhookInstallOptions.LocalServingHost,
		Port:               webhookInstallOptions.LocalServingPort,
		CertDir:            webhookInstallOptions.LocalServingCertDir,
		LeaderElection:     false,
		MetricsBindAddress: "0",
	})
	Expect(err).NotTo(HaveOccurred())

	defaultOdysseyInstanceConfig = []byte(`some-default-config`)
	postgresClient = &PostgresClientDummy{}

	webhooks.NewPostgresClient = func(credentials postgres.Credentials) postgres.Client {
		return postgresClient
	}

	err = webhooks.SetupPostgresEndpointWebhookWithManager(mgr)
	Expect(err).NotTo(HaveOccurred())

	err = webhooks.SetupOdysseyInstanceWebhookWithManager(mgr, defaultOdysseyInstanceConfig)
	Expect(err).NotTo(HaveOccurred())

	err = webhooks.SetupOdysseyWebhookWithManager(mgr)
	Expect(err).NotTo(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = mgr.Start(ctx)
		Expect(err).NotTo(HaveOccurred())
	}()

	// wait for the webhook server to get ready
	dialer := &net.Dialer{Timeout: time.Second}
	addrPort := fmt.Sprintf("%s:%d", webhookInstallOptions.LocalServingHost, webhookInstallOptions.LocalServingPort)
	Eventually(func() error {
		conn, err := tls.DialWithDialer(dialer, "tcp", addrPort, &tls.Config{InsecureSkipVerify: true})
		if err != nil {
			return err
		}
		//goland:noinspection GoUnhandledErrorResult
		conn.Close()
		return nil
	}).Should(Succeed())
})

var _ = AfterSuite(func() {
	cancel()
	By("tearing down the test environment")
	err := testEnv.Stop()
	Expect(err).NotTo(HaveOccurred())
})
