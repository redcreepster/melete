/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package webhooks

import (
	"strconv"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	. "git.monitorsoft.ru/k8s/melete/internal/common"
	. "git.monitorsoft.ru/k8s/melete/internal/test/utils"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
)

var _ = Describe("Storage", func() {
	Describe("Hash", func() {
		It("Without optional", func() {
			storage := v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: nil,
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}

			Expect(storage.Hash()).Should(Equal("55e842d6387653a2081bcc3e44adbfc9739db9767b218dd214342a75021deeaf"))
		})
		It("With optional", func() {
			storage := v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     Pointer("mons"),
				Password: Pointer("some-pass"),
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}

			Expect(storage.Hash()).Should(Equal("91d78557cf31214001c1d51197c9e6ec5b51e2985274c86400b8a708613719ca"))
		})
	})

	Describe("Equal", func() {
		It("Without optional", func() {
			storage := v1alpha2.Storage{
				Host: "patoni.postgres.svc",
				Port: 5432,
			}

			Expect(storage.Equal(v1alpha2.Storage{
				Host: "patoni.postgres.svc",
				Port: 5432,
			})).Should(BeTrue())

			Expect(storage.Equal(v1alpha2.Storage{
				Host: "patoni.postgres.svc",
				Port: 5431,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Host: "some-name.postgres.svc",
				Port: 5432,
			})).Should(BeFalse())
		})
		It("With optional", func() {
			storage := v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     Pointer("mons"),
				Password: Pointer("some-pass"),
				Host:     "patoni.postgres.svc",
				Port:     5432,
			}

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     Pointer("mons"),
				Password: Pointer("some-pass"),
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeTrue())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("test"),
				User:     Pointer("mons"),
				Password: Pointer("some-pass"),
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     Pointer("test"),
				Password: Pointer("some-pass"),
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     Pointer("mons"),
				Password: Pointer("test"),
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     Pointer("mons"),
				Password: Pointer("some-pass"),
				Host:     "test",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     Pointer("mons"),
				Password: Pointer("some-pass"),
				Host:     "patoni.postgres.svc",
				Port:     5431,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     nil,
				User:     Pointer("mons"),
				Password: Pointer("some-pass"),
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     nil,
				Password: Pointer("some-pass"),
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     Pointer("mons"),
				Password: nil,
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: Pointer("some-pass"),
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     Pointer("aspwdm"),
				User:     nil,
				Password: nil,
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())

			Expect(storage.Equal(v1alpha2.Storage{
				Name:     nil,
				User:     Pointer("mons"),
				Password: nil,
				Host:     "patoni.postgres.svc",
				Port:     5432,
			})).Should(BeFalse())
		})
	})
})

var _ = Describe("OdysseyInstance", func() {
	var replicas int32 = 3
	var defaultConfig = `some-default-config`
	var name string
	BeforeEach(func() {
		name = RandomName()
	})

	Describe("Defaulter", func() {
		It("set defaults", func() {
			instance := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Replicas: Pointer(replicas),
			}
			Expect(k8sClient.Create(ctx, &instance)).Should(Succeed())

			Eventually(func() (v1alpha2.OdysseyInstance, error) {
				lookupKey := types.NamespacedName{Name: name}
				return instance, k8sClient.Get(ctx, lookupKey, &instance)
			}, 10, 1).Should(And(
				HaveField("ObjectMeta.Finalizers", ContainElement(FinalizerName)),
				HaveField("ObjectMeta.Labels", HaveKeyWithValue("app.kubernetes.io/managed-by", "melete")),
				HaveField("Config", HaveValue(Equal(defaultConfig))),
			))
		})
		It("skip deleted", func() {
			// Skip for =>1.25
			if minorVersion, _ := strconv.Atoi(version.Minor); minorVersion < 25 {
				instance := v1alpha2.OdysseyInstance{
					ObjectMeta: metav1.ObjectMeta{
						Name: name,
						DeletionTimestamp: &metav1.Time{
							Time: time.Now(),
						},
					},
					Replicas: Pointer(replicas),
				}
				Expect(k8sClient.Create(ctx, &instance)).Should(Succeed())

				Consistently(func() (v1alpha2.OdysseyInstance, error) {
					lookupKey := types.NamespacedName{Name: name}
					return instance, k8sClient.Get(ctx, lookupKey, &instance)
				}, 10, 1).Should(And(
					HaveField("ObjectMeta.Finalizers", HaveLen(0)),
					HaveField("ObjectMeta.Labels", HaveLen(0)),
					HaveField("Config", BeNil()),
				))
			}
		})
	})
})
