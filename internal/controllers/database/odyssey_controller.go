/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package database

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database"
	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"github.com/jackc/pgconn"
	v1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
	"git.monitorsoft.ru/k8s/melete/internal/postgres"
	"git.monitorsoft.ru/k8s/melete/internal/webhooks"
)

var (
	NewPostgresClient = postgres.NewPostgresClient
)

// OdysseyReconciler reconciles an Odyssey object
type OdysseyReconciler struct {
	client.Client
	Scheme *runtime.Scheme

	OdysseySupervisorServiceName      string
	OdysseySupervisorServiceNamespace string
	OdysseyInstanceReplicas           int32
}

//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odyssey,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odyssey/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odyssey/finalizers,verbs=update

//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odysseyinstances,verbs=get;list;create;update;patch;delete
//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odysseyinstances/status,verbs=get;update;patch

//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=postgres-endpoints,verbs=get;list;watch

//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
func (r *OdysseyReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	logger.Info("new request")

	var odyssey v1alpha5.Odyssey
	if err := r.Get(ctx, req.NamespacedName, &odyssey); err != nil {
		if !apierrors.IsNotFound(err) {
			logger.Error(err, "unable to fetch Odyssey")

			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	logger = logger.WithValues("phase", odyssey.Status.Phase)
	logger.V(5).Info("Phase")

	var secret = v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-odyssey", req.Name),
			Namespace: req.Namespace,
		},
	}

	if odyssey.ObjectMeta.DeletionTimestamp.IsZero() {
		// Fallback after update melete version
		if odyssey.PostgresEndpointReference == nil || odyssey.OdysseyInstanceReferences == nil {
			patch := odyssey.DeepCopy()

			defaulter := webhooks.NewOdysseyDefaulter(r.Client)
			if err := defaulter.Default(ctx, patch); err != nil {
				logger.Error(err, "unable to apply defaulter Odyssey")

				return ctrl.Result{}, err
			}

			if err := r.Patch(ctx, patch, client.MergeFrom(&odyssey)); err != nil {
				if !apierrors.IsNotFound(err) {
					logger.Error(err, "unable to update Odyssey")

					return ctrl.Result{}, err
				}

				return ctrl.Result{}, nil
			}

			return ctrl.Result{}, nil
		}

		switch odyssey.Status.Phase {
		case v1alpha5.OdysseyStatusPhaseNone:
			patch := odyssey.DeepCopy()

			patch.Status.Phase = v1alpha5.OdysseyStatusPhasePending

			if err := r.Status().Patch(ctx, patch, client.MergeFrom(&odyssey)); err != nil {
				if !apierrors.IsNotFound(err) {
					logger.Error(err, "unable to update Odyssey status")

					return ctrl.Result{}, err
				}

				return ctrl.Result{}, nil
			}

			return ctrl.Result{}, nil
		case v1alpha5.OdysseyStatusPhasePending:
			return r.firstInit(ctx, odyssey)
		case v1alpha5.OdysseyStatusPhaseReady:
			// Generate GeneratedPassword when Password and GeneratedPassword was not provided
			if odyssey.GetSpecifiedOrGeneratedPassword() == "" {
				patch := odyssey.DeepCopy()

				patch.Status.Phase = v1alpha5.OdysseyStatusPhaseWaitPostgres
				patch.Status.GeneratedPassword = Pointer(GeneratePassword(GeneratedPasswordLength))

				if err := r.Status().Patch(ctx, patch, client.MergeFrom(&odyssey)); err != nil {
					logger.Error(err, "unable to update Odyssey status")

					return ctrl.Result{}, err
				}

				return ctrl.Result{}, nil
			}

			// Remove GeneratedPassword when Password was provided
			if PointerDeref(odyssey.Spec.Password, "") != "" && PointerDeref(odyssey.Status.GeneratedPassword, "") != "" {
				patch := odyssey.DeepCopy()

				patch.Status.Phase = v1alpha5.OdysseyStatusPhaseWaitPostgres
				patch.Status.GeneratedPassword = nil

				if err := r.Status().Patch(ctx, patch, client.MergeFrom(&odyssey)); err != nil {
					logger.Error(err, "unable to update Odyssey status")

					return ctrl.Result{}, err
				}

				return ctrl.Result{}, nil
			}
			fallthrough
		case v1alpha5.OdysseyStatusPhaseWaitPostgres:
			if odyssey.PostgresEndpointReference == nil {
				return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, nil
			}

			lookupKey := types.NamespacedName{
				Namespace: odyssey.PostgresEndpointReference.Namespace,
				Name:      odyssey.PostgresEndpointReference.Name,
			}
			var postgresEndpoint v1alpha1.PostgresEndpoint
			if err := r.Client.Get(ctx, lookupKey, &postgresEndpoint); err != nil {
				logger.Error(err, "unable get postgres endpoint")

				return ctrl.Result{}, err
			}

			postgresClient := NewPostgresClient(postgres.Credentials{
				Host:     postgresEndpoint.Spec.Host,
				Port:     uint16(*postgresEndpoint.Spec.Port),
				Username: postgresEndpoint.Spec.User.Name,
				Password: postgresEndpoint.Spec.User.Password,
			})
			defer postgresClient.Close(ctx)

			databaseName := odyssey.GetDatabaseName()
			databasePassword := odyssey.GetSpecifiedOrGeneratedPassword()

			if err := postgresClient.CreateOrUpdateUser(ctx, databaseName, databasePassword); err != nil {
				var pgErr *pgconn.PgError
				switch {
				case postgres.IsCredentialsEmptyError(err):
					return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, nil
				// role "%s" already exists
				case errors.As(err, &pgErr) && pgErr.SQLState() == "42710":
					logger.V(10).Info("user creating skipped because exists")
				default:
					logger.WithValues("user", databaseName).Error(err, "unable create or update user")

					return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
				}
			}

			extensions := make([]string, len(odyssey.Spec.Extensions))
			for i := 0; i < len(odyssey.Spec.Extensions); i++ {
				extensions[i] = string(odyssey.Spec.Extensions[i])
			}

			if err := postgresClient.CreateOrUpdateDatabase(
				ctx,
				databaseName,
				databaseName,
				extensions,
				odyssey.Spec.TemplateDatabase,
			); err != nil {
				var pgErr *pgconn.PgError
				switch {
				case postgres.IsCredentialsEmptyError(err):
					return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, nil
				// extension not found
				case errors.As(err, &pgErr) && pgErr.SQLState() == "58P01":
					return ctrl.Result{}, err
				// 23505
				// duplicate key value violates unique constraint "pg_database_datname_index"
				// Key (datname)=(%s) already exists.
				// 42P04
				// database "%s" already exists
				case errors.As(err, &pgErr) && (pgErr.SQLState() == "23505" || pgErr.SQLState() == "42P04"):
					logger.V(10).Info("database creating skipped because exists")
				default:
					logger.WithValues("database", databaseName).Error(err, "unable create database")

					return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
				}
			}

			// Early close postgres connection
			postgresClient.Close(ctx)

			for _, odysseyInstanceReference := range odyssey.OdysseyInstanceReferences {
				odysseyInstance := v1alpha2.OdysseyInstance{
					ObjectMeta: metav1.ObjectMeta{
						Name: odysseyInstanceReference.Name,
					},
					Replicas: Pointer(r.OdysseyInstanceReplicas),
				}

				var hostPort = postgresEndpoint.GetHostPortByName(odysseyInstanceReference.Name)
				if hostPort == nil {
					logger.Info("replica with name " + odysseyInstance.Name + " not found")

					// Skip unexists replica
					continue
				}
				if _, err := controllerutil.CreateOrPatch(ctx, r.Client, &odysseyInstance, func() error {
					odysseyInstance.Status.Phase = v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload

					var odysseyDatabase = v1alpha2.Database{
						Name: odyssey.GetDatabaseName(),
					}

					database.OdysseyToDatabase(odyssey, *hostPort, &odysseyDatabase)

					odysseyInstance.AddDatabase(&odysseyDatabase)

					return nil
				}); err != nil {
					return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
				}
			}

			if _, err := controllerutil.CreateOrPatch(ctx, r.Client, &secret, r.provideSecret(odyssey, &secret, odyssey.OdysseyInstanceReferences)); err != nil {
				return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
			}

			patch := odyssey.DeepCopy()
			patch.Status.Phase = v1alpha5.OdysseyStatusPhaseReady

			if err := r.Status().Patch(ctx, patch, client.MergeFrom(&odyssey)); err != nil {
				logger.Error(err, "unable to update Odyssey status")

				return ctrl.Result{}, err
			}
		case v1alpha5.OdysseyStatusPhasePendingDelete:
			return ctrl.Result{}, nil
		default:
			err := errors.New("unknown phase")
			logger.WithValues("phase", odyssey.Status.Phase).
				Error(err, "unknown phase")

			return ctrl.Result{}, err
		}
	} else {
		return r.finalize(ctx, odyssey, secret)
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *OdysseyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1alpha5.Odyssey{}).
		Owns(&v1.Secret{}).
		Owns(&v1alpha2.OdysseyInstance{}).
		Complete(r)
}

func (r *OdysseyReconciler) provideSecret(odyssey v1alpha5.Odyssey, secret *v1.Secret, references []v1alpha5.OdysseyInstanceReference) controllerutil.MutateFn {
	return func() error {
		databaseName := odyssey.GetDatabaseName()
		databasePassword := odyssey.GetSpecifiedOrGeneratedPassword()

		if err := controllerutil.SetControllerReference(&odyssey, secret, r.Scheme); err != nil {
			return err
		}

		secret.Type = v1.SecretTypeOpaque

		if secret.Labels == nil {
			secret.Labels = make(map[string]string)
		}

		secret.Labels["app.kubernetes.io/managed-by"] = "melete"

		if secret.Data == nil {
			secret.Data = make(map[string][]byte, len(references)+3)
		}
		for _, reference := range references {
			var databaseHost string
			var databasePort int
			var hostKey string
			var portKey string
			if reference.Name == "master" {
				databaseHost = fmt.Sprintf("%s.%s.svc", r.OdysseySupervisorServiceName, r.OdysseySupervisorServiceNamespace)
				databasePort = 5432
				hostKey = "DB_HOST"
				portKey = "DB_PORT"
			} else {
				hostSuffix := strings.ToLower(reference.Name)
				hostSuffix = strings.ReplaceAll(hostSuffix, "_", "-")
				databaseHost = fmt.Sprintf("%s-%s.%s.svc", r.OdysseySupervisorServiceName, hostSuffix, r.OdysseySupervisorServiceNamespace)
				databasePort = 5432
				keyName := strings.ToUpper(reference.Name)
				keyName = strings.ReplaceAll(keyName, "-", "_")
				hostKey = "DB_" + keyName + "_HOST"
				portKey = "DB_" + keyName + "_PORT"
			}

			secret.Data[hostKey] = []byte(databaseHost)
			secret.Data[portKey] = []byte(strconv.Itoa(databasePort))
		}
		secret.Data["DB_NAME"] = []byte(databaseName)
		secret.Data["DB_USERNAME"] = []byte(databaseName)
		secret.Data["DB_PASSWORD"] = []byte(databasePassword)

		return nil
	}
}

func (r *OdysseyReconciler) firstInit(ctx context.Context, odyssey v1alpha5.Odyssey) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	patch := odyssey.DeepCopy()

	patch.Status.Phase = v1alpha5.OdysseyStatusPhaseWaitPostgres

	if odyssey.GetSpecifiedOrGeneratedPassword() == "" {
		patch.Status.GeneratedPassword = Pointer(GeneratePassword(GeneratedPasswordLength))
	}

	if err := r.Status().Patch(ctx, patch, client.MergeFrom(&odyssey)); err != nil {
		logger.Error(err, "unable to update Odyssey status")

		return ctrl.Result{}, err
	}
	odyssey = *patch

	return ctrl.Result{}, nil
}

func (r *OdysseyReconciler) finalize(ctx context.Context, odyssey v1alpha5.Odyssey, secret v1.Secret) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	if controllerutil.ContainsFinalizer(&odyssey, FinalizerName) {
		for _, odysseyInstanceReference := range odyssey.OdysseyInstanceReferences {
			var odysseyInstance v1alpha2.OdysseyInstance

			if err := r.Get(ctx, types.NamespacedName{Name: odysseyInstanceReference.Name}, &odysseyInstance); err != nil {
				if apierrors.IsNotFound(err) {
					continue
				}

				logger.Error(err, "unable to fetch OdysseyInstance")

				return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
			}

			if _, err := controllerutil.CreateOrPatch(ctx, r.Client, &odysseyInstance, func() error {
				odysseyInstance.Status.Phase = v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload

				databaseName := odyssey.GetDatabaseName()

				odysseyInstance.RemoveDatabase(databaseName)

				return nil
			}); err != nil {
				return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
			}
		}

		if err := r.Delete(ctx, &secret); err != nil {
			if !apierrors.IsNotFound(err) {
				logger.Error(err, "unable to delete Secret")

				return ctrl.Result{}, err
			}
		}

		if PointerDeref(odyssey.Spec.Deletable, false) && odyssey.PostgresEndpointReference != nil {
			lookupKey := types.NamespacedName{
				Namespace: odyssey.PostgresEndpointReference.Namespace,
				Name:      odyssey.PostgresEndpointReference.Name,
			}
			var postgresEndpoint v1alpha1.PostgresEndpoint
			if err := r.Client.Get(ctx, lookupKey, &postgresEndpoint); err == nil {
				postgresClient := NewPostgresClient(postgres.Credentials{
					Host:     postgresEndpoint.Spec.Host,
					Port:     uint16(*postgresEndpoint.Spec.Port),
					Username: postgresEndpoint.Spec.User.Name,
					Password: postgresEndpoint.Spec.User.Password,
				})
				defer postgresClient.Close(ctx)

				databaseName := odyssey.GetDatabaseName()

				if err := postgresClient.DropDatabase(ctx, databaseName); err != nil {
					var pgErr *pgconn.PgError
					switch {
					case postgres.IsCredentialsEmptyError(err):
						return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, nil
					// database "%s" does not exist
					case errors.As(err, &pgErr) && pgErr.SQLState() == "3D000":
						//goland:noinspection SqlNoDataSourceInspection
						logger.V(10).Info("drop database skipping because does not exists")
					default:
						logger.Error(err, "unable drop database")

						return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
					}
				}

				if err := postgresClient.DropUser(ctx, databaseName); err != nil {
					var pgErr *pgconn.PgError
					switch {
					case postgres.IsCredentialsEmptyError(err):
						return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, nil
					// role "%s" does not exist
					case errors.As(err, &pgErr) && pgErr.SQLState() == "42704":
						logger.V(10).Info("drop user skipping because does not exists")
					default:
						logger.Error(err, "unable drop user")

						return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
					}
				}

				// Skip unexists replica
				postgresClient.Close(ctx)
			}
		}

		// remove our finalizer from the list and update it.
		patch := odyssey.DeepCopy()

		patch.Status.Phase = v1alpha5.OdysseyStatusPhasePendingDelete

		if err := r.Status().Patch(ctx, patch, client.MergeFrom(&odyssey)); err != nil {
			if !apierrors.IsNotFound(err) {
				logger.Error(err, "unable to update Odyssey status")

				return ctrl.Result{}, err
			}
		}

		controllerutil.RemoveFinalizer(patch, FinalizerName)
		if err := r.Patch(ctx, patch, client.MergeFrom(&odyssey)); err != nil {
			if !apierrors.IsNotFound(err) {
				logger.Error(err, "unable to update Odyssey")

				return ctrl.Result{}, err
			}
		}
	}

	return ctrl.Result{}, nil
}
