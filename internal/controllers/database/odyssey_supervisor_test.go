package database

import (
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"k8s.io/utils/pointer"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
)

var _ = Describe("GenerateStoragesConf", func() {
	Describe("Single database", func() {
		It("Without optional", func() {
			storage1 := v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: nil,
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}
			database1 := v1alpha2.Database{
				Name:     "some-name",
				Password: "some-pass",
				User:     v1alpha2.User{},
				Storage:  storage1,
			}
			databases := []v1alpha2.Database{
				database1,
			}

			expected := `storage "` + storage1.Hash() + `" {
	type "remote"
	host "patroni.postgres.svc"
	port 5432
}
`
			Expect(GenerateStoragesConf(databases)).Should(Equal(expected))
		})
		It("With optional", func() {
			storage1 := v1alpha2.Storage{
				Name:     pointer.String("aspwdm"),
				User:     pointer.String("mons"),
				Password: pointer.String("some-pass"),
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}
			database1 := v1alpha2.Database{
				Name:     "some-name",
				Password: "some-pass",
				User:     v1alpha2.User{},
				Storage:  storage1,
			}
			databases := []v1alpha2.Database{
				database1,
			}

			expected := `storage "` + storage1.Hash() + `" {
	type "remote"
	host "patroni.postgres.svc"
	port 5432
}
`
			Expect(GenerateStoragesConf(databases)).Should(Equal(expected))
		})
	})
	Describe("Multiple databases", func() {
		It("Equals storages", func() {
			storage1 := v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: nil,
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}
			database1 := v1alpha2.Database{
				Name:     "some-name",
				Password: "some-pass",
				User:     v1alpha2.User{},
				Storage:  storage1,
			}
			storage2 := v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: nil,
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}
			database2 := v1alpha2.Database{
				Name:     "some-name2",
				Password: "some-pass2",
				User:     v1alpha2.User{},
				Storage:  storage2,
			}
			databases := []v1alpha2.Database{
				database1,
				database2,
			}

			expected := `storage "` + storage1.Hash() + `" {
	type "remote"
	host "patroni.postgres.svc"
	port 5432
}
`
			Expect(GenerateStoragesConf(databases)).Should(Equal(expected))
		})
		It("Different storages", func() {
			storage1 := v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: nil,
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}
			database1 := v1alpha2.Database{
				Name:     "some-name",
				Password: "some-pass",
				User:     v1alpha2.User{},
				Storage:  storage1,
			}
			storage2 := v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: nil,
				Host:     "patroni-replica.postgres.svc",
				Port:     6432,
			}
			database2 := v1alpha2.Database{
				Name:     "some-name2",
				Password: "some-pass2",
				User:     v1alpha2.User{},
				Storage:  storage2,
			}
			databases := []v1alpha2.Database{
				database1,
				database2,
			}

			expected := `storage "` + storage1.Hash() + `" {
	type "remote"
	host "patroni.postgres.svc"
	port 5432
}
storage "` + storage2.Hash() + `" {
	type "remote"
	host "patroni-replica.postgres.svc"
	port 6432
}
`
			Expect(GenerateStoragesConf(databases)).Should(Equal(expected))
		})
	})
})

var _ = Describe("GenerateDatabasesConf", func() {
	Describe("Single database", func() {
		It("Without optional", func() {
			storage1 := v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: nil,
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}
			database1 := v1alpha2.Database{
				Name:     "some-name",
				Password: "some-pass",
				User: v1alpha2.User{
					Authentication:               v1alpha2.AuthenticationMD5,
					Pool:                         v1alpha2.PoolTransaction,
					ClientMax:                    10,
					PoolTTL:                      1 * time.Minute,
					PoolDiscard:                  false,
					PoolCancel:                   true,
					ApplicationNameAddHost:       true,
					ClientForwardError:           true,
					ServerLifetime:               1 * time.Hour,
					LogDebug:                     false,
					PoolReservePreparedStatement: false,
					Quantiles:                    "0.99,0.95,0.5",
				},
				Storage: storage1,
			}
			databases := []v1alpha2.Database{
				database1,
			}

			expected := `database "some-name" {
	user "some-name" {
		authentication "md5"
		storage "` + storage1.Hash() + `"
		pool "transaction"
		password "some-pass"
		client_max 10
		pool_ttl 60
		pool_discard no
		pool_cancel yes
		application_name_add_host yes
		client_fwd_error yes
		server_lifetime 3600
		log_debug no
		pool_reserve_prepared_statement no
		quantiles "0.99,0.95,0.5"
	}
}
`
			Expect(GenerateDatabasesConf(databases)).Should(Equal(expected))
		})
		It("With optional", func() {
			storage1 := v1alpha2.Storage{
				Name:     pointer.String("aspwdm"),
				User:     pointer.String("mons"),
				Password: pointer.String("some-pass"),
				Host:     "patroni-replica.postgres.svc",
				Port:     6432,
			}
			database1 := v1alpha2.Database{
				Name:     "some-name-x",
				Password: "some-pass-x",
				User: v1alpha2.User{
					Authentication:               v1alpha2.AuthenticationNone,
					Pool:                         v1alpha2.PoolSession,
					ClientMax:                    20,
					PoolTTL:                      2 * time.Minute,
					PoolDiscard:                  true,
					PoolCancel:                   false,
					ApplicationNameAddHost:       false,
					ClientForwardError:           false,
					ServerLifetime:               3 * time.Hour,
					LogDebug:                     true,
					PoolReservePreparedStatement: true,
					Quantiles:                    "0.99,0.98,0.5",
				},
				Storage: storage1,
			}
			databases := []v1alpha2.Database{
				database1,
			}

			expected := `database "some-name-x" {
	user "some-name-x" {
		authentication "none"
		storage "` + storage1.Hash() + `"
		storage_db "aspwdm"
		storage_user "mons"
		storage_password "some-pass"
		pool "session"
		password "some-pass-x"
		client_max 20
		pool_ttl 120
		pool_discard yes
		pool_cancel no
		application_name_add_host no
		client_fwd_error no
		server_lifetime 10800
		log_debug yes
		pool_reserve_prepared_statement yes
		quantiles "0.99,0.98,0.5"
	}
}
`
			Expect(GenerateDatabasesConf(databases)).Should(Equal(expected))
		})
	})
	Describe("Multiple database", func() {
		It("Equals storages", func() {
			storage1 := v1alpha2.Storage{
				Name:     nil,
				User:     nil,
				Password: nil,
				Host:     "patroni.postgres.svc",
				Port:     5432,
			}
			database1 := v1alpha2.Database{
				Name:     "some-name",
				Password: "some-pass",
				User: v1alpha2.User{
					Authentication:               v1alpha2.AuthenticationMD5,
					Pool:                         v1alpha2.PoolTransaction,
					ClientMax:                    10,
					PoolTTL:                      1 * time.Minute,
					PoolDiscard:                  false,
					PoolCancel:                   true,
					ApplicationNameAddHost:       true,
					ClientForwardError:           true,
					ServerLifetime:               1 * time.Hour,
					LogDebug:                     false,
					PoolReservePreparedStatement: false,
					Quantiles:                    "0.99,0.95,0.5",
				},
				Storage: storage1,
			}
			storage2 := v1alpha2.Storage{
				Name:     pointer.String("aspwdm"),
				User:     pointer.String("mons"),
				Password: pointer.String("some-pass"),
				Host:     "patroni-replica.postgres.svc",
				Port:     6432,
			}
			database2 := v1alpha2.Database{
				Name:     "some-name-x",
				Password: "some-pass-x",
				User: v1alpha2.User{
					Authentication:               v1alpha2.AuthenticationNone,
					Pool:                         v1alpha2.PoolSession,
					ClientMax:                    20,
					PoolTTL:                      2 * time.Minute,
					PoolDiscard:                  true,
					PoolCancel:                   false,
					ApplicationNameAddHost:       false,
					ClientForwardError:           false,
					ServerLifetime:               3 * time.Hour,
					LogDebug:                     true,
					PoolReservePreparedStatement: true,
					Quantiles:                    "0.99,0.98,0.5",
				},
				Storage: storage2,
			}
			databases := []v1alpha2.Database{
				database1,
				database2,
			}

			expected := `database "some-name" {
	user "some-name" {
		authentication "md5"
		storage "` + storage1.Hash() + `"
		pool "transaction"
		password "some-pass"
		client_max 10
		pool_ttl 60
		pool_discard no
		pool_cancel yes
		application_name_add_host yes
		client_fwd_error yes
		server_lifetime 3600
		log_debug no
		pool_reserve_prepared_statement no
		quantiles "0.99,0.95,0.5"
	}
}
database "some-name-x" {
	user "some-name-x" {
		authentication "none"
		storage "` + storage2.Hash() + `"
		storage_db "aspwdm"
		storage_user "mons"
		storage_password "some-pass"
		pool "session"
		password "some-pass-x"
		client_max 20
		pool_ttl 120
		pool_discard yes
		pool_cancel no
		application_name_add_host no
		client_fwd_error no
		server_lifetime 10800
		log_debug yes
		pool_reserve_prepared_statement yes
		quantiles "0.99,0.98,0.5"
	}
}
`
			Expect(GenerateDatabasesConf(databases)).Should(Equal(expected))
		})
	})
})
