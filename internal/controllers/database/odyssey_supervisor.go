/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package database

import (
	"context"
	"fmt"
	"strings"
	"time"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/postgres"
)

// OdysseySupervisor reconciles a OdysseyInstance object
type OdysseySupervisor struct {
	client.Client
	Scheme *runtime.Scheme

	InstanceName string

	Executor postgres.OdysseyExecutor
}

//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odysseyinstances,verbs=get;list;watch
//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odysseyinstances/status,verbs=get

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
func (r *OdysseySupervisor) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	logger.Info("new request")

	var instance v1alpha2.OdysseyInstance
	if err := r.Get(ctx, req.NamespacedName, &instance); err != nil {
		if !apierrors.IsNotFound(err) {
			logger.Error(err, "unable to fetch OdysseyInstance")

			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	logger = logger.WithValues("phase", instance.Status.Phase)
	logger.V(5).Info("Phase")

	if instance.GetDeletionTimestamp().IsZero() {
		switch instance.Status.Phase {
		case v1alpha2.OdysseyInstanceStatusPhaseReady, v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload:
			storagesConf := GenerateStoragesConf(instance.Databases)
			databasesConf := GenerateDatabasesConf(instance.Databases)

			if err := r.Executor.ReloadConfig(*instance.Config, storagesConf, databasesConf); err != nil {
				return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
			}

			return ctrl.Result{}, nil
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *OdysseySupervisor) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1alpha2.OdysseyInstance{}).
		WithEventFilter(predicate.NewPredicateFuncs(func(object client.Object) bool {
			if object.GetName() != r.InstanceName {
				return false
			}

			instance, ok := object.(*v1alpha2.OdysseyInstance)
			if !ok {
				return false
			}

			switch instance.Status.Phase {
			case
				v1alpha2.OdysseyInstanceStatusPhaseReady,
				v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload:
				return true
			default:
				return false
			}
		})).
		Complete(r)
}

func GenerateStoragesConf(databases v1alpha2.Databases) string {
	var buf strings.Builder

	var uniqueStorages []v1alpha2.Storage
	for _, database := range databases {
		var found = false
		for _, storage := range uniqueStorages {
			if storage.Equal(database.Storage) {
				found = true

				break
			}
		}
		if !found {
			uniqueStorages = append(uniqueStorages, database.Storage)
		}
	}

	for _, storage := range uniqueStorages {
		buf.WriteString(fmt.Sprintf(`storage "%s" {`, storage.Hash()))
		buf.WriteRune('\n')
		buf.WriteRune('\t')
		buf.WriteString(fmt.Sprintf(`type "%s"`, "remote"))
		buf.WriteRune('\n')
		buf.WriteRune('\t')
		buf.WriteString(fmt.Sprintf(`host "%s"`, storage.Host))
		buf.WriteRune('\n')
		buf.WriteRune('\t')
		buf.WriteString(fmt.Sprintf(`port %d`, storage.Port))
		buf.WriteRune('\n')
		buf.WriteRune('}')
		buf.WriteRune('\n')
	}

	return buf.String()
}

func GenerateDatabasesConf(databases v1alpha2.Databases) string {
	var buf strings.Builder
	for _, database := range databases {
		buf.WriteString(fmt.Sprintf("database \"%s\" {\n", database.Name))

		user := database.User
		storage := database.Storage

		buf.WriteString(fmt.Sprintf("\tuser \"%s\" {\n", database.Name))
		buf.WriteString(fmt.Sprintf("\t\tauthentication \"%s\"\n", user.Authentication))
		buf.WriteString(fmt.Sprintf("\t\tstorage \"%s\"\n", storage.Hash()))
		if PointerDeref(storage.Name, "") != "" {
			buf.WriteString(fmt.Sprintf("\t\tstorage_db \"%s\"\n", *database.Storage.Name))
		}
		if PointerDeref(storage.User, "") != "" {
			buf.WriteString(fmt.Sprintf("\t\tstorage_user \"%s\"\n", *storage.User))
		}
		if PointerDeref(storage.Password, "") != "" {
			buf.WriteString(fmt.Sprintf("\t\tstorage_password \"%s\"\n", *storage.Password))
		}
		buf.WriteString(fmt.Sprintf("\t\tpool \"%s\"\n", user.Pool))
		buf.WriteString(fmt.Sprintf("\t\tpassword \"%s\"\n", database.Password))
		buf.WriteString(fmt.Sprintf("\t\tclient_max %d\n", user.ClientMax))
		buf.WriteString(fmt.Sprintf("\t\tpool_ttl %d\n", uint(user.PoolTTL.Seconds())))
		buf.WriteString(fmt.Sprintf("\t\tpool_discard %s\n", BoolToYesNo(user.PoolDiscard)))
		buf.WriteString(fmt.Sprintf("\t\tpool_cancel %s\n", BoolToYesNo(user.PoolCancel)))
		buf.WriteString(fmt.Sprintf("\t\tapplication_name_add_host %s\n", BoolToYesNo(user.ApplicationNameAddHost)))

		buf.WriteString(fmt.Sprintf("\t\tclient_fwd_error %s\n", BoolToYesNo(user.ClientForwardError)))

		buf.WriteString(fmt.Sprintf("\t\tserver_lifetime %d\n", uint(user.ServerLifetime.Seconds())))

		buf.WriteString(fmt.Sprintf("\t\tlog_debug %s\n", BoolToYesNo(user.LogDebug)))

		buf.WriteString(fmt.Sprintf("\t\tpool_reserve_prepared_statement %s\n", BoolToYesNo(user.PoolReservePreparedStatement)))

		buf.WriteString(fmt.Sprintf("\t\tquantiles \"%s\"\n", user.Quantiles))
		buf.WriteString("\t}\n")

		buf.WriteString("}\n")
	}

	return buf.String()
}
