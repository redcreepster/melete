/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package database

import (
	"context"
	"errors"
	"fmt"
	"time"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database"
	"git.monitorsoft.ru/k8s/melete/internal/webhooks"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
)

// OdysseyInstanceReconciler reconciles a OdysseyInstance object
type OdysseyInstanceReconciler struct {
	client.Client
	Scheme *runtime.Scheme

	OdysseySupervisorServiceName        string
	OdysseySupervisorServiceNamespace   string
	OdysseySupervisorDeploymentTemplate []byte
	DefaultOdysseyInstanceConfig        []byte
}

//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odysseyinstances,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odysseyinstances/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=database.monitorsoft.ru,resources=odysseyinstances/finalizers,verbs=update

//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=services,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
func (r *OdysseyInstanceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	logger.Info("new request")

	var instance v1alpha2.OdysseyInstance
	if err := r.Get(ctx, req.NamespacedName, &instance); err != nil {
		if !apierrors.IsNotFound(err) {
			logger.Error(err, "unable to fetch OdysseyInstance")

			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	logger = logger.WithValues("phase", instance.Status.Phase)
	logger.V(5).Info("Phase")

	if instance.GetDeletionTimestamp().IsZero() {
		// Fallback after update melete version
		if instance.Config == nil {
			patch := instance.DeepCopy()

			defaulter := webhooks.NewOdysseyInstanceDefaulter(r.DefaultOdysseyInstanceConfig)
			if err := defaulter.Default(ctx, patch); err != nil {
				logger.Error(err, "unable to apply defaulter OdysseyInstance")

				return ctrl.Result{}, err
			}

			if err := r.Patch(ctx, patch, client.MergeFrom(&instance)); err != nil {
				if !apierrors.IsNotFound(err) {
					logger.Error(err, "unable to update OdysseyInstance")

					return ctrl.Result{}, err
				}

				return ctrl.Result{}, nil
			}

			return ctrl.Result{}, nil
		}

		switch instance.Status.Phase {
		case v1alpha2.OdysseyInstanceStatusPhaseNone:
			patch := instance.DeepCopy()

			patch.Status.Phase = v1alpha2.OdysseyInstanceStatusPhasePending

			if err := r.Status().Patch(ctx, patch, client.MergeFrom(&instance)); err != nil {
				if !apierrors.IsNotFound(err) {
					logger.Error(err, "unable to update OdysseyInstance status")

					return ctrl.Result{}, err
				}

				return ctrl.Result{}, nil
			}

			instance = *patch
		case
			v1alpha2.OdysseyInstanceStatusPhasePending,
			v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload,
			v1alpha2.OdysseyInstanceStatusPhaseReady: // For supervisor
			if err := r.updateDeploymentAndService(ctx, instance); err != nil {
				return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
			}

			if err := r.updateStatus(ctx, instance); err != nil {
				return ctrl.Result{Requeue: true, RequeueAfter: time.Second * 30}, err
			}
		default:
			err := errors.New("unknown phase")
			logger.WithValues("phase", instance.Status.Phase).
				Error(err, "unknown phase")

			return ctrl.Result{}, err
		}
	} else {
		return r.finalize(ctx, instance)
	}

	return ctrl.Result{}, nil
}

func (r *OdysseyInstanceReconciler) updateDeploymentAndService(ctx context.Context, instance v1alpha2.OdysseyInstance) error {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	var deployment appsv1.Deployment
	if err := r.Get(
		ctx,
		types.NamespacedName{
			Name:      database.DeploymentNameFromOdysseyInstance(r.OdysseySupervisorServiceName, instance.Name),
			Namespace: r.OdysseySupervisorServiceNamespace,
		},
		&deployment,
	); err != nil {
		if apierrors.IsNotFound(err) {
			deployment = r.createDeployment(instance)
		}
	}

	if _, err := controllerutil.CreateOrPatch(ctx, r.Client, &deployment, func() error {
		// save previous status
		status := deployment.Status.DeepCopy()
		newDeployment := r.createDeployment(instance)
		// restore previous status
		status.DeepCopyInto(&newDeployment.Status)
		newDeployment.DeepCopyInto(&deployment)

		return nil
	}); err != nil {
		logger.Error(err, "unable create or patch Deployment")

		return err
	}

	var selector = deployment.Spec.Selector.MatchLabels
	var service = corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      database.ServiceNameFromOdysseyInstance(r.OdysseySupervisorServiceName, instance.Name),
			Namespace: r.OdysseySupervisorServiceNamespace,
		},
	}
	if _, err := controllerutil.CreateOrPatch(ctx, r.Client, &service, r.provideService(instance, &service, selector)); err != nil {
		logger.Error(err, "unable create or patch Service")

		return err
	}

	return nil
}

// TODO: Move to configmap or crd
func (r *OdysseyInstanceReconciler) provideService(instance v1alpha2.OdysseyInstance, service *corev1.Service, selector map[string]string) controllerutil.MutateFn {
	return func() error {
		labels := map[string]string{
			"app.kubernetes.io/name":       fmt.Sprintf("odyssey-supervisor-%s", instance.Name),
			"app.kubernetes.io/instance":   instance.Labels["app.kubernetes.io/instance"],
			"app.kubernetes.io/managed-by": "melete",
			"app.kubernetes.io/part-of":    "odyssey-supervisor",
			"app.kubernetes.io/component":  instance.Name,
		}

		if service.ObjectMeta.Labels == nil {
			service.ObjectMeta.Labels = make(map[string]string, len(labels))
		}
		for key, value := range labels {
			service.ObjectMeta.Labels[key] = value
		}

		service.Spec.Type = corev1.ServiceTypeClusterIP

		service.Spec.Ports = []corev1.ServicePort{
			{
				Name:       "postgres",
				Protocol:   corev1.ProtocolTCP,
				Port:       5432,
				TargetPort: intstr.FromString("postgres"),
			},
		}

		if service.Spec.Selector == nil {
			service.Spec.Selector = make(map[string]string, len(selector))
		}
		for key, value := range selector {
			service.Spec.Selector[key] = value
		}

		return nil
	}
}

func (r *OdysseyInstanceReconciler) createDeployment(instance v1alpha2.OdysseyInstance) appsv1.Deployment {
	decode := scheme.Codecs.UniversalDeserializer().Decode
	obj, gKV, _ := decode(r.OdysseySupervisorDeploymentTemplate, nil, nil)
	var deployment *appsv1.Deployment
	if gKV.Kind == "Deployment" {
		deployment = obj.(*appsv1.Deployment)
	}

	deployment.Name = database.DeploymentNameFromOdysseyInstance(r.OdysseySupervisorServiceName, instance.Name)
	deployment.Namespace = r.OdysseySupervisorServiceNamespace
	deployment.Spec.Replicas = instance.Replicas

	selector := map[string]string{
		"app.kubernetes.io/name":       fmt.Sprintf("odyssey-supervisor-%s", instance.Name),
		"app.kubernetes.io/managed-by": "melete",
	}
	if deployment.ObjectMeta.Labels == nil {
		deployment.ObjectMeta.Labels = make(map[string]string, len(selector))
	}
	if deployment.Spec.Selector == nil {
		deployment.Spec.Selector = new(metav1.LabelSelector)
	}
	if deployment.Spec.Selector.MatchLabels == nil {
		deployment.Spec.Selector.MatchLabels = make(map[string]string, len(selector))
	}
	if deployment.Spec.Template.ObjectMeta.Labels == nil {
		deployment.Spec.Template.ObjectMeta.Labels = make(map[string]string, len(selector))
	}
	for key, value := range selector {
		deployment.ObjectMeta.Labels[key] = value
		deployment.Spec.Selector.MatchLabels[key] = value
		deployment.Spec.Template.ObjectMeta.Labels[key] = value
		if deployment.Spec.Template.Spec.Affinity != nil &&
			deployment.Spec.Template.Spec.Affinity.PodAntiAffinity != nil &&
			deployment.Spec.Template.Spec.Affinity.PodAntiAffinity.PreferredDuringSchedulingIgnoredDuringExecution != nil {
			for _, term := range deployment.Spec.Template.Spec.Affinity.PodAntiAffinity.PreferredDuringSchedulingIgnoredDuringExecution {
				term.PodAffinityTerm.LabelSelector.MatchLabels[key] = value
				term.PodAffinityTerm.Namespaces = []string{
					r.OdysseySupervisorServiceNamespace,
				}
			}
		}
	}
	deployment.Spec.Template.Spec.Containers[0].Args = append(deployment.Spec.Template.Spec.Containers[0].Args, "--instance-name="+instance.Name)

	return *deployment
}

func (r *OdysseyInstanceReconciler) updateStatus(ctx context.Context, instance v1alpha2.OdysseyInstance) error {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	patch := instance.DeepCopy()

	patch.Status.RoutesCount = Pointer(uint(len(instance.Databases)))
	if len(instance.Databases) > 0 {
		patch.Status.Phase = v1alpha2.OdysseyInstanceStatusPhaseReady
	}
	var maxConnections = uint(0)
	for _, db := range instance.Databases {
		maxConnections += db.User.ClientMax
	}
	patch.Status.MaxConnections = Pointer(maxConnections)

	if err := r.Status().Patch(ctx, patch, client.MergeFrom(&instance)); err != nil {
		if !apierrors.IsNotFound(err) {
			logger.Error(err, "unable to update OdysseyInstance status")

			return err
		}
	}

	return nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *OdysseyInstanceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1alpha2.OdysseyInstance{}).
		Owns(&appsv1.Deployment{}).
		Owns(&corev1.Service{}).
		Complete(r)
}

func (r *OdysseyInstanceReconciler) finalize(ctx context.Context, instance v1alpha2.OdysseyInstance) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	if err := r.Delete(
		ctx,
		&appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      database.DeploymentNameFromOdysseyInstance(r.OdysseySupervisorServiceName, instance.Name),
				Namespace: r.OdysseySupervisorServiceNamespace,
			},
		},
	); err != nil && !apierrors.IsNotFound(err) {
		logger.Error(err, "unable to delete Deployment")

		return ctrl.Result{}, err
	}

	if err := r.Delete(
		ctx,
		&corev1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      database.ServiceNameFromOdysseyInstance(r.OdysseySupervisorServiceName, instance.Name),
				Namespace: r.OdysseySupervisorServiceNamespace,
			},
		},
	); err != nil && !apierrors.IsNotFound(err) {
		logger.Error(err, "unable to delete Secret")

		return ctrl.Result{}, err
	}

	if controllerutil.ContainsFinalizer(&instance, FinalizerName) {
		// remove our finalizer from the list and update it.
		patch := instance.DeepCopy()

		controllerutil.RemoveFinalizer(patch, FinalizerName)
		if err := r.Patch(ctx, patch, client.MergeFrom(&instance)); err != nil {
			if !apierrors.IsNotFound(err) {
				logger.Error(err, "unable to update OdysseyInstance")

				return ctrl.Result{}, err
			}
		}
	}

	return ctrl.Result{}, nil
}
