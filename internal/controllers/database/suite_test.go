/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package database

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database"
	. "git.monitorsoft.ru/k8s/melete/internal/common"
	"git.monitorsoft.ru/k8s/melete/internal/postgres"
	. "git.monitorsoft.ru/k8s/melete/internal/test/utils"

	"go.uber.org/zap/zapcore"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/utils/pointer"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha3"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha4"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
	"git.monitorsoft.ru/k8s/melete/internal/webhooks"
	//+kubebuilder:scaffold:imports
)

var (
	cfg                                 *rest.Config
	k8sClient                           client.Client
	testEnv                             *envtest.Environment
	postgresClient                      *PostgresClientDummy
	defaultOdysseyInstanceConfig        []byte
	odysseySupervisorDeploymentTemplate []byte
	replicas                            int32 = 3
	ctx                                 context.Context
	cancel                              context.CancelFunc
)

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecs(t, "Controller Suite")
}

var _ = Describe("OdysseyInstance", Ordered, func() {
	Describe("Create", func() {
		var name string
		BeforeEach(func() {
			name = RandomName()
		})
		AfterEach(func() {
			odysseyInstance := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
			}
			Eventually(func() error {
				return k8sClient.Delete(ctx, &odysseyInstance)
			}, 10, 1).Should(Satisfy(apierrors.IsNotFound))
		})

		It("Should create deployment and secret", func() {
			odysseyInstance := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
					Labels: map[string]string{
						"app.kubernetes.io/instance": "some-instance",
					},
				},
				Replicas: pointer.Int32(replicas),
				Databases: []v1alpha2.Database{
					{
						Name:     "some-name",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    30,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
				},
			}
			Expect(k8sClient.Create(ctx, &odysseyInstance)).Should(Succeed())

			Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
				Should(And(
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
					HaveField("ObjectMeta.Finalizers", ContainElement(FinalizerName)),
					HaveField("Config", HaveValue(Equal(string(defaultOdysseyInstanceConfig)))),
				))

			Eventually(func() (appsv1.Deployment, error) {
				var deployment appsv1.Deployment
				lookupKey := types.NamespacedName{Name: database.DeploymentNameFromOdysseyInstance("odyssey", name), Namespace: "default"}
				return deployment, k8sClient.Get(ctx, lookupKey, &deployment)
			}, 10, 1).Should(ReplicasEqual(replicas))

			Eventually(func() (corev1.Service, error) {
				var service corev1.Service
				lookupKey := types.NamespacedName{Name: database.ServiceNameFromOdysseyInstance("odyssey", name), Namespace: "default"}
				return service, k8sClient.Get(ctx, lookupKey, &service)
			}, 10, 1).Should(And(
				HaveField("Spec.Type", Equal(corev1.ServiceTypeClusterIP)),
				HaveField("Spec.Ports", And(HaveLen(1), ContainElement(corev1.ServicePort{
					Name:       "postgres",
					Protocol:   corev1.ProtocolTCP,
					Port:       5432,
					TargetPort: intstr.FromString("postgres"),
				}))),
				HaveField("Spec.Selector", Equal(map[string]string{
					"app.kubernetes.io/instance":   "melete",
					"app.kubernetes.io/managed-by": "melete",
					"app.kubernetes.io/name":       "odyssey-supervisor-" + name,
					"app.kubernetes.io/component":  "odyssey-supervisor",
				})),
			))
		})
		It("Should create deployment and secret if databases is empty", func() {
			odysseyInstance := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Replicas:  pointer.Int32(replicas),
				Databases: []v1alpha2.Database{},
			}
			Expect(k8sClient.Create(ctx, &odysseyInstance)).Should(Succeed())

			Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
				Should(And(
					Not(PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady)),
					HaveField("Status.MaxConnections", HaveValue(BeEquivalentTo(0))),
					HaveField("Status.RoutesCount", HaveValue(BeEquivalentTo(0))),
				))

			Eventually(func() error {
				var deployment appsv1.Deployment
				lookupKey := types.NamespacedName{Name: database.DeploymentNameFromOdysseyInstance("odyssey", odysseyInstance.Name), Namespace: "default"}
				return k8sClient.Get(ctx, lookupKey, &deployment)
			}, 10, 1).Should(Not(HaveOccurred()))

			Eventually(func() error {
				var service corev1.Service
				lookupKey := types.NamespacedName{Name: database.ServiceNameFromOdysseyInstance("odyssey", odysseyInstance.Name), Namespace: "default"}
				return k8sClient.Get(ctx, lookupKey, &service)
			}, 10, 1).Should(Not(HaveOccurred()))
		})
	})
	Describe("Update", func() {
		var name string
		var odysseyInstance v1alpha2.OdysseyInstance
		BeforeEach(func() {
			name = RandomName()

			odysseyInstance = v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
					Labels: map[string]string{
						"app.kubernetes.io/instance": "some-instance",
					},
				},
				Replicas: pointer.Int32(replicas),
				Databases: []v1alpha2.Database{
					{
						Name:     "some-name",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    1,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
				},
			}
			Expect(k8sClient.Create(ctx, &odysseyInstance)).Should(Succeed())

			Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
				Should(PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady))
		})
		AfterEach(func() {
			Eventually(func() error {
				return k8sClient.Delete(ctx, &odysseyInstance)
			}, 10, 1).Should(Satisfy(apierrors.IsNotFound))
		})

		Describe("Deployment", func() {
			It("Replicas", func() {
				odysseyInstance.Replicas = pointer.Int32(1)
				Expect(k8sClient.Update(ctx, &odysseyInstance)).Should(Succeed())

				Eventually(func() (appsv1.Deployment, error) {
					var deployment appsv1.Deployment
					lookupKey := types.NamespacedName{Name: database.DeploymentNameFromOdysseyInstance("odyssey", odysseyInstance.Name), Namespace: "default"}
					return deployment, k8sClient.Get(ctx, lookupKey, &deployment)
				}, 10, 1).Should(ReplicasEqual(1))
			})
		})
		Describe("Status", func() {
			It("MaxConnections", func() {
				odysseyInstance.Databases = []v1alpha2.Database{
					{
						Name:     "some-name",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    30,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
					{
						Name:     "some-name-2",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    10,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
				}
				Expect(k8sClient.Update(ctx, &odysseyInstance)).Should(Succeed())

				Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
					Should(And(
						PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
						HaveField("Status.MaxConnections", HaveValue(BeEquivalentTo(40))),
					))

				odysseyInstance.Databases = []v1alpha2.Database{
					{
						Name:     "some-name",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    10,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
					{
						Name:     "some-name-2",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    10,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
				}
				Expect(k8sClient.Update(ctx, &odysseyInstance)).Should(Succeed())

				Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
					Should(And(
						PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
						HaveField("Status.MaxConnections", HaveValue(BeEquivalentTo(20))),
					))
			})
			It("RoutesCount", func() {
				odysseyInstance.Databases = []v1alpha2.Database{
					{
						Name:     "some-name",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    30,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
					{
						Name:     "some-name-2",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    10,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
				}
				Expect(k8sClient.Update(ctx, &odysseyInstance)).Should(Succeed())

				Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
					Should(And(
						PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
						HaveField("Status.RoutesCount", HaveValue(BeEquivalentTo(2))),
					))

				odysseyInstance.Databases = []v1alpha2.Database{
					{
						Name:     "some-name",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    10,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
					{
						Name:     "some-name-2",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    10,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
					{
						Name:     "some-name-3",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    15,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
				}
				Expect(k8sClient.Update(ctx, &odysseyInstance)).Should(Succeed())

				Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
					Should(And(
						PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
						HaveField("Status.RoutesCount", HaveValue(BeEquivalentTo(3))),
					))

				odysseyInstance.Databases = []v1alpha2.Database{
					{
						Name:     "some-name-3",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    15,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
				}
				Expect(k8sClient.Update(ctx, &odysseyInstance)).Should(Succeed())

				Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
					Should(And(
						PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
						HaveField("Status.RoutesCount", HaveValue(BeEquivalentTo(1))),
					))
			})
		})
		It("Should not delete when databases is empty after update", func() {
			Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
				Should(PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady))

			odysseyInstance.Databases = nil
			Expect(k8sClient.Update(ctx, &odysseyInstance)).Should(Succeed())

			Eventually(GetOdysseyInstance(ctx, k8sClient, name, &odysseyInstance), 10, 1).
				Should(And(
					Or(
						PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePending),
						PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload),
						PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
					),
					HaveField("Status.MaxConnections", HaveValue(BeEquivalentTo(0))),
					HaveField("Status.RoutesCount", HaveValue(BeEquivalentTo(0))),
				))

			Eventually(func() error {
				var deployment appsv1.Deployment
				lookupKey := types.NamespacedName{Name: database.DeploymentNameFromOdysseyInstance("odyssey", odysseyInstance.Name), Namespace: "default"}
				return k8sClient.Get(ctx, lookupKey, &deployment)
			}, 10, 1).ShouldNot(HaveOccurred())

			Eventually(func() error {
				var service corev1.Service
				lookupKey := types.NamespacedName{Name: database.ServiceNameFromOdysseyInstance("odyssey", odysseyInstance.Name), Namespace: "default"}
				return k8sClient.Get(ctx, lookupKey, &service)
			}, 10, 1).ShouldNot(HaveOccurred())
		})
	})
	Describe("Delete", func() {
		var name string
		BeforeEach(func() {
			name = RandomName()
		})
		AfterEach(func() {
			odysseyInstance := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
			}
			Eventually(func() error {
				return k8sClient.Delete(ctx, &odysseyInstance)
			}, 10, 1).Should(Satisfy(apierrors.IsNotFound))
		})

		It("Should delete deployment and service after delete odyssey instance", func() {
			odysseyInstance := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Replicas: pointer.Int32(replicas),
				Databases: []v1alpha2.Database{
					{
						Name:     "some-name",
						Password: "test123456",
						User: v1alpha2.User{
							Authentication:               v1alpha2.AuthenticationMD5,
							Pool:                         v1alpha2.PoolTransaction,
							ClientMax:                    30,
							PoolTTL:                      1 * time.Minute,
							PoolDiscard:                  false,
							PoolCancel:                   true,
							ApplicationNameAddHost:       true,
							ClientForwardError:           true,
							ServerLifetime:               1 * time.Hour,
							LogDebug:                     false,
							PoolReservePreparedStatement: false,
							Quantiles:                    "0.99,0.95,0.5",
						},
						Storage: v1alpha2.Storage{
							Host: "patroni.postgres.svc",
							Port: 5432,
						},
					},
				},
			}
			Expect(k8sClient.Create(ctx, &odysseyInstance)).Should(Succeed())

			Eventually(func() error {
				return k8sClient.Delete(ctx, &odysseyInstance)
			}, 20, 1).Should(Satisfy(apierrors.IsNotFound))

			Eventually(func() error {
				var deployment appsv1.Deployment
				lookupKey := types.NamespacedName{Name: database.DeploymentNameFromOdysseyInstance("odyssey", odysseyInstance.Name), Namespace: "default"}
				return k8sClient.Get(ctx, lookupKey, &deployment)
			}, 20, 1).Should(Satisfy(apierrors.IsNotFound))

			Eventually(func() error {
				var service corev1.Service
				lookupKey := types.NamespacedName{Name: database.ServiceNameFromOdysseyInstance("odyssey", odysseyInstance.Name), Namespace: "default"}
				return k8sClient.Get(ctx, lookupKey, &service)
			}, 20, 1).Should(Satisfy(apierrors.IsNotFound))
		})
	})
})

var _ = Describe("Odyssey", Ordered, func() {
	var name string
	var namespace = "default"
	BeforeAll(func() {
		postgresEndpoints := v1alpha1.PostgresEndpointList{}
		Expect(k8sClient.List(ctx, &postgresEndpoints)).ShouldNot(HaveOccurred())
		for _, postgresEndpoint := range postgresEndpoints.Items {
			if value, ok := postgresEndpoint.Annotations["melete.monitorsoft.ru/is-default-endpoint"]; ok && value == "true" {
				Expect(k8sClient.Delete(ctx, &postgresEndpoint)).ShouldNot(HaveOccurred())
			}
		}

		defaultPostgresEndpoint := v1alpha1.PostgresEndpoint{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "default",
				Namespace: namespace,
				Annotations: map[string]string{
					"melete.monitorsoft.ru/is-default-endpoint": "true",
				},
			},
			Spec: v1alpha1.PostgresEndpointSpec{
				PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
					Host: "patroni.postgres.svc",
					Port: pointer.Uint32(5432),
				},
				User: v1alpha1.PostgresEndpointUser{
					Name:     "postgres",
					Password: "some-pass",
				},
				DefaultDatabase: pointer.String("postgres"),
				Replicas: []v1alpha1.PostgresEndpointReplica{
					{
						Name: "replica",
						PostgresEndpointHostPort: v1alpha1.PostgresEndpointHostPort{
							Host: "patroni-replica.postgres.svc",
							Port: pointer.Uint32(5432),
						},
					},
				},
			},
		}

		Expect(k8sClient.Create(ctx, &defaultPostgresEndpoint)).Should(Succeed())
	})
	BeforeEach(func() {
		name = RandomName()
	})
	AfterEach(func() {
		odyssey := v1alpha5.Odyssey{
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: namespace,
			},
		}
		Eventually(func() error {
			return k8sClient.Delete(ctx, &odyssey)
		}, 10, 1).Should(Satisfy(apierrors.IsNotFound))
	})

	Describe("Create", func() {
		BeforeEach(func() {
			var odysseyInstance v1alpha2.OdysseyInstance
			odysseyInstance = v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: "master",
				},
				Replicas: pointer.Int32(replicas),
			}
			Expect(k8sClient.Create(ctx, &odysseyInstance)).Should(Succeed())

			Eventually(GetOdysseyInstance(ctx, k8sClient, "master", &odysseyInstance), 10, 1).
				Should(Or(
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePending),
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload),
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
				))

			odysseyInstance = v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: "replica",
				},
				Replicas: pointer.Int32(replicas),
			}
			Expect(k8sClient.Create(ctx, &odysseyInstance)).Should(Succeed())

			Eventually(GetOdysseyInstance(ctx, k8sClient, "replica", &odysseyInstance), 10, 1).
				Should(Or(
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePending),
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload),
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
				))
		})
		AfterEach(func() {
			odysseyInstance := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: "master",
				},
			}
			Eventually(func() error {
				return k8sClient.Delete(ctx, &odysseyInstance)
			}, 10, 1).Should(Satisfy(apierrors.IsNotFound))

			odysseyInstanceReplica := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: "replica",
				},
			}
			Eventually(func() error {
				return k8sClient.Delete(ctx, &odysseyInstanceReplica)
			}, 10, 1).Should(Satisfy(apierrors.IsNotFound))
		})

		It("Should create OdysseyInstance", func() {
			odyssey := v1alpha5.Odyssey{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha5.OdysseySpec{
					Password:       pointer.String("some-pass"),
					MaxConnections: 10,
				},
				OdysseyInstanceReferences: []v1alpha5.OdysseyInstanceReference{
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "master",
					},
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "replica",
					},
				},
			}

			Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

			Eventually(GetOdyssey(ctx, k8sClient, name, namespace, &odyssey), 10, 1).
				Should(And(
					PhaseEqual(v1alpha5.OdysseyStatusPhaseReady),
					HaveField("ObjectMeta.Finalizers", ContainElement(FinalizerName)),
				))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "master", nil), 10, 1).
				Should(And(
					HaveField("Replicas", HaveValue(BeEquivalentTo(replicas))),
					HaveField("Databases", And(
						HaveLen(1),
						ContainElement(v1alpha2.Database{
							Name:     namespace + "." + name,
							Password: "some-pass",
							User: v1alpha2.User{
								Authentication:               v1alpha2.AuthenticationMD5,
								Pool:                         v1alpha2.PoolTransaction,
								ClientMax:                    10,
								PoolTTL:                      1 * time.Minute,
								PoolDiscard:                  false,
								PoolCancel:                   true,
								ApplicationNameAddHost:       true,
								ClientForwardError:           true,
								ServerLifetime:               1 * time.Hour,
								LogDebug:                     false,
								PoolReservePreparedStatement: false,
								Quantiles:                    "0.99,0.95,0.5",
							},
							Storage: v1alpha2.Storage{
								Host: "patroni.postgres.svc",
								Port: 5432,
							},
						}),
					)),
				))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "replica", nil), 10, 1).
				Should(And(
					HaveField("Replicas", HaveValue(BeEquivalentTo(replicas))),
					HaveField("Databases", And(
						HaveLen(1),
						ContainElement(v1alpha2.Database{
							Name:     namespace + "." + name,
							Password: "some-pass",
							User: v1alpha2.User{
								Authentication:               v1alpha2.AuthenticationMD5,
								Pool:                         v1alpha2.PoolTransaction,
								ClientMax:                    10,
								PoolTTL:                      1 * time.Minute,
								PoolDiscard:                  false,
								PoolCancel:                   true,
								ApplicationNameAddHost:       true,
								ClientForwardError:           true,
								ServerLifetime:               1 * time.Hour,
								LogDebug:                     false,
								PoolReservePreparedStatement: false,
								Quantiles:                    "0.99,0.95,0.5",
							},
							Storage: v1alpha2.Storage{
								Host: "patroni-replica.postgres.svc",
								Port: 5432,
							},
						}),
					)),
				))
		})

		It("Should create Secret", func() {
			odyssey := v1alpha5.Odyssey{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha5.OdysseySpec{
					Password:       pointer.String("some-pass"),
					MaxConnections: 10,
				},
				OdysseyInstanceReferences: []v1alpha5.OdysseyInstanceReference{
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "master",
					},
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "replica",
					},
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "sync-replica",
					},
				},
			}

			Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

			Eventually(GetOdyssey(ctx, k8sClient, name, namespace, &odyssey), 10, 1).
				Should(PhaseEqual(v1alpha5.OdysseyStatusPhaseReady))

			Eventually(func() (corev1.Secret, error) {
				var secret corev1.Secret
				lookupKey := types.NamespacedName{Name: fmt.Sprintf("%s-odyssey", name), Namespace: namespace}
				return secret, k8sClient.Get(ctx, lookupKey, &secret)
			}, 10, 1).Should(HaveField("Data", And(
				HaveLen(9),
				HaveKeyWithValue("DB_HOST", BeEquivalentTo(fmt.Sprintf("odyssey.%s.svc", namespace))),
				HaveKeyWithValue("DB_PORT", BeEquivalentTo("5432")),
				HaveKeyWithValue("DB_REPLICA_HOST", BeEquivalentTo(fmt.Sprintf("odyssey-replica.%s.svc", namespace))),
				HaveKeyWithValue("DB_REPLICA_PORT", BeEquivalentTo("5432")),
				HaveKeyWithValue("DB_SYNC_REPLICA_HOST", BeEquivalentTo(fmt.Sprintf("odyssey-sync-replica.%s.svc", namespace))),
				HaveKeyWithValue("DB_SYNC_REPLICA_PORT", BeEquivalentTo("5432")),
				HaveKeyWithValue("DB_NAME", BeEquivalentTo(odyssey.GetDatabaseName())),
				HaveKeyWithValue("DB_USERNAME", BeEquivalentTo(odyssey.GetDatabaseName())),
				HaveKeyWithValue("DB_PASSWORD", BeEquivalentTo(odyssey.GetSpecifiedOrGeneratedPassword())),
			)))
		})

		It("Should set pool in OdysseyInstance", func() {
			pool := v1alpha5.PoolSession

			odyssey := v1alpha5.Odyssey{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha5.OdysseySpec{
					Password:       pointer.String("some-pass"),
					MaxConnections: 10,
					Pool:           &pool,
				},
			}

			Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

			Eventually(GetOdyssey(ctx, k8sClient, name, namespace, &odyssey), 10, 1).
				Should(And(
					PhaseEqual(v1alpha5.OdysseyStatusPhaseReady),
					HaveField("ObjectMeta.Finalizers", ContainElement(FinalizerName)),
				))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "master", nil), 10, 1).
				Should(HaveField("Databases", ContainElement(
					HaveField("User.Pool", Equal(v1alpha2.PoolSession)),
				)))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "replica", nil), 10, 1).
				Should(HaveField("Databases", ContainElement(
					HaveField("User.Pool", Equal(v1alpha2.PoolSession)),
				)))
		})

		It("Should generate password", func() {
			odyssey := v1alpha5.Odyssey{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha5.OdysseySpec{
					MaxConnections: 10,
				},
			}

			Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

			Eventually(GetOdyssey(ctx, k8sClient, name, namespace, &odyssey), 10, 1).
				Should(And(
					PhaseEqual(v1alpha5.OdysseyStatusPhaseReady),
					HaveField("Spec.Password", BeNil()),
					HaveField("Status.GeneratedPassword", HaveValue(HaveLen(GeneratedPasswordLength))),
				))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "master", nil), 10, 1).
				Should(And(
					HaveField("Databases", And(
						HaveLen(1),
						ContainElement(HaveField("Password", Equal(*odyssey.Status.GeneratedPassword))),
					)),
				))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "replica", nil), 10, 1).
				Should(And(
					HaveField("Databases", And(
						HaveLen(1),
						ContainElement(HaveField("Password", Equal(*odyssey.Status.GeneratedPassword))),
					)),
				))
		})

		It("OdysseyInstanceReference not found in PostgresEndpoint", func() {
			odyssey := v1alpha5.Odyssey{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha5.OdysseySpec{
					MaxConnections: 10,
				},
				OdysseyInstanceReferences: []v1alpha5.OdysseyInstanceReference{
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "master",
					},
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "replica",
					},
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "sync-replica",
					},
				},
			}
			Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

			Eventually(GetOdyssey(ctx, k8sClient, name, namespace, &odyssey), 10, 1).
				Should(PhaseEqual(v1alpha5.OdysseyStatusPhaseReady))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "master", nil), 10, 1).
				Should(Or(
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePending),
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload),
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
				))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "replica", nil), 10, 1).
				Should(Or(
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePending),
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhasePendingConfigurationReload),
					PhaseEqual(v1alpha2.OdysseyInstanceStatusPhaseReady),
				))

			Consistently(PickError(GetOdysseyInstance(ctx, k8sClient, "sync-replica", nil)), 10, 1).
				Should(Satisfy(apierrors.IsNotFound))
		})
	})

	Describe("Delete", func() {
		AfterEach(func() {
			odysseyInstance := v1alpha2.OdysseyInstance{
				ObjectMeta: metav1.ObjectMeta{
					Name: "master",
				},
			}
			Eventually(func() error {
				return k8sClient.Delete(ctx, &odysseyInstance)
			}, 10, 1).Should(Satisfy(apierrors.IsNotFound))
		})

		It("Should delete database from OdysseyInstance", func() {
			odyssey := v1alpha5.Odyssey{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha5.OdysseySpec{
					MaxConnections: 10,
				},
				OdysseyInstanceReferences: []v1alpha5.OdysseyInstanceReference{
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "master",
					},
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "replica",
					},
					{
						Group: "database.monitorsoft.ru",
						Kind:  "OdysseyInstance",
						Name:  "sync-replica",
					},
				},
			}

			Expect(k8sClient.Create(ctx, &odyssey)).Should(Succeed())

			Eventually(GetOdyssey(ctx, k8sClient, name, namespace, &odyssey), 10, 1).
				Should(PhaseEqual(v1alpha5.OdysseyStatusPhaseReady))

			Eventually(func() error {
				return k8sClient.Delete(ctx, &odyssey)
			}, 10, 1).Should(Satisfy(apierrors.IsNotFound))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "master", nil), 10, 1).
				Should(HaveField("Databases", BeEmpty()))

			Eventually(GetOdysseyInstance(ctx, k8sClient, "replica", nil), 10, 1).
				Should(HaveField("Databases", BeEmpty()))
		})
	})

	It("Set PostgresEndpointReference after conversion", func() {
		//goland:noinspection GoDeprecation
		v1Odyssey := v1alpha1.Odyssey{ //nolint: staticcheck
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: namespace,
			},
			Spec: v1alpha1.OdysseySpec{
				MaxConnections: 10,
			},
		}

		Expect(k8sClient.Create(ctx, &v1Odyssey)).Should(Succeed())

		Eventually(GetOdyssey(ctx, k8sClient, name, namespace, &v1alpha5.Odyssey{}), 10, 1).
			Should(HaveField("PostgresEndpointReference", HaveValue(Equal(v1alpha5.PostgresEndpointReference{
				Group:     "database.monitorsoft.ru",
				Kind:      "PostgresEndpoint",
				Name:      "default",
				Namespace: namespace,
			}))))
	})

	It("Set OdysseyInstanceReferences after conversion", func() {
		//goland:noinspection GoDeprecation
		v1Odyssey := v1alpha1.Odyssey{ //nolint: staticcheck
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: namespace,
			},
			Spec: v1alpha1.OdysseySpec{
				Password:       pointer.String("some-pass"),
				MaxConnections: 10,
			},
		}

		Expect(k8sClient.Create(ctx, &v1Odyssey)).Should(Succeed())

		Eventually(GetOdyssey(ctx, k8sClient, name, namespace, &v1alpha5.Odyssey{}), 10, 1).
			Should(HaveField("OdysseyInstanceReferences", And(
				HaveLen(2),
				ContainElement(v1alpha5.OdysseyInstanceReference{
					Group: "database.monitorsoft.ru",
					Kind:  "OdysseyInstance",
					Name:  "master",
				}),
				ContainElement(v1alpha5.OdysseyInstanceReference{
					Group: "database.monitorsoft.ru",
					Kind:  "OdysseyInstance",
					Name:  "replica",
				}),
			)))
	})
})

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(
		zap.WriteTo(GinkgoWriter),
		zap.UseDevMode(true),
		zap.UseFlagOptions(&zap.Options{Level: zapcore.Level(-127)}),
	))

	ctx, cancel = context.WithCancel(context.TODO())

	dir, _ := os.Getwd()
	Expect(os.Chdir(filepath.Join(dir, ".."))).NotTo(HaveOccurred())
	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("..", "..", "config", "crd", "bases")},
		ErrorIfCRDPathMissing: true,
		WebhookInstallOptions: envtest.WebhookInstallOptions{
			Paths: []string{filepath.Join("..", "..", "config", "webhook")},
		},
	}

	var err error
	// cfg is defined in this file globally.
	cfg, err = testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	err = v1alpha1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	err = v1alpha2.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	err = v1alpha3.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	err = v1alpha4.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	err = v1alpha5.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	//+kubebuilder:scaffold:scheme

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	// start webhook server using Manager
	webhookInstallOptions := &testEnv.WebhookInstallOptions
	mgr, err := ctrl.NewManager(cfg, ctrl.Options{
		Scheme:             scheme.Scheme,
		Host:               webhookInstallOptions.LocalServingHost,
		Port:               webhookInstallOptions.LocalServingPort,
		CertDir:            webhookInstallOptions.LocalServingCertDir,
		LeaderElection:     false,
		MetricsBindAddress: "0",
	})
	Expect(err).ToNot(HaveOccurred())

	odysseySupervisorDeploymentTemplate = []byte(`apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    helm.sh/chart: melete-0.0.0
    app.kubernetes.io/name: melete
    app.kubernetes.io/instance: melete
    app.kubernetes.io/version: "0.0.0"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: odyssey-supervisor
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: melete
      app.kubernetes.io/instance: melete
      app.kubernetes.io/component: odyssey-supervisor
  template:
    metadata:
      annotations:
        kubectl.kubernetes.io/default-container: manager
        checksum/secret: 2497e33686424f8d7bc7cb809509c4bed908896bcd4b5a275281552b75ef7c04
      labels:
        helm.sh/chart: melete-0.0.0
        app.kubernetes.io/name: melete
        app.kubernetes.io/instance: melete
        app.kubernetes.io/version: "0.0.0"
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/component: odyssey-supervisor
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: kubernetes.io/arch
                operator: In
                values:
                - amd64
              - key: kubernetes.io/os
                operator: In
                values:
                - linux
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          # Размещать поды только на разных машинах
          - podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  helm.sh/chart: melete-0.0.0
                  app.kubernetes.io/name: melete
                  app.kubernetes.io/instance: melete
                  app.kubernetes.io/version: "0.0.0"
                  app.kubernetes.io/managed-by: Helm
                  app.kubernetes.io/component: manager
              namespaces:
              - melete-system
            weight: 100
          # По возможности(!) размещать поды в разных регионах
          - podAffinityTerm:
              topologyKey: topology.kubernetes.io/region
              labelSelector:
                matchLabels:
                  helm.sh/chart: melete-0.0.0
                  app.kubernetes.io/name: melete
                  app.kubernetes.io/instance: melete
                  app.kubernetes.io/version: "0.0.0"
                  app.kubernetes.io/managed-by: Helm
                  app.kubernetes.io/component: manager
              namespaces:
              - melete-system
            weight: 10
      imagePullSecrets:
        - name: regcred
      securityContext:
        runAsNonRoot: true
      serviceAccountName: melete
      terminationGracePeriodSeconds: 60
      containers:
      - name: odyssey-supervisor
        image: "docker-registry.monitorsoft.ru/melete/controller:0.0.0"
        imagePullPolicy: IfNotPresent
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            drop:
            - ALL
        command:
        - /usr/bin/manager
        args:
        - --health-probe-bind-address=:8081
        - --metrics-bind-address=:8080
        - --odyssey-executable=/usr/bin/odyssey
        - --odyssey-conf-path=/etc/odyssey/
        - --zap-log-level=debug
        volumeMounts:
        - name: configs
          mountPath: /etc/odyssey/odyssey.conf
          subPath: odyssey.conf
          readOnly: true
        ports:
        - name: postgres
          containerPort: 5432
          protocol: TCP
        readinessProbe:
          httpGet:
            path: /readyz
            port: 8081
          initialDelaySeconds: 5
          periodSeconds: 10
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8081
          initialDelaySeconds: 15
          periodSeconds: 20
        resources:
          limits:
            cpu: 100m
            memory: 128Mi
          requests:
            cpu: 100m
            memory: 128Mi
      volumes:
      - name: configs
        configMap:
          name: melete-odyssey-supervisor
          defaultMode: 420
          items:
          - key: odyssey.conf
            path: odyssey.conf
`)
	defaultOdysseyInstanceConfig = []byte(`some-default-config`)

	NewPostgresClient = func(credentials postgres.Credentials) postgres.Client {
		postgresClient = &PostgresClientDummy{}

		return postgresClient
	}

	err = (&OdysseyReconciler{
		Client:                            mgr.GetClient(),
		Scheme:                            mgr.GetScheme(),
		OdysseySupervisorServiceName:      "odyssey",
		OdysseySupervisorServiceNamespace: "default",
		OdysseyInstanceReplicas:           replicas,
	}).SetupWithManager(mgr)
	Expect(err).ToNot(HaveOccurred())

	err = (&OdysseyInstanceReconciler{
		Client:                              mgr.GetClient(),
		Scheme:                              mgr.GetScheme(),
		OdysseySupervisorServiceName:        "odyssey",
		OdysseySupervisorServiceNamespace:   "default",
		OdysseySupervisorDeploymentTemplate: odysseySupervisorDeploymentTemplate,
		DefaultOdysseyInstanceConfig:        defaultOdysseyInstanceConfig,
	}).SetupWithManager(mgr)
	Expect(err).ToNot(HaveOccurred())

	err = webhooks.SetupPostgresEndpointWebhookWithManager(mgr)
	Expect(err).NotTo(HaveOccurred())

	err = webhooks.SetupOdysseyInstanceWebhookWithManager(mgr, defaultOdysseyInstanceConfig)
	Expect(err).NotTo(HaveOccurred())

	err = webhooks.SetupOdysseyWebhookWithManager(mgr)
	Expect(err).NotTo(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = mgr.Start(ctx)
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
	}()

	// wait for the webhook server to get ready
	dialer := &net.Dialer{Timeout: time.Second}
	addrPort := fmt.Sprintf("%s:%d", webhookInstallOptions.LocalServingHost, webhookInstallOptions.LocalServingPort)
	Eventually(func() error {
		conn, err := tls.DialWithDialer(dialer, "tcp", addrPort, &tls.Config{InsecureSkipVerify: true})
		if err != nil {
			return err
		}
		//goland:noinspection GoUnhandledErrorResult
		conn.Close()
		return nil
	}).Should(Succeed())
})

var _ = AfterSuite(func() {
	cancel()
	By("tearing down the test environment")
	err := testEnv.Stop()
	Expect(err).NotTo(HaveOccurred())
})
