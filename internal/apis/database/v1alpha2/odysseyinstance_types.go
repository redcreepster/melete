/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha2

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	. "git.monitorsoft.ru/k8s/melete/internal/common"
)

//+kubebuilder:object:generate=true

type Database struct {
	// Name of database and user.
	Name     string `json:"name"`
	Password string `json:"password"`

	User    User    `json:"user"`
	Storage Storage `json:"storage"`
}

//+kubebuilder:object:generate=true

type Databases []Database

// User defines authentication and pooling settings for route.
// Details on https://github.com/yandex/odyssey/blob/1.3/documentation/configuration.md#database-and-user
// +kubebuilder:object:generate=true
type User struct {
	Authentication Authentication `json:"authentication"`
	// Pool set pooling mode.
	// Details on https://github.com/yandex/odyssey/blob/1.3/documentation/configuration.md#pool-string
	Pool OdysseyInstancePool `json:"pool"`
	// Connections limit for database and user.
	// Details on https://github.com/yandex/odyssey/blob/1.3/documentation/configuration.md#pool_size-integer
	ClientMax uint `json:"clientMax"`

	PoolTTL                time.Duration `json:"poolTTL"`
	PoolDiscard            bool          `json:"poolDiscard"`
	PoolCancel             bool          `json:"poolCancel"`
	ApplicationNameAddHost bool          `json:"applicationNameAddHost"`
	ClientForwardError     bool          `json:"clientForwardError"`
	ServerLifetime         time.Duration `json:"serverLifetime"`
	LogDebug               bool          `json:"logDebug"`
	// Enable prepared statements.
	// Details on https://github.com/yandex/odyssey/blob/1.3/config-examples/odyssey-dev.conf#L87
	PoolReservePreparedStatement bool      `json:"poolReservePreparedStatement"`
	Quantiles                    Quantiles `json:"quantiles"`
}

type Authentication string

//goland:noinspection GoUnusedConst
const (
	AuthenticationNone = Authentication("none")
	AuthenticationMD5  = Authentication("md5")
)

// OdysseyInstancePool set pooling mode.
// Details on https://github.com/yandex/odyssey/blob/1.3/documentation/configuration.md#pool-string
type OdysseyInstancePool string

//goland:noinspection GoUnusedConst
const (
	// PoolTransaction assign server connection to a client for a transaction processing
	PoolTransaction = OdysseyInstancePool("transaction")
	// PoolSession assign server connection to a client until it disconnects
	PoolSession = OdysseyInstancePool("session")
)

// Storage defines server used.
// Details on https://github.com/yandex/odyssey/blob/1.3/documentation/configuration.md#storage
// +kubebuilder:object:generate=true
type Storage struct {
	// Name uses for existing database
	// +kubebuilder:validation:Optional
	Name *string `json:"name,omitempty"`
	// User uses for existing database
	// +kubebuilder:validation:Optional
	User *string `json:"user,omitempty"`
	// Password uses for existing database
	// +kubebuilder:validation:Optional
	Password *string `json:"password,omitempty"`

	Host string `json:"host"`
	// +kubebuilder:validation:Minimum=0
	// +kubebuilder:validation:Maximum=65535
	Port uint16 `json:"port"`
}

func (s *Storage) Equal(a Storage) bool {
	return s.Host == a.Host &&
		s.Port == a.Port &&
		PointerDeref(s.Name, "") == PointerDeref(a.Name, "") &&
		PointerDeref(s.User, "") == PointerDeref(a.User, "") &&
		PointerDeref(s.Password, "") == PointerDeref(a.Password, "")
}

func (s *Storage) Hash() string {
	hasher := sha256.New()
	hasher.Write([]byte(s.Host))

	//goland:noinspection GoUnhandledErrorResult
	_ = binary.Write(hasher, binary.LittleEndian, s.Port)

	if s.Name != nil {
		hasher.Write([]byte(*s.Name))
	}
	if s.User != nil {
		hasher.Write([]byte(*s.User))
	}
	if s.Password != nil {
		hasher.Write([]byte(*s.Password))
	}

	sumBytes := hasher.Sum(nil)

	return hex.EncodeToString(sumBytes)
}

type Quantiles string

type OdysseyInstanceStatusPhase string

//goland:noinspection GoUnusedConst
const (
	OdysseyInstanceStatusPhaseNone                       = OdysseyInstanceStatusPhase("")
	OdysseyInstanceStatusPhasePending                    = OdysseyInstanceStatusPhase("Pending")
	OdysseyInstanceStatusPhasePendingConfigurationReload = OdysseyInstanceStatusPhase("Pending configuration reload")
	OdysseyInstanceStatusPhaseReady                      = OdysseyInstanceStatusPhase("Ready")
)

//+kubebuilder:object:generate=true

// OdysseyInstanceStatus defines the observed state of OdysseyInstance
type OdysseyInstanceStatus struct {
	// +kubebuilder:default=Pending
	Phase OdysseyInstanceStatusPhase `json:"phase"`

	// Connections limit of all databases and users.
	// +kubebuilder:validation:Optional
	// +kubebuilder:default=0
	MaxConnections *uint `json:"maxConnections"`

	// Count of routes.
	// +kubebuilder:validation:Optional
	// +kubebuilder:default=0
	RoutesCount *uint `json:"routesCount"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:scope=Cluster
//+kubebuilder:storageversion
//+kubebuilder:printcolumn:name="Phase",type=string,JSONPath=`.status.phase`
//+kubebuilder:printcolumn:name="Max connections",type=integer,JSONPath=`.status.maxConnections`
//+kubebuilder:printcolumn:name="Databases count",type=integer,JSONPath=`.status.databasesCount`

// OdysseyInstance is the Schema for the odysseyinstances API
type OdysseyInstance struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Minimum=1
	Replicas *int32 `json:"replicas"`

	// +kubebuilder:validation:Required
	Config *string `json:"config,omitempty"`

	Databases Databases             `json:"databases,omitempty"`
	Status    OdysseyInstanceStatus `json:"status,omitempty"`
}

// Hub marks this type as a conversion hub.
func (*OdysseyInstance) Hub() {}

// AddDatabase append or replace database.
// Return true when database was found in slice and replaced with new database.
func (oi *OdysseyInstance) AddDatabase(new *Database) bool {
	for i, database := range oi.Databases {
		if database.Name == new.Name {
			oi.Databases[i] = *new

			return true
		}
	}

	oi.Databases = append(oi.Databases, *new)

	return false
}

// RemoveDatabase remove database from slice.
// Return true when database was found.
func (oi *OdysseyInstance) RemoveDatabase(name string) bool {
	for i, database := range oi.Databases {
		if database.Name == name {
			oi.Databases = append(oi.Databases[0:i], oi.Databases[i+1:]...)

			return true
		}
	}

	return false
}

//+kubebuilder:object:root=true

// OdysseyInstanceList contains a list of OdysseyInstance
type OdysseyInstanceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []OdysseyInstance `json:"items"`
}

func init() {
	SchemeBuilder.Register(&OdysseyInstance{}, &OdysseyInstanceList{})
}
