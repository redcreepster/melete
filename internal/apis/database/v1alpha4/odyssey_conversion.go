/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha4

import (
	"errors"

	"k8s.io/apimachinery/pkg/util/json"
	"sigs.k8s.io/controller-runtime/pkg/conversion"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
)

var odysseyAdditionalFieldsAnnotationName = "melete.monitorsoft.ru/additional-fields-" + GroupVersion.Version

//+kubebuilder:object:generate=false

type odysseyAdditionalFields struct {
	OdysseyInstanceReferences []v1alpha5.OdysseyInstanceReference `json:"odysseyInstanceReferences,omitempty"`
	PostgresEndpointReference *v1alpha5.PostgresEndpointReference `json:"postgresEndpointReference,omitempty"`
}

// ConvertTo converts this Odyssey to the Hub version (v1alpha5).
//
//goland:noinspection GoDeprecation
func (o *Odyssey) ConvertTo(dstRaw conversion.Hub) error {
	switch dst := dstRaw.(type) {
	case *v1alpha5.Odyssey:
		dst.Spec.Password = o.Spec.Password
		dst.Spec.MaxConnections = o.Spec.MaxConnections
		if len(o.Spec.Extensions) > 0 {
			dst.Spec.Extensions = make([]v1alpha5.Extension, len(o.Spec.Extensions))
			for i := 0; i < len(o.Spec.Extensions); i++ {
				dst.Spec.Extensions[i] = v1alpha5.Extension(o.Spec.Extensions[i])
			}
		}
		dst.Spec.Deletable = o.Spec.Deletable

		dst.Spec.TemplateDatabase = o.Spec.TemplateDatabase

		if o.Spec.ExistingDatabase != nil {
			dst.Spec.ExistingDatabase = &v1alpha5.OdysseyExistingDatabase{
				Name:     o.Spec.ExistingDatabase.Name,
				User:     o.Spec.ExistingDatabase.User,
				Password: o.Spec.ExistingDatabase.Password,
			}
		}

		dst.Spec.EnablePoolReservePreparedStatement = o.Spec.EnablePoolReservePreparedStatement

		dst.ObjectMeta = o.ObjectMeta
		// Restore additional fields
		if additionalFieldsString, ok := o.Annotations[odysseyAdditionalFieldsAnnotationName]; ok {
			var additionalFields odysseyAdditionalFields
			if err := json.Unmarshal([]byte(additionalFieldsString), &additionalFields); err == nil {
				RestoreAdditionalFields(dst, &additionalFields)
			}

			delete(dst.Annotations, odysseyAdditionalFieldsAnnotationName)
		}

		dst.Status.GeneratedPassword = o.Status.GeneratedPassword
		dst.Status.Phase = v1alpha5.OdysseyStatusPhase(o.Status.Phase)
	default:
		return errors.New("unknown type")
	}

	return nil
}

// ConvertFrom converts from the Hub version (v1alpha5) to this version.
//
//goland:noinspection GoDeprecation
func (o *Odyssey) ConvertFrom(srcRaw conversion.Hub) error {
	switch src := srcRaw.(type) {
	case *v1alpha5.Odyssey:
		o.Spec.Password = src.Spec.Password
		o.Spec.MaxConnections = src.Spec.MaxConnections
		if len(src.Spec.Extensions) > 0 {
			o.Spec.Extensions = make([]Extension, len(src.Spec.Extensions))
			for i := 0; i < len(src.Spec.Extensions); i++ {
				o.Spec.Extensions[i] = Extension(src.Spec.Extensions[i])
			}
		}

		o.Spec.TemplateDatabase = src.Spec.TemplateDatabase

		o.Spec.Deletable = src.Spec.Deletable

		if src.Spec.ExistingDatabase != nil {
			o.Spec.ExistingDatabase = &OdysseyExistingDatabase{
				Name:     src.Spec.ExistingDatabase.Name,
				User:     src.Spec.ExistingDatabase.User,
				Password: src.Spec.ExistingDatabase.Password,
			}
		} else {
			o.Spec.ExistingDatabase = nil
		}

		o.Spec.EnablePoolReservePreparedStatement = src.Spec.EnablePoolReservePreparedStatement

		o.ObjectMeta = src.ObjectMeta
		// Save additional fields
		var additionalFields = odysseyAdditionalFields{}
		SaveAdditionalFields(&additionalFields, src)
		if additionalFieldsBytes, err := json.Marshal(additionalFields); err == nil {
			if o.Annotations == nil {
				o.Annotations = make(map[string]string)
			}

			o.Annotations[odysseyAdditionalFieldsAnnotationName] = string(additionalFieldsBytes)
		}

		o.Status.GeneratedPassword = src.Status.GeneratedPassword
		o.Status.Phase = OdysseyStatusPhase(src.Status.Phase)
	default:
		return errors.New("unknown type")
	}

	return nil
}
