/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package database

import (
	"time"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
)

func ServiceNameFromOdysseyInstance(prefix, instanceName string) string {
	result := prefix
	if instanceName != "master" {
		result = result + "-" + instanceName
	}

	return result
}

func DeploymentNameFromOdysseyInstance(prefix, instanceName string) string {
	result := prefix
	if instanceName != "master" {
		result = result + "-" + instanceName
	}

	return result
}

func OdysseyPoolToUserPool(pool *v1alpha5.OdysseyPool) v1alpha2.OdysseyInstancePool {
	if pool != nil && *pool == v1alpha5.PoolSession {
		return v1alpha2.PoolSession
	}

	return v1alpha2.PoolTransaction
}

func OdysseyToDatabase(odyssey v1alpha5.Odyssey, hostPort v1alpha1.PostgresEndpointHostPort, odysseyDatabase *v1alpha2.Database) {
	odysseyDatabase.Name = odyssey.GetDatabaseName()
	odysseyDatabase.Password = odyssey.GetSpecifiedOrGeneratedPassword()

	odysseyDatabase.User.Authentication = v1alpha2.AuthenticationMD5
	odysseyDatabase.User.Pool = OdysseyPoolToUserPool(odyssey.Spec.Pool)
	odysseyDatabase.User.ClientMax = odyssey.Spec.MaxConnections
	odysseyDatabase.User.PoolTTL = 1 * time.Minute
	odysseyDatabase.User.PoolDiscard = false
	odysseyDatabase.User.PoolCancel = true
	odysseyDatabase.User.ApplicationNameAddHost = true
	odysseyDatabase.User.ClientForwardError = true
	odysseyDatabase.User.ServerLifetime = 1 * time.Hour
	odysseyDatabase.User.LogDebug = false
	odysseyDatabase.User.PoolReservePreparedStatement = PointerDeref(odyssey.Spec.EnablePoolReservePreparedStatement, false)
	odysseyDatabase.User.Quantiles = "0.99,0.95,0.5"

	if odyssey.Spec.ExistingDatabase != nil {
		odysseyDatabase.Storage.Name = Pointer(odyssey.Spec.ExistingDatabase.Name)
		odysseyDatabase.Storage.User = Pointer(odyssey.Spec.ExistingDatabase.User)
		odysseyDatabase.Storage.Password = Pointer(odyssey.Spec.ExistingDatabase.Password)
	}

	odysseyDatabase.Storage.Host = hostPort.Host
	odysseyDatabase.Storage.Port = uint16(*hostPort.Port)
}
