/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha5

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	. "git.monitorsoft.ru/k8s/melete/internal/common"
)

// OdysseySpec defines the desired state of Odyssey
type OdysseySpec struct {
	// Password for database.
	// If is empty password, it will be generated and set to .status.generatedPassword.
	// +kubebuilder:validation:Optional
	// +kubebuilder:validation:XIntOrString
	// +kubebuilder:validation:MinLength=4
	Password *string `json:"password,omitempty"`
	// Connections limit for database and user.
	// Details on https://github.com/yandex/odyssey/blob/1.3/documentation/configuration.md#pool_size-integer
	// +kubebuilder:validation:Required
	// +kubebuilder:default=1
	// +kubebuilder:validation:Minimum=1
	MaxConnections uint `json:"maxConnections"`
	// List of extensions to be created in database.
	// +kubebuilder:validation:Optional
	Extensions []Extension `json:"extensions,omitempty"`
	// Mark database deletable after delete resource from cluster.
	// +kubebuilder:validation:Optional
	Deletable *bool `json:"deletable,omitempty"`

	// Name of database for use that template in create new database.
	// Details on https://www.postgresql.org/docs/13/sql-createdatabase.html
	// +kubebuilder:validation:Optional
	TemplateDatabase *string `json:"templateDatabase,omitempty"`

	// If set, the existing database will be used.
	// An internal database name and username will be created for access.
	// +kubebuilder:validation:Optional
	ExistingDatabase *OdysseyExistingDatabase `json:"existingDatabase,omitempty"`

	// Enable prepared statements.
	// Details on https://github.com/yandex/odyssey/blob/1.3/config-examples/odyssey-dev.conf#L87
	// +kubebuilder:validation:Optional
	// +kubebuilder:default=false
	EnablePoolReservePreparedStatement *bool `json:"enablePoolReservePreparedStatement,omitempty"`

	// Pool set pooling mode.
	// Details on https://github.com/yandex/odyssey/blob/1.3/documentation/configuration.md#pool-string
	// +kubebuilder:validation:Optional
	// +kubebuilder:validation:Enum=session;transaction
	// +kubebuilder:default="transaction"
	Pool *OdysseyPool `json:"pool,omitempty"`
}

// Extension in database.
// +kubebuilder:validation:MinLength=1
type Extension string

// OdysseyPool set pooling mode.
// Details on https://github.com/yandex/odyssey/blob/1.3/documentation/configuration.md#pool-string
type OdysseyPool string

//goland:noinspection GoUnusedConst
const (
	// PoolTransaction assign server connection to a client for a transaction processing
	PoolTransaction = OdysseyPool("transaction")
	// PoolSession assign server connection to a client until it disconnects
	PoolSession = OdysseyPool("session")
)

type OdysseyExistingDatabase struct {
	// Database name.
	// +kubebuilder:validation:Required
	Name string `json:"name"`
	// Username to access database.
	// +kubebuilder:validation:Required
	User string `json:"user"`
	// Password to access database.
	// +kubebuilder:validation:Required
	Password string `json:"password"`
}

type OdysseyInstanceReference struct {
	// API version of the referent.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=database.monitorsoft.ru
	Group string `json:"group"`
	// Kind of the referent.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=OdysseyInstance
	Kind string `json:"kind"`
	// Name of the referent.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinLength=1
	Name string `json:"name"`
}

type PostgresEndpointReference struct {
	// API version of the referent.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=database.monitorsoft.ru
	Group string `json:"group"`
	// Kind of the referent.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=PostgresEndpoint
	Kind string `json:"kind"`
	// Name of the referent.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinLength=1
	Name string `json:"name"`
	// Namespace of the referent.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinLength=1
	Namespace string `json:"namespace"`
}

type OdysseyStatusPhase string

//goland:noinspection GoUnusedConst
const (
	OdysseyStatusPhaseNone          = OdysseyStatusPhase("")
	OdysseyStatusPhasePending       = OdysseyStatusPhase("Pending")
	OdysseyStatusPhaseReady         = OdysseyStatusPhase("Ready")
	OdysseyStatusPhaseWaitPostgres  = OdysseyStatusPhase("Wait postgres")
	OdysseyStatusPhasePendingDelete = OdysseyStatusPhase("Pending delete")
)

//+kubebuilder:object:generate=true

// OdysseyStatus defines the observed state of Odyssey
type OdysseyStatus struct {
	// +kubebuilder:default=Pending
	// +kubebuilder:validation:Enum="";Pending;Ready;Wait postgres;Pending delete
	Phase OdysseyStatusPhase `json:"phase"`

	// GeneratedPassword will be generated when .spec.password is empty.
	// +kubebuilder:validation:Optional
	GeneratedPassword *string `json:"generatedPassword,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:path=odyssey
//+kubebuilder:storageversion
//+kubebuilder:printcolumn:name="Phase",type=string,JSONPath=`.status.phase`
//+kubebuilder:printcolumn:name="Max connections",type=integer,JSONPath=`.spec.maxConnections`
//+kubebuilder:printcolumn:name="Existing database",type=string,JSONPath=`.spec.existingDatabase.name`
//+kubebuilder:printcolumn:name="Odyssey instances",type=string,JSONPath=`.odysseyInstanceReferences.[*].name`

// Odyssey is the Schema for the odyssey API
type Odyssey struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// +kubebuilder:validation:Required
	Spec OdysseySpec `json:"spec,omitempty"`

	// Reference to v1alpha2.OdysseyInstance.
	// Will be added "master" and "replica" instances when it is empty.
	// +kubebuilder:validation:Optional
	OdysseyInstanceReferences []OdysseyInstanceReference `json:"odysseyInstanceReferences,omitempty"`

	// Reference to v1alpha1.PostgresEndpoint.
	// Will be added default when is empty and found in cluster.
	// +kubebuilder:validation:Optional
	PostgresEndpointReference *PostgresEndpointReference `json:"postgresEndpointReference,omitempty"`

	Status OdysseyStatus `json:"status,omitempty"`
}

// Hub marks this type as a conversion hub.
func (*Odyssey) Hub() {}

func (o *Odyssey) GetSpecifiedOrGeneratedPassword() string {
	return PointerDeref(o.Spec.Password, PointerDeref(o.Status.GeneratedPassword, ""))
}

func (o *Odyssey) GetDatabaseName() string {
	return o.Namespace + "." + o.Name
}

//+kubebuilder:object:root=true

// OdysseyList contains a list of Odyssey
type OdysseyList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Odyssey `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Odyssey{}, &OdysseyList{})
}
