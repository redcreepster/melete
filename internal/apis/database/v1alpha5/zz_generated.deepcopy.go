//go:build !ignore_autogenerated
// +build !ignore_autogenerated

/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by controller-gen. DO NOT EDIT.

package v1alpha5

import (
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Odyssey) DeepCopyInto(out *Odyssey) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	if in.OdysseyInstanceReferences != nil {
		in, out := &in.OdysseyInstanceReferences, &out.OdysseyInstanceReferences
		*out = make([]OdysseyInstanceReference, len(*in))
		copy(*out, *in)
	}
	if in.PostgresEndpointReference != nil {
		in, out := &in.PostgresEndpointReference, &out.PostgresEndpointReference
		*out = new(PostgresEndpointReference)
		**out = **in
	}
	in.Status.DeepCopyInto(&out.Status)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Odyssey.
func (in *Odyssey) DeepCopy() *Odyssey {
	if in == nil {
		return nil
	}
	out := new(Odyssey)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *Odyssey) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OdysseyExistingDatabase) DeepCopyInto(out *OdysseyExistingDatabase) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OdysseyExistingDatabase.
func (in *OdysseyExistingDatabase) DeepCopy() *OdysseyExistingDatabase {
	if in == nil {
		return nil
	}
	out := new(OdysseyExistingDatabase)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OdysseyInstanceReference) DeepCopyInto(out *OdysseyInstanceReference) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OdysseyInstanceReference.
func (in *OdysseyInstanceReference) DeepCopy() *OdysseyInstanceReference {
	if in == nil {
		return nil
	}
	out := new(OdysseyInstanceReference)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OdysseyList) DeepCopyInto(out *OdysseyList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]Odyssey, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OdysseyList.
func (in *OdysseyList) DeepCopy() *OdysseyList {
	if in == nil {
		return nil
	}
	out := new(OdysseyList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *OdysseyList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OdysseySpec) DeepCopyInto(out *OdysseySpec) {
	*out = *in
	if in.Password != nil {
		in, out := &in.Password, &out.Password
		*out = new(string)
		**out = **in
	}
	if in.Extensions != nil {
		in, out := &in.Extensions, &out.Extensions
		*out = make([]Extension, len(*in))
		copy(*out, *in)
	}
	if in.Deletable != nil {
		in, out := &in.Deletable, &out.Deletable
		*out = new(bool)
		**out = **in
	}
	if in.TemplateDatabase != nil {
		in, out := &in.TemplateDatabase, &out.TemplateDatabase
		*out = new(string)
		**out = **in
	}
	if in.ExistingDatabase != nil {
		in, out := &in.ExistingDatabase, &out.ExistingDatabase
		*out = new(OdysseyExistingDatabase)
		**out = **in
	}
	if in.EnablePoolReservePreparedStatement != nil {
		in, out := &in.EnablePoolReservePreparedStatement, &out.EnablePoolReservePreparedStatement
		*out = new(bool)
		**out = **in
	}
	if in.Pool != nil {
		in, out := &in.Pool, &out.Pool
		*out = new(OdysseyPool)
		**out = **in
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OdysseySpec.
func (in *OdysseySpec) DeepCopy() *OdysseySpec {
	if in == nil {
		return nil
	}
	out := new(OdysseySpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OdysseyStatus) DeepCopyInto(out *OdysseyStatus) {
	*out = *in
	if in.GeneratedPassword != nil {
		in, out := &in.GeneratedPassword, &out.GeneratedPassword
		*out = new(string)
		**out = **in
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OdysseyStatus.
func (in *OdysseyStatus) DeepCopy() *OdysseyStatus {
	if in == nil {
		return nil
	}
	out := new(OdysseyStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *PostgresEndpointReference) DeepCopyInto(out *PostgresEndpointReference) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new PostgresEndpointReference.
func (in *PostgresEndpointReference) DeepCopy() *PostgresEndpointReference {
	if in == nil {
		return nil
	}
	out := new(PostgresEndpointReference)
	in.DeepCopyInto(out)
	return out
}
