/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"errors"

	"k8s.io/apimachinery/pkg/util/json"
	"sigs.k8s.io/controller-runtime/pkg/conversion"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
)

var odysseyAdditionalFieldsAnnotationName = "melete.monitorsoft.ru/additional-fields-" + GroupVersion.Version

//+kubebuilder:object:generate=false

type odysseyAdditionalFields struct {
	Spec struct {
		ExistingDatabase                   *v1alpha5.OdysseyExistingDatabase `json:"existingDatabase,omitempty"`
		EnablePoolReservePreparedStatement *bool                             `json:"enablePoolReservePreparedStatement,omitempty"`
		Pool                               *v1alpha5.OdysseyPool             `json:"pool,omitempty"`
	} `json:"spec"`
	OdysseyInstanceReferences []v1alpha5.OdysseyInstanceReference `json:"odysseyInstanceReferences,omitempty"`
	PostgresEndpointReference *v1alpha5.PostgresEndpointReference `json:"postgresEndpointReference,omitempty"`
}

// ConvertTo converts this Odyssey to the Hub version (v1alpha5).
//
//goland:noinspection GoDeprecation,GoReceiverNames
func (o *Odyssey) ConvertTo(dstRaw conversion.Hub) error {
	switch dst := dstRaw.(type) {
	case *v1alpha5.Odyssey:
		dst.Spec.Password = o.Spec.Password
		dst.Spec.MaxConnections = o.Spec.MaxConnections
		if len(o.Spec.Extensions) > 0 {
			dst.Spec.Extensions = make([]v1alpha5.Extension, len(o.Spec.Extensions))
			for i := 0; i < len(o.Spec.Extensions); i++ {
				dst.Spec.Extensions[i] = v1alpha5.Extension(o.Spec.Extensions[i])
			}
		}
		dst.Spec.Deletable = o.Spec.Deletable

		dst.Spec.TemplateDatabase = o.Spec.TemplateDatabase

		dst.Spec.ExistingDatabase = nil

		dst.Spec.EnablePoolReservePreparedStatement = Pointer(false)

		dst.ObjectMeta = o.ObjectMeta
		// Restore additional fields
		if additionalFieldsString, ok := o.Annotations[odysseyAdditionalFieldsAnnotationName]; ok {
			var additionalFields odysseyAdditionalFields
			if err := json.Unmarshal([]byte(additionalFieldsString), &additionalFields); err == nil {
				RestoreAdditionalFields(dst, &additionalFields)
			}

			delete(dst.Annotations, odysseyAdditionalFieldsAnnotationName)
		}

		dst.Status.GeneratedPassword = o.Status.GeneratedPassword
		if o.Status.Ready != nil {
			if *o.Status.Ready {
				dst.Status.Phase = v1alpha5.OdysseyStatusPhaseReady
			} else {
				dst.Status.Phase = v1alpha5.OdysseyStatusPhasePending
			}
		} else {
			dst.Status.Phase = v1alpha5.OdysseyStatusPhaseNone
		}
	default:
		return errors.New("unknown type")
	}

	return nil
}

// ConvertFrom converts from the Hub version (v1alpha5) to this version.
//
//goland:noinspection GoDeprecation,GoReceiverNames
func (o *Odyssey) ConvertFrom(srcRaw conversion.Hub) error {
	switch src := srcRaw.(type) {
	case *v1alpha5.Odyssey:
		o.Spec.Password = src.Spec.Password
		o.Spec.MaxConnections = src.Spec.MaxConnections
		if len(src.Spec.Extensions) > 0 {
			o.Spec.Extensions = make([]string, len(src.Spec.Extensions))
			for i := 0; i < len(src.Spec.Extensions); i++ {
				o.Spec.Extensions[i] = string(src.Spec.Extensions[i])
			}
		}

		o.Spec.TemplateDatabase = src.Spec.TemplateDatabase

		o.Spec.Deletable = src.Spec.Deletable

		o.ObjectMeta = src.ObjectMeta
		// Save additional fields
		var additionalFields = odysseyAdditionalFields{}
		SaveAdditionalFields(&additionalFields, src)
		if additionalFieldsBytes, err := json.Marshal(additionalFields); err == nil {
			if o.Annotations == nil {
				o.Annotations = make(map[string]string)
			}

			o.Annotations[odysseyAdditionalFieldsAnnotationName] = string(additionalFieldsBytes)
		}

		o.Status.GeneratedPassword = src.Status.GeneratedPassword
		switch src.Status.Phase {
		case v1alpha5.OdysseyStatusPhasePending, v1alpha5.OdysseyStatusPhaseWaitPostgres:
			o.Status.Ready = Pointer(false)
		case v1alpha5.OdysseyStatusPhaseReady, v1alpha5.OdysseyStatusPhasePendingDelete:
			o.Status.Ready = Pointer(true)
		default:
			o.Status.Ready = nil
		}
	default:
		return errors.New("unknown type")
	}

	return nil
}
