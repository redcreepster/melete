/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// OdysseySpec defines the desired state of Odyssey
type OdysseySpec struct {
	// +kubebuilder:validation:Optional
	Password *string `json:"password,omitempty"`
	// +kubebuilder:validation:Required
	MaxConnections uint `json:"maxConnections"`
	// +kubebuilder:validation:Optional
	Extensions []string `json:"extensions,omitempty"`

	// +kubebuilder:validation:Optional
	TemplateDatabase *string `json:"templateDatabase,omitempty"`

	// +kubebuilder:validation:Optional
	Deletable *bool `json:"deletable,omitempty"`
}

//+kubebuilder:object:generate=true

// OdysseyStatus defines the observed state of Odyssey
type OdysseyStatus struct {
	// Ready is old indicator about status.
	//
	//+kubebuilder:validation:Optional
	//+nullable
	Ready *bool `json:"ready"`

	// GeneratedPassword is set when .spec.password is empty
	// +kubebuilder:validation:Optional
	GeneratedPassword *string `json:"generatedPassword,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:path=odyssey
//+kubebuilder:printcolumn:name="Ready",type=boolean,JSONPath=`.status.ready`
//+kubebuilder:printcolumn:name="Max connections",type=integer,JSONPath=`.spec.maxConnections`
//+kubebuilder:printcolumn:name="Template database",type=string,JSONPath=`.spec.templateDatabase`
//+kubebuilder:deprecatedversion:warning="database.monitorsoft.ru/v1alpha1 Odyssey is deprecated in 0.2.0+, unavailable in 0.4.0+; use database.monitorsoft.ru/v1alpha5 Odyssey"

// Odyssey is the Schema for the odyssey API
//
// Deprecated: Use v1alpha5.Odyssey
//
//goland:noinspection GoDeprecation
type Odyssey struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   OdysseySpec   `json:"spec,omitempty"`
	Status OdysseyStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// OdysseyList contains a list of Odyssey
//
// Deprecated: Use v1alpha5.OdysseyList
//
//goland:noinspection GoDeprecation
type OdysseyList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Odyssey `json:"items"`
}

func init() {
	//goland:noinspection GoDeprecation
	SchemeBuilder.Register(&Odyssey{}, &OdysseyList{})
}
