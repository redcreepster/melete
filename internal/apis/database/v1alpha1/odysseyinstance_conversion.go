/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"errors"

	"k8s.io/apimachinery/pkg/util/json"
	"sigs.k8s.io/controller-runtime/pkg/conversion"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
)

var odysseyInstanceAdditionalFieldsAnnotationName = "melete.monitorsoft.ru/additional-fields-" + GroupVersion.Version

//+kubebuilder:object:generate=false

type odysseyInstanceAdditionalFields struct {
	Config *string `json:"config,omitempty"`
}

// ConvertTo converts this Odyssey to the Hub version (v1alpha2).
//
//goland:noinspection GoDeprecation
func (o *OdysseyInstance) ConvertTo(dstRaw conversion.Hub) error {
	switch dst := dstRaw.(type) {
	case *v1alpha2.OdysseyInstance:
		dst.Replicas = o.Replicas

		for _, database := range o.Databases {
			dst.Databases = append(dst.Databases, v1alpha2.Database{
				Name:     database.Name,
				Password: database.Password,
				User: v1alpha2.User{
					Authentication:               v1alpha2.Authentication(database.User.Authentication),
					Pool:                         v1alpha2.OdysseyInstancePool(database.User.Pool),
					ClientMax:                    database.User.ClientMax,
					PoolTTL:                      database.User.PoolTTL,
					PoolDiscard:                  database.User.PoolDiscard,
					PoolCancel:                   database.User.PoolCancel,
					ApplicationNameAddHost:       database.User.ApplicationNameAddHost,
					ClientForwardError:           database.User.ClientForwardError,
					ServerLifetime:               database.User.ServerLifetime,
					LogDebug:                     database.User.LogDebug,
					PoolReservePreparedStatement: database.User.PoolReservePreparedStatement,
					Quantiles:                    v1alpha2.Quantiles(database.User.Quantiles),
				},
				Storage: v1alpha2.Storage{
					Name:     database.Storage.Name,
					User:     database.Storage.User,
					Password: database.Storage.Password,
					Host:     database.Storage.Host,
					Port:     database.Storage.Port,
				},
			})
		}

		dst.ObjectMeta = o.ObjectMeta
		// Restore additional fields
		if additionalFieldsString, ok := o.Annotations[odysseyInstanceAdditionalFieldsAnnotationName]; ok {
			var additionalFields odysseyInstanceAdditionalFields
			if err := json.Unmarshal([]byte(additionalFieldsString), &additionalFields); err == nil {
				RestoreAdditionalFields(dst, &additionalFields)
			}

			delete(dst.Annotations, odysseyAdditionalFieldsAnnotationName)
		}

		if o.Status.Phase == OdysseyInstanceStatusPhasePendingDelete {
			dst.Status.Phase = v1alpha2.OdysseyInstanceStatusPhaseReady
		} else {
			dst.Status.Phase = v1alpha2.OdysseyInstanceStatusPhase(o.Status.Phase)
		}
		dst.Status.MaxConnections = o.Status.MaxConnections
		dst.Status.RoutesCount = o.Status.DatabasesCount
	default:
		return errors.New("unknown type")
	}

	return nil
}

// ConvertFrom converts from the Hub version (v1alpha2) to this version.
//
//goland:noinspection GoDeprecation
func (o *OdysseyInstance) ConvertFrom(srcRaw conversion.Hub) error {
	switch src := srcRaw.(type) {
	case *v1alpha2.OdysseyInstance:
		o.Replicas = src.Replicas

		for _, database := range src.Databases {
			o.Databases = append(o.Databases, Database{
				Name:     database.Name,
				Password: database.Password,
				User: User{
					Authentication:               Authentication(database.User.Authentication),
					Pool:                         OdysseyInstancePool(database.User.Pool),
					ClientMax:                    database.User.ClientMax,
					PoolTTL:                      database.User.PoolTTL,
					PoolDiscard:                  database.User.PoolDiscard,
					PoolCancel:                   database.User.PoolCancel,
					ApplicationNameAddHost:       database.User.ApplicationNameAddHost,
					ClientForwardError:           database.User.ClientForwardError,
					ServerLifetime:               database.User.ServerLifetime,
					LogDebug:                     database.User.LogDebug,
					PoolReservePreparedStatement: database.User.PoolReservePreparedStatement,
					Quantiles:                    Quantiles(database.User.Quantiles),
				},
				Storage: Storage{
					Name:     database.Storage.Name,
					User:     database.Storage.User,
					Password: database.Storage.Password,
					Host:     database.Storage.Host,
					Port:     database.Storage.Port,
				},
			})
		}

		o.ObjectMeta = src.ObjectMeta
		// Save additional fields
		var additionalFields = odysseyInstanceAdditionalFields{}
		SaveAdditionalFields(&additionalFields, src)
		if additionalFieldsBytes, err := json.Marshal(additionalFields); err == nil {
			if o.Annotations == nil {
				o.Annotations = make(map[string]string)
			}

			o.Annotations[odysseyInstanceAdditionalFieldsAnnotationName] = string(additionalFieldsBytes)
		}

		if !src.GetDeletionTimestamp().IsZero() {
			o.Status.Phase = OdysseyInstanceStatusPhasePendingDelete
		} else {
			o.Status.Phase = OdysseyInstanceStatusPhase(src.Status.Phase)
		}
		o.Status.MaxConnections = src.Status.MaxConnections
		o.Status.DatabasesCount = src.Status.RoutesCount
	default:
		return errors.New("unknown type")
	}

	return nil
}
