/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// PostgresEndpointUser contains credentials for access to postgres.
type PostgresEndpointUser struct {
	// Name of user.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinLength=1
	Name string `json:"name"`

	// Password of user.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:XIntOrString
	// +kubebuilder:validation:MinLength=1
	Password string `json:"password"`
}

type PostgresEndpointHostPort struct {
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinLength=1
	Host string `json:"host"`

	// +kubebuilder:validation:Required
	Port *uint32 `json:"port"`
}

type PostgresEndpointReplica struct {
	// Replica name
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinLength=1
	Name                     string `json:"name"`
	PostgresEndpointHostPort `json:",inline"`
}

// PostgresEndpointSpec defines the desired state of PostgresEndpoint
type PostgresEndpointSpec struct {
	PostgresEndpointHostPort `json:",inline"`

	// +kubebuilder:validation:Required
	User PostgresEndpointUser `json:"user"`

	// Name of database for connecting.
	// Will be used superUser.name when is empty.
	// +kubebuilder:validation:Optional
	DefaultDatabase *string `json:"defaultDatabase,omitempty"`

	// Additional replicas.
	// +kubebuilder:validation:Optional
	Replicas []PostgresEndpointReplica `json:"replicas,omitempty"`
}

//+kubebuilder:object:generate=true

// PostgresEndpointStatus defines the observed state of PostgresEndpoint
type PostgresEndpointStatus struct {
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:path=postgres-endpoints
//+kubebuilder:storageversion

// PostgresEndpoint is the Schema for the postgres endpoint API
type PostgresEndpoint struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PostgresEndpointSpec   `json:"spec,omitempty"`
	Status PostgresEndpointStatus `json:"status,omitempty"`
}

func (e *PostgresEndpoint) GetHostPortByName(name string) *PostgresEndpointHostPort {
	if name == "master" {
		return &e.Spec.PostgresEndpointHostPort
	}

	for _, replica := range e.Spec.Replicas {
		if replica.Name == name {
			return &replica.PostgresEndpointHostPort
		}
	}

	return nil
}

//+kubebuilder:object:root=true

// PostgresEndpointList contains a list of PostgresEndpoint
type PostgresEndpointList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []PostgresEndpoint `json:"items"`
}

func init() {
	SchemeBuilder.Register(&PostgresEndpoint{}, &PostgresEndpointList{})
}
