/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package webhooks

import (
	"context"
	"errors"

	"k8s.io/apimachinery/pkg/types"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha3"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha4"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
	"git.monitorsoft.ru/k8s/melete/internal/postgres"
)

var (
	NewPostgresClient = postgres.NewPostgresClient
)

// SetupOdysseyWebhookWithManager
//
//nolint:golint,staticcheck
func SetupOdysseyWebhookWithManager(mgr ctrl.Manager) error {
	//goland:noinspection GoDeprecation
	if err := ctrl.NewWebhookManagedBy(mgr).For(&v1alpha1.Odyssey{}).Complete(); err != nil {
		return err
	}
	//goland:noinspection GoDeprecation
	if err := ctrl.NewWebhookManagedBy(mgr).For(&v1alpha2.Odyssey{}).Complete(); err != nil {
		return err
	}
	//goland:noinspection GoDeprecation
	if err := ctrl.NewWebhookManagedBy(mgr).For(&v1alpha3.Odyssey{}).Complete(); err != nil {
		return err
	}
	//goland:noinspection GoDeprecation
	if err := ctrl.NewWebhookManagedBy(mgr).For(&v1alpha4.Odyssey{}).Complete(); err != nil {
		return err
	}

	if err := ctrl.NewWebhookManagedBy(mgr).
		For(&v1alpha5.Odyssey{}).
		WithDefaulter(&OdysseyDefaulter{mgr.GetClient()}).
		WithValidator(&OdysseyValidator{mgr.GetClient()}).
		Complete(); err != nil {
		return err
	}

	return nil
}

//+kubebuilder:webhook:path=/mutate-database-monitorsoft-ru-v1alpha5-odyssey,mutating=true,failurePolicy=fail,sideEffects=None,groups=database.monitorsoft.ru,resources=odyssey,verbs=create;update,versions=v1alpha5,name=modyssey.kb.io,admissionReviewVersions=v1

//+kubebuilder:object:generate=false

type OdysseyDefaulter struct {
	k8sClient client.Client
}

var _ webhook.CustomDefaulter = &OdysseyDefaulter{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (o *OdysseyDefaulter) Default(ctx context.Context, obj runtime.Object) error {
	odyssey := obj.(*v1alpha5.Odyssey)

	log := logf.FromContext(ctx).
		WithName("odyssey-resource").
		WithValues("name", odyssey.Name).
		WithValues("namespace", odyssey.Namespace)

	log.Info("default")

	if !odyssey.GetDeletionTimestamp().IsZero() {
		log.Info("Resource deleted. Skip adding finalizer.")

		return nil
	}

	controllerutil.AddFinalizer(odyssey, FinalizerName)

	if len(odyssey.OdysseyInstanceReferences) == 0 {
		odyssey.OdysseyInstanceReferences = []v1alpha5.OdysseyInstanceReference{
			{
				Group: "database.monitorsoft.ru",
				Kind:  "OdysseyInstance",
				Name:  "master",
			},
			{
				Group: "database.monitorsoft.ru",
				Kind:  "OdysseyInstance",
				Name:  "replica",
			},
		}
	}

	if odyssey.PostgresEndpointReference == nil {
		postgresEndpoints := v1alpha1.PostgresEndpointList{}
		if err := o.k8sClient.List(ctx, &postgresEndpoints); err != nil {
			return err
		}

		var defaultPostgresEndpoint *v1alpha1.PostgresEndpoint
		for _, postgresEndpoint := range postgresEndpoints.Items {
			if value, ok := postgresEndpoint.Annotations["melete.monitorsoft.ru/is-default-endpoint"]; ok && value == "true" {
				if defaultPostgresEndpoint == nil || postgresEndpoint.Namespace == odyssey.Namespace {
					defaultPostgresEndpoint = &postgresEndpoint
				}
			}
		}

		if defaultPostgresEndpoint == nil {
			return errors.New("default postgres endpoint not found")
		}
		odyssey.PostgresEndpointReference = &v1alpha5.PostgresEndpointReference{
			Group:     defaultPostgresEndpoint.GroupVersionKind().Group,
			Kind:      defaultPostgresEndpoint.GroupVersionKind().Kind,
			Name:      defaultPostgresEndpoint.Name,
			Namespace: defaultPostgresEndpoint.Namespace,
		}
	}

	return nil
}

func NewOdysseyDefaulter(k8sClient client.Client) webhook.CustomDefaulter {
	return &OdysseyDefaulter{
		k8sClient: k8sClient,
	}
}

//+kubebuilder:webhook:path=/validate-database-monitorsoft-ru-v1alpha5-odyssey,mutating=false,failurePolicy=fail,sideEffects=None,groups=database.monitorsoft.ru,resources=odyssey,verbs=create;update,versions=v1alpha5,name=vodyssey.kb.io,admissionReviewVersions=v1

//+kubebuilder:object:generate=false

type OdysseyValidator struct {
	k8sClient client.Client
}

var _ webhook.CustomValidator = &OdysseyValidator{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (c *OdysseyValidator) ValidateCreate(ctx context.Context, obj runtime.Object) error {
	odyssey := obj.(*v1alpha5.Odyssey)

	log := logf.FromContext(ctx).
		WithName("odyssey-resource").
		WithValues("name", odyssey.Name).
		WithValues("namespace", odyssey.Namespace)
	ctx = logf.IntoContext(ctx, log)

	log.Info("validate create")

	return c.validate(ctx, odyssey)
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (c *OdysseyValidator) ValidateUpdate(ctx context.Context, _, newObj runtime.Object) error {
	odyssey := newObj.(*v1alpha5.Odyssey)

	log := logf.FromContext(ctx).
		WithName("odyssey-resource").
		WithValues("name", odyssey.Name).
		WithValues("namespace", odyssey.Namespace)
	ctx = logf.IntoContext(ctx, log)

	log.Info("validate update")

	return c.validate(ctx, odyssey)
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (c *OdysseyValidator) ValidateDelete(_ context.Context, _ runtime.Object) error {
	return nil
}

func (c *OdysseyValidator) validate(ctx context.Context, odyssey *v1alpha5.Odyssey) error {
	log := logf.FromContext(ctx)

	var allErrs field.ErrorList

	extensionsPath := field.NewPath("spec").Child("extensions")

	for i, a := range odyssey.Spec.Extensions {
		for j, b := range odyssey.Spec.Extensions {
			if i != j && a == b {
				fldPath := extensionsPath.Index(i)
				allErrs = append(allErrs, field.Invalid(fldPath, a, "Duplicated extension"))
			}
		}
	}

	var checkExtensions = len(odyssey.Spec.Extensions) > 0

	var postgresClient postgres.Client
	if checkExtensions {
		lookupKey := types.NamespacedName{
			Namespace: odyssey.PostgresEndpointReference.Namespace,
			Name:      odyssey.PostgresEndpointReference.Name,
		}
		var postgresEndpoint v1alpha1.PostgresEndpoint
		if err := c.k8sClient.Get(ctx, lookupKey, &postgresEndpoint); err != nil {
			log.Error(err, "Unable get postgres endpoint")
			allErrs = append(allErrs, field.InternalError(field.NewPath("postgresEndpointReference"), errors.New("unable get postgres endpoint")))

			checkExtensions = false
		} else {
			postgresClient = NewPostgresClient(postgres.Credentials{
				Host:     postgresEndpoint.Spec.Host,
				Port:     uint16(*postgresEndpoint.Spec.Port),
				Username: postgresEndpoint.Spec.User.Name,
				Password: postgresEndpoint.Spec.User.Password,
			})

			defer postgresClient.Close(ctx)
		}
	}

	var databaseExtensions []string
	if checkExtensions {
		if extensions, err := postgresClient.GetExtensions(ctx); err != nil {
			log.Error(err, "Unable get extensions")
			allErrs = append(allErrs, field.InternalError(extensionsPath, errors.New("unable get extensions")))

			checkExtensions = false
		} else {
			databaseExtensions = extensions

			log.V(5).Info("Database extensions list", "extensions", extensions)
		}

		// Early close postgres connection
		postgresClient.Close(ctx)
	}

	if checkExtensions {
		log = log.WithValues("extensions", odyssey.Spec.Extensions)
		for i, extension := range odyssey.Spec.Extensions {
			found := false
			for _, databaseExtension := range databaseExtensions {
				if string(extension) == databaseExtension {
					found = true

					break
				}
			}
			if !found {
				fieldPath := extensionsPath.Index(i)
				allErrs = append(allErrs, field.Invalid(fieldPath, extension, "Extension not found in database"))

				log.V(5).Info("Extension not found in database", "path", fieldPath.String(), "name", extension)
			}
		}
	}

	if len(allErrs) == 0 {
		return nil
	}

	return apierrors.NewInvalid(odyssey.GroupVersionKind().GroupKind(), odyssey.Name, allErrs)
}
