/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package webhooks

import (
	"context"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
)

// SetupOdysseyInstanceWebhookWithManager
//
//nolint:golint,staticcheck
func SetupOdysseyInstanceWebhookWithManager(mgr ctrl.Manager, defaultOdysseyInstanceConfig []byte) error {
	//goland:noinspection GoDeprecation
	if err := ctrl.NewWebhookManagedBy(mgr).For(&v1alpha1.OdysseyInstance{}).Complete(); err != nil {
		return err
	}
	if err := ctrl.NewWebhookManagedBy(mgr).
		For(&v1alpha2.OdysseyInstance{}).
		WithDefaulter(&OdysseyInstanceDefaulter{defaultOdysseyInstanceConfig}).
		Complete(); err != nil {
		return err
	}

	return nil
}

//+kubebuilder:webhook:path=/mutate-database-monitorsoft-ru-v1alpha2-odysseyinstance,mutating=true,failurePolicy=fail,sideEffects=None,groups=database.monitorsoft.ru,resources=odysseyinstances,verbs=create;update,versions=v1alpha2,name=modysseyinstance.kb.io,admissionReviewVersions=v1

var _ admission.CustomDefaulter = &OdysseyInstanceDefaulter{}

type OdysseyInstanceDefaulter struct {
	DefaultOdysseyInstanceConfig []byte
}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (c *OdysseyInstanceDefaulter) Default(ctx context.Context, obj runtime.Object) error {
	instance := obj.(*v1alpha2.OdysseyInstance)

	log := logf.FromContext(ctx).
		WithName("odysseyinstance-resource").
		WithValues("name", instance.Name)

	log.Info("default")

	if !instance.GetDeletionTimestamp().IsZero() {
		log.Info("Resource deleted. Skip adding finalizer.")

		return nil
	}

	controllerutil.AddFinalizer(instance, FinalizerName)

	if instance.Labels == nil {
		instance.Labels = make(map[string]string, 1)
	}

	instance.Labels["app.kubernetes.io/managed-by"] = "melete"

	if instance.Config == nil || *instance.Config == "" {
		instance.Config = Pointer(string(c.DefaultOdysseyInstanceConfig))
	}

	return nil
}

func NewOdysseyInstanceDefaulter(defaultOdysseyInstanceConfig []byte) admission.CustomDefaulter {
	return &OdysseyInstanceDefaulter{
		DefaultOdysseyInstanceConfig: defaultOdysseyInstanceConfig,
	}
}
