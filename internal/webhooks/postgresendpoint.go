/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package webhooks

import (
	"context"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"

	. "git.monitorsoft.ru/k8s/melete/internal/common"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
)

func SetupPostgresEndpointWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(&v1alpha1.PostgresEndpoint{}).
		WithDefaulter(&PostgresEndpointDefaulter{}).
		Complete()
}

//+kubebuilder:webhook:path=/mutate-database-monitorsoft-ru-v1alpha1-postgresendpoint,mutating=true,failurePolicy=fail,sideEffects=None,groups=database.monitorsoft.ru,resources=postgres-endpoints,verbs=create;update,versions=v1alpha1,name=mpostgresendpoint.kb.io,admissionReviewVersions=v1

type PostgresEndpointDefaulter struct {
}

var _ webhook.CustomDefaulter = &PostgresEndpointDefaulter{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (o *PostgresEndpointDefaulter) Default(ctx context.Context, obj runtime.Object) error {
	endpoint := obj.(*v1alpha1.PostgresEndpoint)

	log := logf.FromContext(ctx).
		WithName("postgres-endpoints-resource").
		WithValues("name", endpoint.Name).
		WithValues("namespace", endpoint.Namespace)

	log.Info("default")

	if !endpoint.GetDeletionTimestamp().IsZero() {
		log.Info("Resource deleted. Skip adding finalizer.")

		return nil
	}

	if endpoint.Spec.DefaultDatabase == nil {
		endpoint.Spec.DefaultDatabase = Pointer(endpoint.Spec.User.Name)
	}

	return nil
}
