package common

import (
	"math/rand"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/json"
)

func Test(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecs(t, "Webhook Suite")
}

var _ = It("GeneratePassword", func() {
	rngSource = rand.NewSource(0)
	Expect(GeneratePassword(GeneratedPasswordLength)).Should(Equal("Munera9Ri2CVtk4u"))
	Expect(GeneratePassword(GeneratedPasswordLength)).Should(Equal("hOMCJCeqVYMKZadM"))
})

var _ = It("BoolToYesNo", func() {
	Expect(BoolToYesNo(true)).Should(Equal("yes"))
	Expect(BoolToYesNo(false)).Should(Equal("no"))
})

var _ = Describe("Pointer", func() {
	It("bool", func() {
		Expect(Pointer[bool](true)).Should(HaveValue(Equal(true)))
	})
	It("uint8", func() {
		Expect(Pointer[uint8](123)).Should(HaveValue(Equal(uint8(123))))
	})
	It("uint16", func() {
		Expect(Pointer[uint16](123)).Should(HaveValue(Equal(uint16(123))))
	})
	It("uint32", func() {
		Expect(Pointer[uint32](123)).Should(HaveValue(Equal(uint32(123))))
	})
	It("uint64", func() {
		Expect(Pointer[uint64](123)).Should(HaveValue(Equal(uint64(123))))
	})
	It("int8", func() {
		Expect(Pointer[int8](123)).Should(HaveValue(Equal(int8(123))))
	})
	It("int16", func() {
		Expect(Pointer[int16](123)).Should(HaveValue(Equal(int16(123))))
	})
	It("int32", func() {
		Expect(Pointer[int32](123)).Should(HaveValue(Equal(int32(123))))
	})
	It("int64", func() {
		Expect(Pointer[int64](123)).Should(HaveValue(Equal(int64(123))))
	})
	It("float32", func() {
		Expect(Pointer[float32](123)).Should(HaveValue(Equal(float32(123))))
	})
	It("float64", func() {
		Expect(Pointer[float64](123)).Should(HaveValue(Equal(float64(123))))
	})
	It("string", func() {
		Expect(Pointer[string]("some-string")).Should(HaveValue(Equal("some-string")))
	})
	It("int", func() {
		Expect(Pointer[int](123)).Should(HaveValue(Equal(int(123))))
	})
	It("uint", func() {
		Expect(Pointer[uint](123)).Should(HaveValue(Equal(uint(123))))
	})
	It("byte", func() {
		Expect(Pointer[byte](123)).Should(HaveValue(Equal(byte(123))))
	})
	It("struct", func() {
		type testStruct struct {
			f string
		}
		Expect(Pointer(testStruct{f: "s"})).Should(HaveValue(Equal(testStruct{f: "s"})))
	})
})

var _ = Describe("PointerDeref", func() {
	It("bool", func() {
		Expect(PointerDeref[bool](Pointer[bool](true), false)).Should(Equal(true))
		Expect(PointerDeref[bool](nil, false)).Should(Equal(false))
	})
	It("uint8", func() {
		Expect(PointerDeref[uint8](Pointer[uint8](123), 0)).Should(Equal(uint8(123)))
		Expect(PointerDeref[uint8](nil, 0)).Should(Equal(uint8(0)))
	})
	It("uint16", func() {
		Expect(PointerDeref[uint16](Pointer[uint16](123), 0)).Should(Equal(uint16(123)))
		Expect(PointerDeref[uint16](nil, 0)).Should(Equal(uint16(0)))
	})
	It("uint32", func() {
		Expect(PointerDeref[uint32](Pointer[uint32](123), 0)).Should(Equal(uint32(123)))
		Expect(PointerDeref[uint32](nil, 0)).Should(Equal(uint32(0)))
	})
	It("uint64", func() {
		Expect(PointerDeref[uint64](Pointer[uint64](123), 0)).Should(Equal(uint64(123)))
		Expect(PointerDeref[uint64](nil, 0)).Should(Equal(uint64(0)))
	})
	It("int8", func() {
		Expect(PointerDeref[int8](Pointer[int8](123), 0)).Should(Equal(int8(123)))
		Expect(PointerDeref[int8](nil, 0)).Should(Equal(int8(0)))
	})
	It("int16", func() {
		Expect(PointerDeref[int16](Pointer[int16](123), 0)).Should(Equal(int16(123)))
		Expect(PointerDeref[int16](nil, 0)).Should(Equal(int16(0)))
	})
	It("int32", func() {
		Expect(PointerDeref[int32](Pointer[int32](123), 0)).Should(Equal(int32(123)))
		Expect(PointerDeref[int32](nil, 0)).Should(Equal(int32(0)))
	})
	It("int64", func() {
		Expect(PointerDeref[int64](Pointer[int64](123), 0)).Should(Equal(int64(123)))
		Expect(PointerDeref[int64](nil, 0)).Should(Equal(int64(0)))
	})
	It("float32", func() {
		Expect(PointerDeref[float32](Pointer[float32](123), 0)).Should(Equal(float32(123)))
		Expect(PointerDeref[float32](nil, 0)).Should(Equal(float32(0)))
	})
	It("float64", func() {
		Expect(PointerDeref[float64](Pointer[float64](123), 0)).Should(Equal(float64(123)))
		Expect(PointerDeref[float64](nil, 0)).Should(Equal(float64(0)))
	})
	It("string", func() {
		Expect(PointerDeref[string](Pointer[string]("some-string"), "another-string")).Should(HaveValue(Equal("some-string")))
		Expect(PointerDeref[string](nil, "another-string")).Should(HaveValue(Equal("another-string")))
	})
	It("int", func() {
		Expect(PointerDeref[int](Pointer[int](123), 0)).Should(Equal(int(123)))
		Expect(PointerDeref[int](nil, 0)).Should(Equal(int(0)))
	})
	It("uint", func() {
		Expect(PointerDeref[uint](Pointer[uint](123), 0)).Should(Equal(uint(123)))
		Expect(PointerDeref[uint](nil, 0)).Should(Equal(uint(0)))
	})
	It("byte", func() {
		Expect(PointerDeref[byte](Pointer[byte](123), 0)).Should(Equal(byte(123)))
		Expect(PointerDeref[byte](nil, 0)).Should(Equal(byte(0)))
	})
	It("struct", func() {
		type testStruct struct {
			f string
		}
		Expect(PointerDeref(Pointer(testStruct{f: "s"}), testStruct{f: "s2"})).Should(Equal(testStruct{f: "s"}))
		Expect(PointerDeref(nil, testStruct{f: "s2"})).Should(Equal(testStruct{f: "s2"}))
	})
})

var _ = Describe("SaveAdditionalFields", func() {
	type Src struct {
		runtime.Object
		StructPointer *struct {
			String        string  `json:"string"`
			StringPointer *string `json:"stringPointer"`
			Struct        struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
			} `json:"struct"`
		} `json:"structPointer"`
		Slice           []string `json:"slice"`
		SliceWithStruct []struct {
			String        string  `json:"string"`
			StringPointer *string `json:"stringPointer"`
			Struct        struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
			} `json:"struct"`
		} `json:"sliceWithStruct"`
	}
	It("full", func() {
		type Dst struct {
			StructPointer *struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					String        string  `json:"string"`
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"structPointer"`
			Slice           []string `json:"slice"`
			SliceWithStruct []struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					String        string  `json:"string"`
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"sliceWithStruct"`
		}

		var af = Dst{}
		var s = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "0-string",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "2-string",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "5-string",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "7-string",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &s)
		var expectedAF = Dst{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "0-string",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "2-string",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "5-string",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "7-string",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &expectedAF)
		SaveAdditionalFields(&af, &s)
		Expect(af).Should(Equal(expectedAF))
	})
	It("dst partial", func() {
		type Dst struct {
			StructPointer *struct {
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"structPointer"`
			Slice           []string `json:"slice"`
			SliceWithStruct []struct {
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"sliceWithStruct"`
		}

		var af = Dst{}
		var s = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "0-string",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "2-string",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "5-string",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "7-string",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &s)
		var expectedAF = Dst{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "stringPointer": "1-string-pointer",
    "struct": {
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "stringPointer": "6-string-pointer",
      "struct": {
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &expectedAF)
		SaveAdditionalFields(&af, &s)
		Expect(af).Should(Equal(expectedAF))
	})
	It("src partial", func() {
		type Src struct {
			runtime.Object
			StructPointer *struct {
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"structPointer"`
			Slice           []string `json:"slice"`
			SliceWithStruct []struct {
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"sliceWithStruct"`
		}
		type Dst struct {
			StructPointer *struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					String        string  `json:"string"`
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"structPointer"`
			Slice           []string `json:"slice"`
			SliceWithStruct []struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					String        string  `json:"string"`
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"sliceWithStruct"`
		}

		var af = Dst{}
		var s = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "stringPointer": "1-string-pointer",
    "struct": {
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "stringPointer": "6-string-pointer",
      "struct": {
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &s)
		var expectedAF = Dst{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &expectedAF)
		SaveAdditionalFields(&af, &s)
		Expect(af).Should(Equal(expectedAF))
	})
})

var _ = Describe("RestoreAdditionalFields", func() {
	type Src struct {
		runtime.Object
		StructPointer *struct {
			String        string  `json:"string"`
			StringPointer *string `json:"stringPointer"`
			Struct        struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
			} `json:"struct"`
		} `json:"structPointer"`
		Slice           []string `json:"slice"`
		SliceWithStruct []struct {
			String        string  `json:"string"`
			StringPointer *string `json:"stringPointer"`
			Struct        struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
			} `json:"struct"`
		} `json:"sliceWithStruct"`
	}
	It("full", func() {
		type Dst struct {
			StructPointer *struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					String        string  `json:"string"`
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"structPointer"`
			Slice           []string `json:"slice"`
			SliceWithStruct []struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					String        string  `json:"string"`
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"sliceWithStruct"`
		}

		var af = Dst{}
		var s = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "0-string",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "2-string",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "5-string",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "7-string",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &af)
		var expectedS = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "0-string",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "2-string",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "5-string",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "7-string",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &expectedS)
		RestoreAdditionalFields(&s, &af)
		Expect(s).Should(Equal(expectedS))
	})
	It("dst partial", func() {
		type Dst struct {
			StructPointer *struct {
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"structPointer"`
			Slice           []string `json:"slice"`
			SliceWithStruct []struct {
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"sliceWithStruct"`
		}

		var af = Dst{}
		var s = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "0-string",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "2-string",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "5-string",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "7-string",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &af)
		var expectedS = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &expectedS)
		RestoreAdditionalFields(&s, &af)
		Expect(s).Should(Equal(expectedS))
	})
	It("src partial", func() {
		type Src struct {
			runtime.Object
			StructPointer *struct {
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"structPointer"`
			Slice           []string `json:"slice"`
			SliceWithStruct []struct {
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"sliceWithStruct"`
		}
		type Dst struct {
			StructPointer *struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					String        string  `json:"string"`
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"structPointer"`
			Slice           []string `json:"slice"`
			SliceWithStruct []struct {
				String        string  `json:"string"`
				StringPointer *string `json:"stringPointer"`
				Struct        struct {
					String        string  `json:"string"`
					StringPointer *string `json:"stringPointer"`
				} `json:"struct"`
			} `json:"sliceWithStruct"`
		}

		var af = Dst{}
		var s = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "string": "0-string",
    "stringPointer": "1-string-pointer",
    "struct": {
      "string": "2-string",
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "string": "5-string",
      "stringPointer": "6-string-pointer",
      "struct": {
        "string": "7-string",
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &af)
		var expectedS = Src{}
		_ = json.Unmarshal([]byte(`{
  "structPointer": {
    "stringPointer": "1-string-pointer",
    "struct": {
      "stringPointer": "3-string-pointer"
    }
  },
  "slice": [
    "4-string"
  ],
  "sliceWithStruct": [
    {
      "stringPointer": "6-string-pointer",
      "struct": {
        "stringPointer": "8-string-pointer"
      }
    }
  ]
}`), &expectedS)
		RestoreAdditionalFields(&s, &af)
		Expect(s).Should(Equal(expectedS))
	})
})
