/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package common

import (
	"math/rand"
	"reflect"
	"strings"
	"time"

	"k8s.io/apimachinery/pkg/runtime"
)

var rngSource = rand.NewSource(time.Now().UnixNano())

func GeneratePassword(length int) string {
	rng := rand.New(rngSource)
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		"0123456789")
	chartsLength := len(chars)

	var password strings.Builder
	for i := 0; i < length; i++ {
		password.WriteRune(chars[rng.Intn(chartsLength)])
	}

	return password.String()
}

func BoolToYesNo(value bool) string {
	if value {
		return "yes"
	}

	return "no"
}

func Pointer[T any](val T) *T {
	return &val
}

func PointerDeref[T any](val *T, defaultValue T) T {
	if val != nil {
		return *val
	}

	return defaultValue
}

func SaveAdditionalFields[T any](fields *T, obj runtime.Object) {
	fieldsValue := reflect.ValueOf(fields)
	objectValue := reflect.ValueOf(obj)

	copyFieldValue(objectValue, objectValue.Type(), fieldsValue, fieldsValue.Type())
}

func RestoreAdditionalFields[T any](obj runtime.Object, fields *T) {
	fieldsValue := reflect.ValueOf(fields)
	objectValue := reflect.ValueOf(obj)

	copyFieldValue(fieldsValue, fieldsValue.Type(), objectValue, objectValue.Type())
}

func copyFieldValue(srcValue reflect.Value, srcType reflect.Type, dstValue reflect.Value, dstType reflect.Type) {
	if srcType.Kind() != dstType.Kind() {
		panic("value of type " + srcType.String() + " is not assignable to type " + dstType.String())
	}

	switch srcType.Kind() {
	case reflect.Struct:
		for i := 0; i < dstValue.NumField(); i++ {
			typeFieldField := dstValue.Type().Field(i)

			fieldName := typeFieldField.Name

			for j := 0; j < srcValue.NumField(); j++ {
				typeObjectField := srcValue.Type().Field(j)

				objectFieldName := typeObjectField.Name
				if objectFieldName == fieldName {
					copyFieldValue(srcValue.Field(j), srcValue.Field(j).Type(), dstValue.Field(i), dstValue.Field(i).Type())

					break
				}
			}
		}
	case reflect.Pointer:
		if !srcValue.IsNil() {
			srcElem := srcValue.Elem()
			if dstValue.IsNil() {
				dstValue.Set(reflect.New(dstType.Elem()))
			}
			copyFieldValue(srcElem, srcElem.Type(), dstValue.Elem(), dstType.Elem())
		} else {
			dstValue.Set(reflect.Zero(srcType))
		}
	case reflect.Slice:
		if dstValue.IsNil() || dstValue.Len() != srcValue.Len() {
			dstValue.Set(reflect.MakeSlice(dstType, srcValue.Len(), srcValue.Cap()))
		}
		for i := 0; i < srcValue.Len(); i++ {
			copyFieldValue(srcValue.Index(i), srcValue.Index(i).Type(), dstValue.Index(i), dstValue.Index(i).Type())
		}
	default:
		dstValue.Set(srcValue)
	}
}
