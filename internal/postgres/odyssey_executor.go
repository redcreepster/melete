/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package postgres

import (
	"context"
	"errors"
	"os"
	"os/exec"
	"path"
	"syscall"
	"time"

	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

type OdysseyExecutor interface {
	manager.Runnable

	ReloadConfig(odyssey, storages, databases string) error
	IsRunning() bool
}

var _ OdysseyExecutor = &odysseyExecutor{}

type odysseyExecutor struct {
	executablePath string
	configsPath    string

	configIsEmpty bool

	cmd *exec.Cmd
	// Is true after call #Start()
	running bool
	// Is true after call #stop()
	stopped bool

	sighupTimer         *time.Timer
	reloadScheduled     bool
	latestOdysseyConf   string
	latestStoragesConf  string
	latestDatabasesConf string
}

var execCommand = exec.Command
var reloadConfigThrottleDuration = time.Second * 5

func (e *odysseyExecutor) Start(ctx context.Context) error {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	if e.running && !e.stopped {
		logger.V(5).Info("odysseyExecutor already running")

		return errors.New("odysseyExecutor already running")
	}

	e.sighupTimer = time.NewTimer(0)
	e.configIsEmpty = true

	go func() {
		for {
			select {
			case <-e.sighupTimer.C:
				if e.running && e.cmd != nil && e.cmd.Process != nil {
					err := e.cmd.Process.Signal(syscall.SIGHUP)
					if err != nil {
						logger.Error(err, "failed to send SIGHUP to odyssey")
					} else {
						e.reloadScheduled = false
					}
				}
			case <-ctx.Done():
				return
			}
		}
	}()

	go func() {
		for {
			if e.configIsEmpty {
				e.running = true
				e.stopped = false

				logger.Info("Config is empty. Wait for config.")

				select {
				case <-time.After(5 * time.Second):
					continue
				case <-ctx.Done():
					stopCtx, cancel := context.WithTimeout(context.Background(), time.Second*10)
					log.IntoContext(ctx, logger)

					e.stop(stopCtx)

					cancel()

					return
				}
			}

			e.cmd = execCommand(e.executablePath, path.Join(path.Join(e.configsPath, "odyssey.conf")))

			e.cmd.Stdout = os.Stdout
			e.cmd.Stderr = os.Stderr

			e.triggerSighupTimer()

			var startChannel = make(chan error, 1)
			var stateChannel = make(chan *os.ProcessState, 1)

			go func() {
				err := e.cmd.Start()
				if err != nil {
					startChannel <- err

					return
				}
				close(startChannel)

				e.stopped = false
				e.running = true

				state, _ := e.cmd.Process.Wait()
				e.running = false

				stateChannel <- state
			}()
			if err := <-startChannel; err != nil {
				logger.Error(err, "unable to start odyssey")

				return
			}

			select {
			case processState := <-stateChannel:
				if processState != nil && !e.stopped {
					logger.V(5).Info("ExecutorController stopped. Restarting after 5 seconds")

					select {
					case <-time.After(5 * time.Second):
						continue
					case <-ctx.Done():
						stopCtx, cancel := context.WithTimeout(context.Background(), time.Second*10)
						log.IntoContext(ctx, logger)

						e.stop(stopCtx)

						cancel()

						return
					}
				}
			case <-ctx.Done():
				stopCtx, cancel := context.WithTimeout(context.Background(), time.Second*10)
				log.IntoContext(ctx, logger)

				e.stop(stopCtx)

				cancel()
				e.sighupTimer.Stop()

				return
			}
		}
	}()

	return nil
}

func (e *odysseyExecutor) stop(ctx context.Context) {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	e.stopped = true

	if !e.running {
		logger.V(5).Info("ExecutorController already stopped")
		return
	}

	err := e.cmd.Process.Signal(syscall.SIGTERM)
	if err != nil {
		logger.V(5).Info("failed to send SIGTERM to odyssey", "error", err)
	} else {
		var thresholds = 30
	thresholdsLoop:
		for i := thresholds; i >= 0; i-- {
			if !e.running {
				return
			}
			select {
			case <-time.After(time.Second):
			case <-ctx.Done():
				break thresholdsLoop
			}
		}
	}

	err = e.cmd.Process.Kill()
	if err != nil {
		logger.V(5).Info("failed to send SIGKILL to odyssey", "error", err)
	}
}

func (e *odysseyExecutor) ReloadConfig(odyssey, storages, databases string) error {
	e.configIsEmpty = odyssey == "" || storages == "" || databases == ""
	reloadRequired := false

	if e.latestOdysseyConf != odyssey {
		if err := os.WriteFile(path.Join(e.configsPath, "odyssey.conf"), []byte(odyssey), 0600); err != nil {
			return err
		}

		e.latestOdysseyConf = odyssey

		reloadRequired = true
	}

	if e.latestStoragesConf != storages {
		if err := os.WriteFile(path.Join(e.configsPath, "storages.conf"), []byte(storages), 0600); err != nil {
			return err
		}

		e.latestStoragesConf = storages

		reloadRequired = true
	}

	if e.latestDatabasesConf != databases {
		if err := os.WriteFile(path.Join(e.configsPath, "databases.conf"), []byte(databases), 0600); err != nil {
			return err
		}

		e.latestDatabasesConf = databases

		reloadRequired = true
	}

	if reloadRequired {
		e.triggerSighupTimer()
	}

	return nil
}

func (e *odysseyExecutor) triggerSighupTimer() {
	e.sighupTimer.Reset(reloadConfigThrottleDuration)
	e.reloadScheduled = true
}

func (e *odysseyExecutor) IsRunning() bool {
	return e.running
}

func NewOdysseyExecutor(odysseyExecutable, odysseyConfPath string) OdysseyExecutor {
	return &odysseyExecutor{
		executablePath: odysseyExecutable,
		configsPath:    odysseyConfPath,
	}
}
