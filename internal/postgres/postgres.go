/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package postgres

import (
	"context"
	"errors"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"sigs.k8s.io/controller-runtime/pkg/log"

	. "git.monitorsoft.ru/k8s/melete/internal/common"
)

type loggerAdapter struct {
	pgx.Logger

	logger logr.Logger
}

func (a loggerAdapter) Log(_ context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	var keyValuesData []interface{}
	for key, value := range data {
		keyValuesData = append(keyValuesData, key, value)
	}
	a.logger.V(int(level)*10).Info(msg, keyValuesData...)
}

//+kubebuilder:object:generate=true

type Credentials struct {
	Host     string
	Port     uint16
	Username string
	Password string
}

func (c *Credentials) empty() bool {
	if c.Host == "" {
		return true
	}
	if c.Port == 0 {
		return true
	}
	if c.Username == "" {
		return true
	}
	if c.Password == "" {
		return true
	}

	return false
}

func (c *Credentials) connectionString() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%d/postgres",
		c.Username,
		c.Password,
		c.Host,
		c.Port,
	)
}

var (
	CredentialsEmpty = errors.New("credentials is empty")
)

func IsCredentialsEmptyError(err error) bool {
	return errors.Is(err, CredentialsEmpty)
}

type Client interface {
	GetCredentials() Credentials

	GetExtensions(ctx context.Context) ([]string, error)

	CreateOrUpdateUser(ctx context.Context, username, password string) error
	CreateOrUpdateDatabase(ctx context.Context, name, owner string, extensions []string, templateDatabase *string) error
	DropUser(ctx context.Context, username string) error
	DropDatabase(ctx context.Context, name string) error
	Close(ctx context.Context)
}

var _ Client = &client{}

type client struct {
	credentials Credentials

	connection *pgx.Conn
}

func (c *client) GetCredentials() Credentials {
	out := Credentials{}
	c.credentials.DeepCopyInto(&out)

	return out
}

func (c *client) GetExtensions(ctx context.Context) ([]string, error) {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return nil, err
	}

	var extensions []string
	//goland:noinspection SqlNoDataSourceInspection,SqlDialectInspection
	sql := "SELECT name as Name FROM pg_available_extensions"
	rows, err := connection.Query(ctx, sql)
	if err != nil {
		logger.Error(err, "error on get available extensions")

		return nil, err
	}

	for rows.Next() {
		var name string
		if err = rows.Scan(&name); err != nil {
			return nil, err
		}

		extensions = append(extensions, name)
	}

	return extensions, nil
}

func (c *client) CreateOrUpdateUser(ctx context.Context, username, password string) error {
	logger := log.FromContext(ctx).WithValues("username", username)
	ctx = log.IntoContext(ctx, logger)

	if username == "postgres" {
		return nil
	}

	userExists, err := c.userExists(ctx, username)
	if err != nil {
		return err
	}

	if userExists {
		logger.Info("updating user password")

		if err = c.setUserPassword(ctx, username, password); err != nil {
			logger.Error(err, "unable to update user password")

			return err
		}

		logger.Info("updated user password")

		return nil
	}

	logger.Info("creating user")
	if err = c.createUser(ctx, username, password); err != nil {
		logger.Error(err, "unable to create user")

		return err
	}

	logger.Info("user created")

	return nil
}

func (c *client) CreateOrUpdateDatabase(ctx context.Context, name, owner string, extensions []string, templateDatabase *string) error {
	logger := log.FromContext(ctx).WithValues(
		"name", name,
		"templateDatabase", templateDatabase,
		"extensions", extensions,
		"owner", owner,
	)
	ctx = log.IntoContext(ctx, logger)

	exists, err := c.databaseExists(ctx, name)
	if err != nil {
		return err
	}

	if !exists {
		logger.Info("Database not exist. Creating")
		if PointerDeref(templateDatabase, "") != "" {
			err = c.copyDatabase(ctx, *templateDatabase, name, owner)
			if err != nil {
				logger.Error(err, "unable copy database")

				return err
			}

			logger.Info("database copied")
		} else {
			err = c.createDatabase(ctx, name, owner)
			if err != nil {
				logger.Error(err, "unable create database")

				return err
			}

			logger.Info("database created")
		}
	}

	// TODO: delete extensions
	if len(extensions) > 0 {
		err = c.createExtensions(ctx, name, extensions)
		if err != nil {
			return err
		}

		logger.Info("extensions created")
	}

	return c.changeOwner(ctx, name, owner)
}

func (c *client) DropUser(ctx context.Context, username string) error {
	logger := log.FromContext(ctx).WithValues("username", username)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	sql := fmt.Sprintf("DROP USER \"%s\";", username)
	_, err = connection.Exec(ctx, sql)
	if err != nil {
		logger.Error(err, "unable to drop user")

		return err
	}

	return nil
}

func (c *client) DropDatabase(ctx context.Context, name string) error {
	if err := c.stopDatabase(ctx, name); err != nil {
		return err
	}

	if err := c.dropDatabase(ctx, name); err != nil {
		return err
	}

	return nil
}

func (c *client) Close(ctx context.Context) {
	if c.connection != nil && !c.connection.IsClosed() {
		_ = c.connection.Close(ctx)
	}
}

func (c *client) getConnection(ctx context.Context) (*pgx.Conn, error) {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	if err := c.assertHaveCredentials(); err != nil {
		return nil, err
	}

	if c.connection == nil || c.connection.IsClosed() || c.connection.PgConn() == nil || c.connection.PgConn().IsBusy() {
		if c.connection != nil && (c.connection.PgConn() == nil || c.connection.PgConn().IsBusy()) {
			if err := c.connection.Close(ctx); err != nil {
				logger.Error(err, "error close connection on database")
			}
		}

		return c.connection, c.connect(ctx)
	}

	return c.connection, nil
}

func (c *client) connect(ctx context.Context) error {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	var err error
	config, err := pgx.ParseConfig(c.credentials.connectionString())
	if err != nil {
		logger.Error(err, "unable parse config")

		return err
	}
	config.Logger = loggerAdapter{logger: logger}
	config.LogLevel = pgx.LogLevelTrace

	c.connection, err = pgx.ConnectConfig(ctx, config)
	if err != nil {
		logger.Error(err, "unable connect to database")

		return err
	}

	return nil
}

func (c *client) assertHaveCredentials() error {
	if c.credentials.empty() {
		return CredentialsEmpty
	}

	return nil
}

func (c *client) userExists(ctx context.Context, username string) (bool, error) {
	logger := log.FromContext(ctx).WithValues("username", username)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return false, err
	}

	var count byte
	//goland:noinspection SqlNoDataSourceInspection,SqlDialectInspection
	sql := "SELECT COUNT(usename) FROM pg_catalog.pg_user WHERE usename = $1"
	if err = connection.QueryRow(ctx, sql, username).Scan(&count); err != nil {
		logger.Error(err, "error on check user")

		return false, err
	}

	return count > 0, nil
}

func (c *client) createUser(ctx context.Context, username, password string) error {
	logger := log.FromContext(ctx).WithValues("username", username)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	//goland:noinspection SqlNoDataSourceInspection
	sql := fmt.Sprintf("CREATE USER \"%s\" WITH PASSWORD '%s'", username, password)
	if _, err = connection.Exec(ctx, sql); err != nil {
		logger.Error(err, "error on create user")

		return err
	}

	return nil
}

func (c *client) databaseExists(ctx context.Context, name string) (bool, error) {
	logger := log.FromContext(ctx).WithValues("name", name)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return false, err
	}

	var count byte
	//goland:noinspection SqlNoDataSourceInspection,SqlDialectInspection
	sql := "SELECT COUNT(datname) FROM pg_catalog.pg_database WHERE datname = $1"
	err = connection.QueryRow(ctx, sql, name).Scan(&count)
	if err != nil {
		logger.Error(err, "Error on check database")

		return false, err
	}

	return count > 0, nil
}

func (c *client) createDatabase(ctx context.Context, name, owner string) error {
	logger := log.FromContext(ctx).WithValues(
		"name", name,
		"owner", owner,
	)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	//goland:noinspection SqlNoDataSourceInspection
	sqlCreateDatabase := fmt.Sprintf("CREATE database \"%s\" WITH OWNER \"%s\"", name, owner)
	_, err = connection.Exec(ctx, sqlCreateDatabase)
	if err != nil {
		logger.Error(err, "error on create database")

		return err
	}

	return nil
}

func (c *client) copyDatabase(ctx context.Context, source string, name string, owner string) error {
	logger := log.FromContext(ctx).WithValues(
		"source", source,
		"name", name,
		"owner", owner,
	)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	//goland:noinspection SqlNoDataSourceInspection,SqlDialectInspection
	sqlTerminateBackend := fmt.Sprintf("SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '%s'", source)
	if _, err = connection.Exec(ctx, sqlTerminateBackend); err != nil {
		logger.Error(err, "error on disconnect all clients from database")

		return err
	}

	//goland:noinspection SqlNoDataSourceInspection,SqlResolve
	sqlCreateDatabase := fmt.Sprintf("CREATE DATABASE \"%s\" WITH TEMPLATE \"%s\" OWNER \"%s\"", name, source, owner)
	if _, err = connection.Exec(ctx, sqlCreateDatabase); err != nil {
		logger.Error(err, "error on create database")

		return err
	}

	return nil
}

func (c *client) createExtensions(ctx context.Context, name string, extensions []string) error {
	logger := log.FromContext(ctx).WithValues("name", name)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	newDatabaseConnectionConfig := connection.Config().Copy()
	newDatabaseConnectionConfig.Database = name
	newDatabaseConnection, err := pgx.ConnectConfig(ctx, newDatabaseConnectionConfig)
	if err != nil {
		logger.Error(err, "unable connect to database")

		return err
	}

	defer func() {
		if err := newDatabaseConnection.Close(ctx); err != nil {
			logger.Error(err, "error close connection on database")
		}
	}()

	for _, extension := range extensions {
		loopLogger := logger.WithValues("extension", extension)

		sqlAddExtension := fmt.Sprintf("CREATE EXTENSION IF NOT EXISTS \"%s\";", extension)
		if _, err = newDatabaseConnection.Exec(ctx, sqlAddExtension); err != nil {
			if pgErr, ok := err.(*pgconn.PgError); ok {
				if pgErr.Code == "23505" {
					loopLogger.Info("extension already exists")

					continue
				}
			}
			loopLogger.Error(err, "unable create extension")

			return err
		}
	}

	return nil
}

func (c *client) changeOwner(ctx context.Context, name, owner string) error {
	logger := log.FromContext(ctx).WithValues(
		"name", name,
		"owner", owner,
	)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	sqlAlterToOwner := fmt.Sprintf(`DO $$
DECLARE
    r record;
    v_new_owner varchar := '%s';
BEGIN
    FOR r IN
        SELECT 'ALTER TABLE "' || table_schema || '"."' || table_name || '" OWNER TO "' || v_new_owner || '";' AS a FROM information_schema.tables WHERE NOT table_schema IN ('pg_catalog', 'information_schema')
        UNION ALL
        SELECT 'ALTER TABLE "' || sequence_schema || '"."' || sequence_name || '" OWNER TO "' || v_new_owner || '";' AS a FROM information_schema.sequences WHERE NOT sequence_schema IN ('pg_catalog', 'information_schema')
        UNION ALL
        SELECT 'ALTER TABLE "' || table_schema || '"."' || table_name || '" OWNER TO "' || v_new_owner || '";' AS a FROM information_schema.views WHERE NOT table_schema IN ('pg_catalog', 'information_schema')
        UNION ALL
        SELECT 'ALTER FUNCTION "' || nsp.nspname || '"."' || p.proname || '"(' || pg_get_function_identity_arguments(p.oid) || ') OWNER TO "' || v_new_owner || '";' AS a FROM pg_proc p JOIN pg_namespace nsp ON p.pronamespace = nsp.oid WHERE NOT nsp.nspname IN ('pg_catalog', 'information_schema')
        UNION ALL
        SELECT 'ALTER TYPE "' || n.nspname || '"."' || t.typname || '" OWNER TO "' || v_new_owner || '";' AS a FROM pg_type t LEFT JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace WHERE (t.typrelid = 0 OR (SELECT c.relkind = 'c' FROM pg_catalog.pg_class c WHERE c.oid = t.typrelid)) AND NOT EXISTS(SELECT 1 FROM pg_catalog.pg_type el WHERE el.oid = t.typelem AND el.typarray = t.oid) AND n.nspname NOT IN ('pg_catalog', 'information_schema')
        UNION ALL
        SELECT DISTINCT 'ALTER SCHEMA "' || table_schema || '" OWNER TO "' || v_new_owner || '";' AS a FROM information_schema.tables WHERE NOT table_schema IN ('pg_catalog', 'information_schema')
        UNION ALL
        SELECT 'ALTER DATABASE "' || current_database() || '" OWNER TO "' || v_new_owner || '"' AS a
        LOOP
            EXECUTE r.a;
        END LOOP;
END$$;`, owner)

	newDatabaseConnectionConfig := connection.Config().Copy()
	newDatabaseConnectionConfig.Database = name
	newDatabaseConnection, err := pgx.ConnectConfig(ctx, newDatabaseConnectionConfig)
	if err != nil {
		logger.Error(err, "Unable connect to database")

		return err
	}

	defer func() {
		if err := newDatabaseConnection.Close(ctx); err != nil {
			logger.Error(err, "Error close connection on database")
		}
	}()

	if _, err = newDatabaseConnection.Exec(ctx, sqlAlterToOwner); err != nil {
		logger.Error(err, "Error on grant privileges on database")

		return err
	}

	return nil
}

func (c *client) setUserPassword(ctx context.Context, username, password string) error {
	logger := log.FromContext(ctx).WithValues("username", username)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	sql := fmt.Sprintf("ALTER USER \"%s\" WITH PASSWORD '%s'", username, password)
	_, err = connection.Exec(ctx, sql)
	if err != nil {
		logger.Error(err, "error on set user password")

		return err
	}

	return nil
}

func (c *client) stopDatabase(ctx context.Context, name string) error {
	logger := log.FromContext(ctx).WithValues("name", name)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	//goland:noinspection SqlNoDataSourceInspection
	maintenanceModeQuery := fmt.Sprintf("ALTER DATABASE \"%s\" WITH ALLOW_CONNECTIONS false;", name)
	_, err = connection.Exec(ctx, maintenanceModeQuery)
	if err != nil {
		logger.Error(err, "unable to set db in maintenance mode")

		return err
	}

	//goland:noinspection SqlNoDataSourceInspection,SqlDialectInspection
	dropConnectionsQuery := fmt.Sprintf("SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname='%s';", name)
	_, err = connection.Exec(ctx, dropConnectionsQuery)
	if err != nil {
		logger.Error(err, "Unable to drop db connections")

		return err
	}

	return nil
}

func (c *client) dropDatabase(ctx context.Context, name string) error {
	logger := log.FromContext(ctx)
	ctx = log.IntoContext(ctx, logger)

	connection, err := c.getConnection(ctx)
	if err != nil {
		return err
	}

	//goland:noinspection SqlNoDataSourceInspection,SqlResolve
	sql := fmt.Sprintf("DROP DATABASE \"%s\";", name)
	_, err = connection.Exec(ctx, sql)
	if err != nil {
		logger.Error(err, "Unable to drop database")

		return err
	}

	return nil
}

func NewPostgresClient(credentials Credentials) Client {
	return &client{
		credentials: credentials,
	}
}
