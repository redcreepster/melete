#!/bin/sh

set -e

test -s ./bin/yq || mkdir -p ./bin/ \
    && wget -q https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O ./bin/yq \
    && chmod +x ./bin/yq

rm -rf helm/melete-rendered/
helm template melete helm/melete --output-dir helm/melete-rendered > /dev/null

set +e

failed=false

for item in $(find helm/melete-rendered/melete/templates/crd/ -type f | grep -E "\.yaml$"); do
  name=$(basename "${item}")

  ./bin/yq --output-format props --prettyPrint .spec.versions "config/crd/bases/${name}" | sort > "helm/melete-rendered/base-${name}"
  ./bin/yq --output-format props --prettyPrint .spec.versions "${item}" | sort > "helm/melete-rendered/rendered-${name}"

  if ! diff "helm/melete-rendered/base-${name}" "helm/melete-rendered/rendered-${name}" > /dev/null; then
    echo "Diff in config/crd/bases/${name}"

    failed=true
  fi
done

set -e

if $failed; then
  exit 1
fi
