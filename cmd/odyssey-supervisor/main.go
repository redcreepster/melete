/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"errors"
	"flag"
	"net/http"
	"os"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"git.monitorsoft.ru/k8s/melete/internal/postgres"

	"git.monitorsoft.ru/k8s/melete/internal/controllers/database"

	//+kubebuilder:scaffold:imports

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha3"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha4"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(v1alpha1.AddToScheme(scheme))
	utilruntime.Must(v1alpha2.AddToScheme(scheme))
	utilruntime.Must(v1alpha3.AddToScheme(scheme))
	utilruntime.Must(v1alpha4.AddToScheme(scheme))
	utilruntime.Must(v1alpha5.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var probeAddr string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")

	var instanceName string
	flag.StringVar(&instanceName, "instance-name", "master", "Instance name.")
	var odysseyExecutable string
	flag.StringVar(&odysseyExecutable, "odyssey-executable", "/usr/bin/odyssey-supervisor", "Path of yandex odyssey.")
	var odysseyConfPath string
	flag.StringVar(&odysseyConfPath, "odyssey-conf-path", "/etc/odyssey", "Path of yandex odyssey configs.")

	opts := zap.Options{}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         false,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	signalHandler := ctrl.SetupSignalHandler()

	if _, err = os.Stat(odysseyExecutable); errors.Is(err, os.ErrNotExist) {
		setupLog.Error(err, "odyssey executable not found", "path", odysseyExecutable)
		os.Exit(1)
	}
	if _, err = os.Stat(odysseyConfPath); errors.Is(err, os.ErrNotExist) {
		setupLog.Error(err, "odyssey config path not found", "path", odysseyConfPath)
		os.Exit(1)
	}

	executor := postgres.NewOdysseyExecutor(odysseyExecutable, odysseyConfPath)
	if err = mgr.Add(executor); err != nil {
		setupLog.Error(err, "unable to create executor", "executor", "odysseyExecutor")
		os.Exit(1)
	}

	if err = (&database.OdysseySupervisor{
		Client:       mgr.GetClient(),
		Scheme:       mgr.GetScheme(),
		InstanceName: instanceName,
		Executor:     executor,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "OdysseySupervisor")
		os.Exit(1)
	}

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", func(req *http.Request) error {
		if !executor.IsRunning() {
			return errors.New("executor not running")
		}

		return nil
	}); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(signalHandler); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
