/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"errors"
	"flag"
	"os"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"git.monitorsoft.ru/k8s/melete/internal/webhooks"

	"git.monitorsoft.ru/k8s/melete/internal/controllers/database"

	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha1"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha2"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha3"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha4"
	"git.monitorsoft.ru/k8s/melete/internal/apis/database/v1alpha5"
	//+kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(v1alpha1.AddToScheme(scheme))
	utilruntime.Must(v1alpha2.AddToScheme(scheme))
	utilruntime.Must(v1alpha3.AddToScheme(scheme))
	utilruntime.Must(v1alpha4.AddToScheme(scheme))
	utilruntime.Must(v1alpha5.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	var disableWebhooks bool
	flag.BoolVar(&disableWebhooks, "disable-webhooks", false, "Disable webhooks")

	var odysseySupervisorServiceName string
	flag.StringVar(&odysseySupervisorServiceName, "odyssey-supervisor-service-name", "odyssey", "The name of odyssey supervisor service.")
	var odysseySupervisorServiceNamespace string
	flag.StringVar(&odysseySupervisorServiceNamespace, "odyssey-supervisor-service-namespace", "", "The namespace of odyssey supervisor service.")
	var odysseySupervisorDeploymentTemplatePath string
	flag.StringVar(&odysseySupervisorDeploymentTemplatePath, "odyssey-supervisor-deployment-template-path", "", "The path of odyssey supervisor deployment template.")
	var odysseyInstanceReplicas uint
	flag.UintVar(&odysseyInstanceReplicas, "odyssey-instance-replicas", 0, "The replicas of odyssey instance deployment.")
	var odysseyInstanceDefaultConfigPath string
	flag.StringVar(&odysseyInstanceDefaultConfigPath, "odyssey-instance-default-config-path", "", "The path of default odyssey config.")

	opts := zap.Options{}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	if odysseySupervisorServiceName == "" {
		var errorMessage = "unable get odyssey supervisor service name"
		setupLog.Error(errors.New(errorMessage), errorMessage)
		os.Exit(1)
	}
	if odysseySupervisorServiceNamespace == "" {
		if value, ok := os.LookupEnv("NAMESPACE"); ok {
			odysseySupervisorServiceNamespace = value
		} else {
			content, err := os.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/namespace")
			if err == nil {
				odysseySupervisorServiceNamespace = string(content)
			}
		}
		if odysseySupervisorServiceNamespace == "" {
			var errorMessage = "unable get odyssey supervisor namespace"
			setupLog.Error(errors.New(errorMessage), errorMessage)
			os.Exit(1)
		}
	}
	if odysseySupervisorDeploymentTemplatePath == "" {
		var errorMessage = "unable get odyssey supervisor template path"
		setupLog.Error(errors.New(errorMessage), errorMessage)
		os.Exit(1)
	}
	odysseySupervisorDeploymentTemplateBytes, err := os.ReadFile(odysseySupervisorDeploymentTemplatePath)
	if err != nil {
		var errorMessage = "unable read odyssey supervisor template path"
		setupLog.Error(errors.New(errorMessage), errorMessage)
		os.Exit(1)
	}
	if odysseyInstanceReplicas == 0 {
		var errorMessage = "unable get odyssey instance replicas"
		setupLog.Error(errors.New(errorMessage), errorMessage)
		os.Exit(1)
	}
	if odysseyInstanceDefaultConfigPath == "" {
		var errorMessage = "unable get default odyssey config path"
		setupLog.Error(errors.New(errorMessage), errorMessage)
		os.Exit(1)
	}
	odysseyInstanceDefaultConfigBytes, err := os.ReadFile(odysseyInstanceDefaultConfigPath)
	if err != nil {
		var errorMessage = "unable read default odyssey config path"
		setupLog.Error(errors.New(errorMessage), errorMessage)
		os.Exit(1)
	}

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "manager.monitorsoft.ru",
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&database.OdysseyReconciler{
		Client:                            mgr.GetClient(),
		Scheme:                            mgr.GetScheme(),
		OdysseySupervisorServiceName:      odysseySupervisorServiceName,
		OdysseySupervisorServiceNamespace: odysseySupervisorServiceNamespace,
		OdysseyInstanceReplicas:           int32(odysseyInstanceReplicas),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Odyssey")
		os.Exit(1)
	}

	if !disableWebhooks {
		if err = webhooks.SetupOdysseyWebhookWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "v1alpha4.Odyssey")
			os.Exit(1)
		}
	}

	if err = (&database.OdysseyInstanceReconciler{
		Client:                              mgr.GetClient(),
		Scheme:                              mgr.GetScheme(),
		OdysseySupervisorServiceName:        odysseySupervisorServiceName,
		OdysseySupervisorServiceNamespace:   odysseySupervisorServiceNamespace,
		OdysseySupervisorDeploymentTemplate: odysseySupervisorDeploymentTemplateBytes,
		DefaultOdysseyInstanceConfig:        odysseyInstanceDefaultConfigBytes,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "OdysseyInstance")
		os.Exit(1)
	}

	if !disableWebhooks {
		if err = webhooks.SetupOdysseyInstanceWebhookWithManager(mgr, odysseyInstanceDefaultConfigBytes); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "v1alpha2.OdysseyInstance")
			os.Exit(1)
		}
	}

	if !disableWebhooks {
		if err = webhooks.SetupPostgresEndpointWebhookWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "v1alpha1.PostgresEndpoint")
			os.Exit(1)
		}
	}
	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
