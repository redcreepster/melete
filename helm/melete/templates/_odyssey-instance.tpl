{{- define "melete.odyssey-instance" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    {{- include "melete.labels" . | nindent 4 }}
    app.kubernetes.io/component: odyssey-supervisor
spec:
  selector:
    matchLabels:
      {{- include "melete.selectorLabels" . | nindent 6 }}
      app.kubernetes.io/component: odyssey-supervisor
  template:
    metadata:
      annotations:
        kubectl.kubernetes.io/default-container: odyssey-supervisor
        {{- with .Values.odysseySupervisor.image.hash }}
        checksum/image: {{ . | quote }}
        {{- end }}
        checksum/secret: {{ (tpl (.Files.Get "configs/odyssey.conf" ) .) | sha256sum }}
      labels:
        {{- include "melete.labels" . | nindent 8 }}
        app.kubernetes.io/component: odyssey-supervisor
    spec:
      affinity:
        {{- if .Values.odysseySupervisor.affinity }}
        {{- .Values.odysseySupervisor.affinity | toYaml | nindent 8 }}
        {{- else }}
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: kubernetes.io/arch
                operator: In
                values:
                - amd64
              - key: kubernetes.io/os
                operator: In
                values:
                - linux
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          # На разных машинах
          - podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  {{- include "melete.labels" . | nindent 18 }}
                  app.kubernetes.io/component: odyssey-supervisor
            weight: 100
          # В разных регионах
          - podAffinityTerm:
              topologyKey: topology.kubernetes.io/region
              labelSelector:
                matchLabels:
                  {{- include "melete.labels" . | nindent 18 }}
                  app.kubernetes.io/component: odyssey-supervisor
            weight: 10
        {{- end }}
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
      {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        runAsNonRoot: true
      serviceAccountName: {{ include "melete.serviceAccountName" . }}
      terminationGracePeriodSeconds: 60
      containers:
      - name: odyssey-supervisor
        image: "{{ .Values.odysseySupervisor.image.registry }}/{{ .Values.odysseySupervisor.image.repository }}:{{ .Values.odysseySupervisor.image.tag | default .Chart.AppVersion }}"
        imagePullPolicy: {{ .Values.odysseySupervisor.image.pullPolicy }}
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            drop:
            - ALL
        command:
        - /usr/bin/odyssey-supervisor
        args:
        - --health-probe-bind-address=:8081
        - --metrics-bind-address=:8080
        - --odyssey-executable=/usr/bin/odyssey
        - --odyssey-conf-path=/etc/odyssey/
        - --zap-encoder=json
        - --zap-log-level={{ .Values.odysseySupervisor.logLevel | default "info" }}
        ports:
        - name: postgres
          containerPort: 5432
          protocol: TCP
        readinessProbe:
          httpGet:
            path: /readyz
            port: 8081
          initialDelaySeconds: 5
          periodSeconds: 10
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8081
          initialDelaySeconds: 15
          periodSeconds: 20
        {{- with .Values.odysseySupervisor.resources }}
        resources:
          {{- toYaml . | nindent 10 }}
        {{- end }}
{{- end -}}
